from apiapp.tests.admin_api import AdminApiTest
from apiapp.tests.auth import AuthTest
from apiapp.tests.mobile_api import MobileApiTest
from crm.tests.create_super_user import CreateSuperUser
from apiapp.tests.auth import LoginAsSuperuser

CreateSuperUser()
MobileApiTest()
AdminApiTest()
AuthTest()
LoginAsSuperuser()
