#!/usr/bin/env bash

source env/bin/activate
./manage.py dumpdata > log/db-dump/"database-$(date +%m-%d-%Y--%H-%M-%S)-git-$(git rev-parse --short HEAD ).dmp"
git pull
pip install -r requirements.txt
./manage.py makemigrations
./manage.py migrate
sudo systemctl restart gunicorn