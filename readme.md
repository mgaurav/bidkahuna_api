# How to install BiddingApp

I used my server IP for this doc `10.10.10.85`
For setup in to a different server, should use its IP.

### Install Ubuntu and update it

```bash
$ sudo apt update
$ sudo apt upgrade
```

### Install Requirements

```bash
$ sudo apt install git mysql-server libmysqlclient-dev python3-pip npm nodejs-legacy nginx
```

### Config GIT
```bash
$ git config --global user.email "you@example.com"
$ git config --global user.name "Your Name"
```



## Deploy Backend (APP API server)


### Install Virtualenv
```bash
$ sudo pip3 install virtualenv
```

If will be error like as:

```bash
locale.Error: unsupported locale setting
```

Should execute:
```bash
$ export LC_ALL="en_US.UTF-8"
$ export LC_CTYPE="en_US.UTF-8"
$ sudo dpkg-reconfigure locales
```

### Install code base
```bash
$ mkdir /home/developer/www/api/
$ cd /home/developer/www/api/
$ git clone https://romasolot@bitbucket.org/romasolot/biddingapp-api.git .
```

### Make virtual env and activate
```bash
$ virtualenv env
$ source env/bin/activate
```

### Install Pythons requirements
```bash
(env)$ pip install -r requirements.txt
```

### Create Database in Mysql
 
### Configure App Settings

Documentation included in the file
 
```bash
(env)$ cp .env.example .env
(env)$ nano .env
```

Install static data, migrations, send test mail

```bash
(env)$ ./manage.py collectstatic
(env)$ ./manage.py makemigrations
(env)$ ./manage.py migrate
(env)$ ./manage.py sendtestemail email developer.dev.2016@gmail.com
```

### Install PDF Utilite
Download and install Wkhtmltopdf

**@todo for developer:** sudo aptitude install libfontconfig

```bash
$ sudo apt install libxrender-dev
$ cd /home/developer/www/api/bin
$ wget http://download.gna.org/wkhtmltopdf/0.12/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz
$ tar xf wkhtmltox-0.12.4_linux-generic-amd64.tar.xz
```

### Create superuser
```bash
(env)$ ./manage.py createsuperuser --username=developer --email=dev@dev.loc
```

### Install fixtures
```bash
(env)$ ./manage.py loaddata ./db_seed/auth_group.json
(env)$ ./manage.py loaddata ./db_seed/billing_plan.json
(env)$ ./manage.py loaddata ./db_seed/measurement_type.json
```

### Test sever

as internal server 
```bash
(env)$ ./manage.py runserver 0.0.0.0:8000
```

open in browser
```
http://10.10.10.85:8000
```

You should see:
```
Hello, AnonymousUser
```

and run as gunicorn
`(env)$ gunicorn --bind 0.0.0.0:8000 biddingapp.wsgi:application`

open in browser
`
http://10.10.10.85:8000
`

You should see:
```
Hello, AnonymousUser
```

### Create and configure the Gunicorn systemd Service
```bash
$ sudo nano /etc/systemd/system/gunicorn.service
```

```ini
[Unit]
Description=gunicorn daemon
After=network.target

[Service]
User=developer
Group=www-data
WorkingDirectory=/home/developer/www/api
ExecStart=/home/developer/www/api/env/bin/gunicorn --workers 3 --bind unix:/home/developer/www/api/biddingapp.sock biddingapp.wsgi:application --error-logfile /home/developer/www/api-error.log --log-level debug

[Install]
WantedBy=multi-user.target
```

```bash
$ sudo systemctl start gunicorn
$ sudo systemctl enable gunicorn
```

*Output like this:*
```bash
Created symlink from /etc/systemd/system/multi-user.target.wants/gunicorn.service to /etc/systemd/system/gunicorn.service.
```            

*Check the status of the service by typing:*
```bash
$ sudo systemctl status gunicorn
```

### Config NGINX

```bash
$ sudo nano /etc/nginx/sites-available/biddingapp
```

```bash
server {
    listen 8000;
    server_name 10.10.10.85;

    location = /favicon.ico { access_log off; log_not_found off; }
    
    location /static/ {
        root /home/developer/www/api/;
    }
    
    location /media/ {
        root /home/developer/www/api/;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/home/developer/www/api/biddingapp.sock;
    }
}
```

```bash
$ sudo ln -s /etc/nginx/sites-available/biddingapp /etc/nginx/sites-enabled
$ sudo nginx -t
```

*Output like this:*

```bash
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```

Restart NGINX

```bash
$ sudo systemctl restart nginx
```


Check in browser:   
```
http://10.10.10.85:8000/
```

You Should see: 
```
Hello, AnonymousUser
```


## Deploy Front application

```bash
$ sudo npm install -g angular-cli@latest
$ mkdir /home/developer/www/client/
$ cd /home/developer/www/client/
$ git clone https://romasolot@bitbucket.org/romasolot/biddingapp-front.git .
$ npm install
```

*Config App*

Copy configure file and set Api url inf config file
```bash
$ cd /home/developer/www/client/src/environments
$ cp environment.prod.example.ts environment.prod.ts
$ cp environment.example.ts environment.ts
```

*Test app*
```bash
$ ng serve --host 0.0.0.0 --port 4201
```

*Set the Api url into config file for the dev*
```bash
$ nano environment.ts
```

```typescript
export const environment = {
  production: false,
  api: 'http://10.10.10.54:8000'
};
```

Check in browser. You should see the Login page.   
```
http://10.10.10.85:4201/
```

*Set the Api url into config file for the production*
```bash
$ nano environment.prod.ts
```

```typescript
export const environment = {
  production: true,
  api: 'http://10.10.10.54:8000'
};
```


*Build application*
```bash
$ ng build --prod
```

## Config NGINX
```bash
$ sudo nano /etc/nginx/sites-available/biddingapp_client
```

```bash
server {
    listen 80;
    server_name client;

    location / {
        root /home/developer/www/client/dist;
        index index.html;
        try_files $uri $uri/ /index.html;
    }
}
```

```bash
$ sudo ln -s /etc/nginx/sites-available/biddingapp_client /etc/nginx/sites-enabled
$ sudo rm /etc/nginx/sites-enabled/default
$ sudo nginx -t
$ sudo systemctl restart nginx
```

# Config
Login into admin Panel `http://10.10.10.85:8000/admin`

+ Open admin user form. 
+ Open user: `http://10.10.10.85:8000/admin/crm/user/1/change/`
+ Set Group: SUPERADMIN
+ Select Role: Superadmin
+ Save user



# How to update BiddingApp

## Backend application

```bash
$ cd /home/developer/www/api/
$ ./bin/update.sh
```

**or**

```bash
$ cd /home/developer/www/api/
$ git stash
$ git pull
$ git stash apply stash@{0}
$ source env/bin/activate
(env)$ pip install -r requirements.txt 
(env)$ ./manage.py makemigrations
(env)$ ./manage.py migrate
$ sudo systemctl restart gunicorn
```


## Frontend application
```bash
$ cd /home/developer/www/client/
$ git stash
$ git pull
$ npm install
$ ng build --prod
```

If will be errors for build process, should execute:
```bash
$ npm install
```
