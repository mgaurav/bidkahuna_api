"""biddingapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static

from biddingapp import views
from crm.views import pages

urlpatterns = []

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
        url(r'^admin/doc/', include('django.contrib.admindocs.urls'))
    ]

# Api
from rest_framework_jwt.views import obtain_jwt_token

urlpatterns += [
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/', include('apiapp.urls', namespace='api')),
    url(r'^shop/', include('onlineshop.urls', )),
]

# App urls
urlpatterns += [
                   url(r'^$', views.index, name='index'),
                   url(r'^terms-and-conditions/$', pages.show_terms, name='terms'),
                   url(r'^privacy-policy/$', pages.show_privacy, name='policy'),
                   url(r'^admin/', admin.site.urls),
                   url(r'^payment/', include('crm.urls_payment')),
                   url(r'^auth/', include('crm.urls_auth')),
                   url(r'^paypal/', include('paypal.standard.ipn.urls')),
               ] \
               + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
               + static(settings.STATIC_URL, document_root=settings.STATIC_URL)
