import logging
from django.core.management.base import BaseCommand
from django.utils import timezone
from crm.models import BillingCompany, BillingTransaction


class Command(BaseCommand):
    help = 'Check Billing State'
    logger = logging.getLogger('billing')

    def handle(self, *args, **options):
        queryset = BillingCompany.objects.all()
        target_billing_object = queryset.filter(next_check__lt=timezone.now(), company__is_active=True)
        self.logger.info('----------------------------')
        self.logger.info('Run the Check billing task.')
        self.logger.info('Found objects: %s', str(target_billing_object.count()))

        for obj in target_billing_object:
            company = obj.company
            plan = obj.used_plan

            self.logger.info('Process company: %s [%s]', company.name, company.id)
            self.logger.info('         active: %s', company.is_active)
            self.logger.info('     Using plan: %s [%s] / $%s', plan.name, plan.id, plan.price)
            company.is_active = False
            company.save()

            transaction = BillingTransaction()
            transaction.company_id = company.id
            transaction.comment = 'Your company deactivate.'
            transaction.save()
