function addcart(){
    $("#message").empty();
    $("#error").empty();
    var styleid = localStorage.getItem('styleid');
    var roomid = localStorage.getItem('roomid');
    var framecolorid = localStorage.getItem('framecolorid');

    if (roomid && styleid && framecolorid){
            var height = $('#height').val();
            var width = $('#width').val();
            var glass = $('#glass').is(':checked');
            var grid = $('#grid').val();
            var exterior = $('#exterior').val();
            var tempered = $('#tempered').is(':checked');

            if (height && width){
                $("#spinner").addClass("loading");

                var cartData = [];

                cartData = JSON.parse(localStorage.getItem('cart_items'));
                cartData = cartData ? cartData : [];

                $.ajax({

                type : 'get',
                url : '/shop/check-dimension/',
                data : {
                    'height':height,
                    'width': width,
                    'styleid': styleid
                },
                success: function(result){

                if (result.height_error){
                    $("#error").append(result.error);
                    $("#spinner").removeClass("loading");
                } else{

//                       $('input[name=width]').val(result.width);
//                       $('input[name=height]').val(result.height);
                       localStorage.removeItem("roomid");
                        localStorage.removeItem("styleid");
                        localStorage.removeItem("framecolorid");
                        localStorage.removeItem("window_name");

                    data = {
                    'height': result.height,
                    'width' : result.width,
                    'real_height': height,
                    'real_width' : width,
                    'glass': glass,
                    'grid' : grid,
                    'styleid': styleid,
                    'roomid': roomid,
                    'framecolorid' : framecolorid,
                    'tempered' : tempered,
                    'exterior' : exterior,

                    }
                    $("#message").empty();
                    $("#error").empty();
                    cartData.push(data);
                    localStorage.setItem('cart_items',JSON.stringify(cartData));
                    $("#message").append('Your window was added to your cart');
                    $('#height').attr('readonly', true);
                    $('#width').attr('readonly', true);
                    $("#spinner").removeClass("loading");
                    $('#exampleModal').modal('show');
                    $('#itemmsg').removeAttr("hidden");
                    }}
                 })
             }else{

             $("#message").empty();
                $("#error").empty();

             $("#error").append('Height Width Required');
             }
        }else{

        var cart_items = localStorage.getItem('cart_items');
        if (cart_items){
            $("#message").empty();
            $("#error").empty();
            $('#itemmsg').removeAttr("hidden");
            $("#message").append('Your window already added to your cart.');
            $('#exampleModal').modal('show');

        }else{
            document.location.href = "/shop/"
        }

        }

}

function saveitemfirst(){
    $("#message").empty();
    $("#error").empty();
    var styleid = localStorage.getItem('styleid');
    var roomid = localStorage.getItem('roomid');
    var framecolorid = localStorage.getItem('framecolorid');
    var csrftoken = $('#csrftoken').val();

    if (roomid && styleid && framecolorid){
        var height = $('#height').val();
        var width = $('#width').val();
        var glass = $('#glass').is(':checked');
        var grid = $('#grid').val();
        var exterior = $('#exterior').val();
        var tempered = $('#tempered').is(':checked');

        if (height && width){
            $("#spinner").addClass("loading");

            var cartData = [];

            cartData = JSON.parse(localStorage.getItem('cart_items'));
            cartData = cartData ? cartData : [];

            $.ajax({

            type : 'get',
            url : '/shop/check-dimension/',
            data : {
                'height':height,
                'width': width,
                'styleid': styleid
            },
            success: function(result){

            if (result.height_error){
                $("#error").append(result.error);
                $("#spinner").removeClass("loading");
            } else{

//                   $('input[name=width]').val(result.width);
//                   $('input[name=height]').val(result.height);
                   localStorage.removeItem("roomid");
                    localStorage.removeItem("styleid");
                    localStorage.removeItem("framecolorid");
                    localStorage.removeItem("window_name");

                data = {
                'height': result.height,
                'width' : result.width,
                'real_height': height,
                'real_width' : width,
                'glass': glass,
                'grid' : grid,
                'styleid': styleid,
                'roomid': roomid,
                'framecolorid' : framecolorid,
                'tempered' : tempered,
                'exterior' : exterior,

                }

                cartData.push(data);
                localStorage.setItem('cart_items',JSON.stringify(cartData));

                    $.ajax({
                        type:'post',
                          url : '/shop/saveitem/',
                        data: {

                            'cart_items' : localStorage.getItem('cart_items'),
                            'csrfmiddlewaretoken' : csrftoken
                            },

                        success: function(result){

                            if(result.success == true){
                            $("#message2").empty();

                                localStorage.removeItem("cart_items");
                                $("#message2").append('Your window was added to your cart');
                                $("#spinner").removeClass("loading");
                                $('#height').attr('readonly', true);
                                 $('#width').attr('readonly', true);
                                 $('#itemmsg').removeAttr("hidden");
                                $('#exampleModal2').modal('show');
                            }else{
                                document.location.href = "/shop/"
                            }
                        }});
                    }}
             })
         }else{

         $("#error").empty();
         $("#error").append('Height and Width Required');
         }
    }else{
        $("#message2").empty();
        $('#itemmsg').removeAttr("hidden");
        $("#message2").append('Your window already added to your cart.');
        $('#exampleModal2').modal('show');
        }
}


function savecart(){

    $("#modalrequiered").empty();

    var name = $('#name').val();
    var email = $('#email').val();
    var pincode = $('#pincode').val();
    var number = $('#number').val();
    var csrftoken = $('#csrftoken').val();
    var cart_items = localStorage.getItem('cart_items');

    if (name && email && pincode){

    $('#emailModal').modal('hide');
    $("#spinner").addClass("loading");

		$.ajax({
		type:'post',
		data: {
			'name': name,
			'email' : email,
			'pincode' : pincode,
			'number' : number,
			'cart_items' : cart_items,
			'csrfmiddlewaretoken' : csrftoken
			},

		success: function(result){

		if(result.success == true){
		    localStorage.removeItem("cart_items");
            document.location.href = "/shop/cart/";
		}else{

            document.location.href = "/shop/"
		}
		}});
    }else{
        $("#modalrequiered").append('Field required please enter value');
        };
};

function updatecart(){

    $("#modalrequiered").empty();
    $("#error").empty();
    var id = $('#id').val();
    var height = $('#height').val();
    var width = $('#width').val();
    var glass = $('#glass').is(':checked');
    var grid = $('#grid').val();
    var exterior = $('#exterior').val();
    console.log(exterior)
    var tempered = $('#tempered').is(':checked');
    var csrftoken = $('#csrftoken').val();

    if (height && width){

                $("#spinner").addClass("loading");
                $.ajax({
                type : 'post',
                url : '/shop/window-dimensions/',
                data : {
                    'height':height,
                    'width': width,
                    'id': id,
                    'glass': glass,
                    'grid': grid,
                    'exterior': exterior,
                    'tempered': tempered,
                    'edit': true,
                    'csrfmiddlewaretoken' : csrftoken
                },
                success: function(result){

                if (result.height_error){
                    $("#error").append(result.error);
                    $("#spinner").removeClass("loading");
                } else{

                    if (result.success == true){

                        document.location.href = "/shop/cart/";
                    } else{

                    $("#item_error").append('Item not update');

                    }


                    }}
                 })
             }
    else{

    $("#error").append('Height Width Required');
    }

    };


function jaja(){

    $("#modalrequiered").empty();

    var name = $('#name').val();
    var email = $('#email').val();
    var pincode = $('#pincode').val();
    var number = $('#number').val();
    var csrftoken = $('#csrftoken').val();


    if (name && email && pincode){

        $('#exampleModal').modal('hide');
        $("#spinner").addClass("loading");

            $.ajax({
            type:'post',


            success: function(result){
                location.reload();
            }
            });
        }else{
            $("#modalrequiered").append('Field required please enter value');
            };
    };


 function showbrochure(){
			$('#showbrochure').modal('hide');
			$('#showbrochure').modal('show');
		};

		function showTermsAndCondition() {
			$('#showTermsAndCondition').modal('show');
		}

		function orderproccess() {
			$('#orderproccess').modal('show');
		}
		function dealerdetails() {
			$('#dealerdetails').modal('show');
		}

		function agrees(){
			if($('#i_gree').is(":checked")){

				$('#agree').removeAttr("hidden");
			}else{

				$("#agree").attr("hidden",true);
			}

		};
		function Buy(){
		$('#showTermsAndCondition').modal('hide');
		$('#showTermsAndCondition').modal('show');


	}
	function agreed(){
	$('#showTermsAndCondition').modal('hide');

		$('#detailModal').modal('hide');
		$('#detailModal').modal('show');
	}

function addHyphen (element) {
    	let ele = document.getElementById(element.id);
        ele = ele.value.split('-').join('');
        var n = ele.length;
        if (n > 9){


            if(n > 10){
                var res = ele.charAt(9);
                ele = ele.substring(0, ele.length - 1);
                ele = ele.substring(0, ele.length - 1);
                let finalVal = ele.match(/.{1,3}/g).join('-');

                finalVal = finalVal+res;
                document.getElementById(element.id).value = finalVal;
            }else{
                var res = ele.charAt(9);
                 ele = ele.substring(0, ele.length - 1);
                 let finalVal = ele.match(/.{1,3}/g).join('-');
//                finalVal = finalVal.substring(0, finalVal.length - 1);
                 finalVal = finalVal+res;
                document.getElementById(element.id).value = finalVal;
                }
        }else{
             let finalVal = ele.match(/.{1,3}/g).join('-');
             document.getElementById(element.id).value = finalVal;
        }
    }

    		function hideItme(id,price){
			$("#payment").empty();
			$("#H-"+id).hide();
			$("#PH-"+id).hide();
			$("#S-"+id).attr("hidden",false);
			$("#PS-"+id).attr("hidden",false);
			$("#T-"+id).attr("hidden",false);
			$("#S-"+id).show();
			$("#T-"+id).show();
			$("#PS-"+id).show();
			var av = $('#total_prices').val().replace(/,/g, '');
			var total_prices = parseFloat(av);

			push_price = Number(total_prices - price).toFixed(2);
			$('#total_prices').val("");
			push_price = push_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			$("#total_prices").val(push_price);
			$('#payment').append(push_price);
		};

		function showItme(id,price){
		$("#payment").empty();
			$("#H-"+id).show();
			$("#PH-"+id).show();
			$("#S-"+id).hide();
			$("#T-"+id).hide();
			$("#PS-"+id).hide();
			var total_prices = $('#total_prices').val().replace(/,/g, '');
<!--			var total_prices = $('#total_prices').val();-->
			add = parseFloat(total_prices) + price;
			push_price = Number(add).toFixed(2);
			push_price = push_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			$("#total_prices").val(push_price);
			$('#payment').append(push_price);
		};

	function cart(){
		$('#exampleModal').modal('hide');
		$('#emailModal').modal('show');
	}

	function showvideo(){
		$('#video').modal('hide');
		$('#video').modal('show');
	}
	function help(name){
		$('#'+name).modal('hide');
		$('#'+name).modal('show');
	}

	$(document).ready(function(){

    var window_name = localStorage.getItem('window_name');
    if (window_name == 'Sliding Patio Door' ){
    	$(".requesthidden").attr("hidden",true);

    }
});

function decreaseValue(id) {
		var value = document.getElementsByClassName('number-'+id)[0].value;
		value = isNaN(value) ? 0 : value;
		value < 1 ? value = 1 : '';
		value--;
		document.getElementsByClassName('number-'+id)[0].value = value;
	}