from django.db import models


class Option(models.Model):
    name = models.CharField(max_length=100, blank=False)
    value = models.TextField(blank=True, null=True, default=None)

    class Meta:
        db_table = 'crm_system_options'

    def __str__(self):
        return self.name
