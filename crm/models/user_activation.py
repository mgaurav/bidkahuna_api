from django.db import models
from django.utils.crypto import get_random_string

from crm.models import User, settings


class UserActivation(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    code = models.CharField(blank=False, max_length=64)
    user = models.ForeignKey(User)

    def __str__(self):
        return 'RowId' + str(self.id)

    @staticmethod
    def make_code():
        return get_random_string(length=64)

    def get_url(self):
        return settings.HOST_URL + '/auth/activation/' + str(self.code)
