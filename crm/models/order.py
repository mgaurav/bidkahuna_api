from django.db import models
from django.core.exceptions import ValidationError
from crm.models.mixins.cud_time_fields import CudTimeFieldsModel
from .customer import Customer
from .user import User
from .company import Company

ORDER_STATUS = (
    ("ACTIVE", "Active"),
    ("DRAFT", "Draft"),
    ("NEW", "New"),
    ("CLOSED", "Closed"),
    ("CREATED", "Created")
)


class Order(CudTimeFieldsModel):
    company = models.ForeignKey(Company, related_name="related_orders")
    user = models.ForeignKey(User, related_name="related_orders")
    customer = models.ForeignKey(Customer, null=True)
    status = models.CharField(max_length=7, choices=ORDER_STATUS)
    checklist_id = models.IntegerField(blank=True, null=True)
    checklist_answers = models.TextField(blank=True, null=True,)
    show_review_link_on_pdf = models.BooleanField(default=False)

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return "Order id: " + str(self.id)

    def clean(self):
        if not self.customer or self.customer.company != self.company:
            raise ValidationError(
                {'customer': 'This customer is not assigned to this company.'})

    def get_total_price(self):
        price = 0
        for item in self.related_items.all():
            price = price + item.price
        return price

    def get_total_price_of_discountable_proudcts(self):
        price = 0
        for item in self.related_items.all():
            if item.product.enable_discount:
                price = price + item.price
        return price

    def get_taxable_price(self):
        price = 0
        for item in self.related_items.all():
            if item.product.taxable:
                price = price + item.price
       
        return price

    def save_as_draft(self, user):
        """
        Save the order as Draft.
        Will reset user's active order.
        :param user:
        :return:
        """
        # Set order as Draft.
        self.status = 'DRAFT'
        self.save()
        # Reset User's data
        user.active_order = None
        user.save()

    def set_as_active_for_user(self, user):
        """
        Set this order as current for the user
        :param user:
        :return:
        """
        if user.role == 'CONTRACTOR' or user.id == self.user.id:
            user.active_order = self
            user.save()
            return True
        else:
            raise self.DoesNotExist

    def close_order(self):
        self.status = "CLOSED"
