from django.db import models
from django.core.exceptions import ValidationError

from crm.models.customer_lead_source import CustomerLeadSource
from crm.models.mixins.cud_time_fields import CudTimeFieldsModel
from .prospect_status import ProspectStatus
from .company import Company
from .user import User


class Customer(CudTimeFieldsModel):
    company = models.ForeignKey(Company, related_name="related_customers", on_delete=models.CASCADE)
    prospect = models.ForeignKey(ProspectStatus, blank=True, null=True, default=None, on_delete=models.SET_NULL)
    user = models.ForeignKey(User, null=True, related_name="related_users_customers")
    first_name = models.CharField(max_length=100, null=True, default="")
    last_name = models.CharField(max_length=100, null=True, default="")
    spouse_name = models.CharField(max_length=100, null=True, blank=True)
    spouse_phone = models.CharField(max_length=20, null=True, blank=True)
    email = models.EmailField(blank=True, null=True)
    phone = models.CharField(max_length=20, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    state = models.CharField(max_length=100, blank=True, null=True)
    zip = models.CharField(max_length=5, blank=True, null=True)
    lead_source = models.ForeignKey('crm.CustomerLeadSource', blank=True, null=True, default=None,
                                    on_delete=models.SET_NULL)
    lead_owner = models.ForeignKey('crm.User', blank=True, null=True, default=None, on_delete=models.SET_NULL)

    class Meta:
        db_table = 'crm_customers'

    def clean(self):
        if hasattr(self, 'prospect') and self.prospect:
            if not hasattr(self.prospect, 'company') or self.prospect.company != self.company:
                raise ValidationError(
                    {'prospect': 'This prospect is not assigned to this company.'})

    def full_name(self):
        full_name = ""
        if self.first_name:
            full_name += self.first_name
        if self.last_name:
            full_name += " "+self.last_name
        return full_name

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)
