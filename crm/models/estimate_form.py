from django.db import models
from django.core.exceptions import ValidationError
from .user import User
from crm.models.mixins.cud_time_fields import CudTimeFieldsModel


class EstimateForm(CudTimeFieldsModel):
    form_data = models.CharField(max_length=4000, null=True, default="")
    user = models.ForeignKey(User, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    class Meta:
        db_table = 'crm_estimate_forms'

    def __str__(self):
        return "{}".format(self.id)