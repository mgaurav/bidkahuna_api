from django.db import models
from django.db.models import Q
from django.utils.datetime_safe import datetime


class CudQuerySet(models.QuerySet):
    def get_update_summary(self, timestamp=None):
        return self.filter(
            Q(created_at__gt=timestamp) | Q(updated_at__gt=timestamp) | Q(deleted_at__gt=timestamp))


class CudActiveQuerySet(models.QuerySet):
    def active(self):
        return self.filter(deleted_at__isnull=True)


class CudTimeFieldsModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    objects = CudQuerySet.as_manager()
    active_objects = CudActiveQuerySet.as_manager()

    class Meta:
        abstract = True

    def delete(self, soft_delete=False, *args, **kwargs):
        if soft_delete:
            self.deleted_at = datetime.now()
            return self.save()
        else:
            return super(CudTimeFieldsModel, self).delete(*args, **kwargs)
