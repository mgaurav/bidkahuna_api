from django.db import models
from paypal.standard.ipn.models import PayPalIPN

from crm.models import Company


class BillingTransaction(models.Model):
    company = models.ForeignKey(Company, related_name='related_billing_transactions')
    comment = models.CharField(max_length=100)
    old_company_status = models.NullBooleanField(blank=True, null=True)
    new_company_status = models.NullBooleanField(blank=True, null=True)
    ipn = models.OneToOneField(PayPalIPN, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    class Meta:
        db_table = 'crm_billing_transactions'
        ordering = ['-created_at']

    def __str__(self):
        return 'Transaction for Company ID:' + str(self.company_id)
