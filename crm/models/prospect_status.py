from django.db import models

from .company import Company


class ProspectStatus(models.Model):
    company = models.ForeignKey(Company, related_name='related_prospect_statuses')
    title = models.CharField(max_length=50)

    class Meta:
        db_table = 'crm_company_prospect_status'
