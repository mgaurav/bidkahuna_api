from django.db import models
from crm.models.mixins.cud_time_fields import CudTimeFieldsModel

class ChecklistQuestions(CudTimeFieldsModel):
    question = models.CharField(max_length=255)
    checklist_id = models.IntegerField(blank=True, null=True)
    related_product_id = models.IntegerField(blank=True, null=True)
    worn_related_product_id = models.IntegerField(blank=True, null=True)
    is_section = models.BooleanField(default=False)
    show_na_button = models.BooleanField(default=False)
    position = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)


    def __str__(self):

        return self.question