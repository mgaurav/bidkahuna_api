from biddingapp import settings
from django.contrib.staticfiles.storage import staticfiles_storage
from django.db import models
from crm.models import Product


def get_file_path(instance, filename):
    path = 'company/' + str(instance.product.company.pk) + '/products/' + str(instance.product.pk) + '_' + filename
    return path


class ProductImage(models.Model):
    product = models.ForeignKey(Product, related_name="related_images", on_delete=models.CASCADE)
    image = models.ImageField(upload_to=get_file_path)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    def url(self):
        image = self.image
        if not image:
            logo = settings.HOST_URL + staticfiles_storage.url('img/no-logo.png')
        else:
            logo = settings.HOST_URL + image.url
        return logo

    def __str__(self):
        return self.product.name
