from django.db import models

from crm.models import Order, CudTimeFieldsModel


class OrderActionAbstractModel(models.Model):
    order = models.ForeignKey(Order, related_name='related_actions')
    date = models.DateTimeField(blank=False)
    description = models.TextField(blank=False)

    class Meta:
        abstract = True
        db_table = 'crm_order_action'
        ordering = ['-date']

    def __str__(self):
        return 'Action ID:' + str(self.id)


class OrderAction(OrderActionAbstractModel, CudTimeFieldsModel):
    pass
