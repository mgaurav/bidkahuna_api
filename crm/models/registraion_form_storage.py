from django.core.mail import EmailMessage
from django.db import models
from django.template import Context
from django.template.loader import get_template

from biddingapp import settings


class RegistrationFormStorage(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField(max_length=50)
    phone = models.CharField(max_length=20)
    details = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'crm_registration_form_storage'

    def send_to_email(self):
        ctx = {
            'form': self
        }

        subject = 'New message from the registration form'
        body = get_template('email/registration_form.html').render(Context(ctx))

        new_email = EmailMessage(
            subject,
            body,
            'Bid Kahuna' + '<' + settings.EMAIL_HOST_USER + '>',
            [settings.EMAIL_INCOMING]
        )
        new_email.content_subtype = 'html'
        new_email.send()
