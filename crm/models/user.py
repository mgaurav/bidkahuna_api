from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ObjectDoesNotExist
from django.db import models

USER_ROLES = (
    ('SUPERADMIN', 'Superadmin'),
    ('CONTRACTOR', 'Contractor'),
    ('SALESPERSON', 'Salesperson')
)


class User(AbstractUser):
    role = models.CharField(max_length=11, choices=USER_ROLES, null=False)
    show_checklist = models.BooleanField(default=False)
    show_custom_form = models.BooleanField(default=False)
    review_link = models.CharField(max_length=200,blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    active_order = models.ForeignKey('crm.Order', related_name="related_active_order", on_delete=models.SET_NULL, null=True, blank=True)

    class Meta(AbstractUser.Meta):
        app_label = "crm"

    def is_sales(self):
        return self.role == 'SALESPERSON'

    def is_contractor(self):
        return self.role == 'CONTRACTOR'

    @property
    def full_name(self):
        return self.get_full_name()

    @property
    def company(self):
        if self.role == 'CONTRACTOR':
            try:
                company = self.owner_company
            except ObjectDoesNotExist:
                from .company import Company
                company = Company.objects.create(owner=self)
            return company
        if self.role == 'SALESPERSON':
            return self.related_company.get()

    @property
    def group(self):
        return self.role

    def get_user_group(self):
        try:
            group = self.groups.all()[0]
            return group
        except:
            return False

    def delete_with_assign(self, assign_to=None):
        company = self.company
        target_user = self.company.users.all().get(pk=assign_to)
        # Migrate customers
        customers = self.related_users_customers.all()
        for customer in customers:
            customer.user = target_user
            customer.save()
        # Migrate customers notes
        notes = self.related_customers_notes.all()
        for note in notes:
            note.author = target_user
            note.save()
        # Migrate orders
        orders = self.related_orders.all()
        for order in orders:
            order.user = target_user
            order.save()
        return self.delete()
