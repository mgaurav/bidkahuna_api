from django.db import models
from django.core.exceptions import ValidationError

from crm.models.mixins.cud_time_fields import CudTimeFieldsModel


class CustomForm(CudTimeFieldsModel):
    job_id = models.CharField(max_length=100, null=True, default="")
    form_data = models.CharField(max_length=4000, null=True, default="")

    class Meta:
        db_table = 'crm_custom_forms'

    def __str__(self):
        return "{}".format(self.job_id)
