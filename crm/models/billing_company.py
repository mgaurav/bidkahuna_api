from django.db import models

from crm.models import CudTimeFieldsModel, Company
from crm.models.billing_plans import BillingPlan


class BillingCompany(CudTimeFieldsModel):
    company = models.OneToOneField(Company, related_name="data_billing")
    used_plan = models.ForeignKey(BillingPlan, null=True, related_name="related_company")
    is_subscribe = models.BooleanField(default=False)
    last_transaction = models.DateTimeField(blank=True, null=True)
    next_check = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'crm_billing_company'

    def __str__(self):
        return 'Billing Company ID:' + str(self.company_id)
