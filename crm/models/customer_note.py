from django.db import models

from crm.models.mixins.cud_time_fields import CudTimeFieldsModel
from .customer import Customer
from .user import User


class CustomerNoteAbstract(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name="related_notes")
    author = models.ForeignKey(User, related_name='related_customers_notes')
    text = models.TextField(null=True)

    class Meta:
        db_table = 'crm_customers_notes'
        ordering = ['-created_at']
        abstract = True


class CustomerNote(CustomerNoteAbstract, CudTimeFieldsModel):
    pass
