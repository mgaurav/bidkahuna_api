from django.db import models


class CustomerLeadSource(models.Model):
    value = models.CharField(max_length=100, blank=False)
    company = models.ForeignKey('crm.Company', related_name='related_customer_lead_sources')
