from django.db import models

STATUSES = (
    ('SUSPECT', 'Suspect'),
    ('PROSPECT', 'Prospect'),
    ('CUSTOMER', 'Customer'),
)


class Contact(models.Model):
    use_in_migrations = True

    contact_first_name = models.CharField(max_length=100)
    contact_last_name = models.CharField(max_length=100)
    spouse_first_name = models.CharField(max_length=100)
    spouse_last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    zip = models.CharField(max_length=100)
    home_phone = models.CharField(max_length=100)
    cell_phone_1 = models.CharField(max_length=100)
    cell_phone_2 = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    status = models.CharField(max_length=8, choices=STATUSES)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
