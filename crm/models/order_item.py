from django.db import models
from django.core.exceptions import ValidationError
import json
from .product import Product
from .order import Order
from crm.models.mixins.cud_time_fields import CudTimeFieldsModel


class OrderItem(CudTimeFieldsModel):
    _cache_data = None

    order = models.ForeignKey(Order, related_name="related_items", on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2, blank=True, default=False)
    product = models.ForeignKey(Product)
    data = models.TextField(blank=True, null=True, serialize=True)
    index = models.IntegerField(blank=True, null=True)
    note = models.CharField(max_length=255, blank=True, null=True)
    dimension_notes = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return "Order item: " + str(self.id)

    def clean(self):
        if self.product.company != self.order.company:
            raise ValidationError(
                {'product': 'This product is not assigned to this company.'})

    def set_data(self, data):
        self.data = json.dumps(data)

    def get_data(self):
        if self._cache_data is None:
            try:
                rows = json.loads(self.data)
                # Sort measurement Keys
                # @see https://trello.com/c/VNMVIh73/128-sales-order-email
                index_w = None
                index_h = None
                for key, row in enumerate(rows):
                    target_key = row.get('measurement_key')
                    if target_key == 'width':
                        index_w = key
                    elif target_key == 'height':
                        index_h = key
                if index_w is not None and index_h is not None:
                    if index_w > index_h:
                        rows[index_w], rows[index_h] = rows[index_h], rows[index_w]
                self._cache_data = rows
            except:
                self._cache_data = []
        return self._cache_data

    @property
    def item_details(self):
        """
        Allias for API
        :return:
        """
        return self.get_data()
