from django.db import models

from crm.models import Product, Category


class ProductCategories(models.Model):
    product = models.ForeignKey(Product, related_name='related_categories')
    category = models.ForeignKey(Category)
    subcategory = models.ForeignKey(Category)

    class Meta:
        db_table = 'crm_product_categories'
