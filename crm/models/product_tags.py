from django.db import models

from crm.models.mixins.cud_time_fields import CudTimeFieldsModel

from crm.models import Company, Product


class ProductTag(CudTimeFieldsModel):
    tag = models.CharField(max_length=100)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    product = models.ManyToManyField(Product, related_name='related_tags')
