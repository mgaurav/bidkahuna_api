from django.db import models
from django.core.validators import RegexValidator
from django.contrib.staticfiles.storage import staticfiles_storage

from crm.models.user import User
from crm.validators.mime_type_validator import MimeTypeValidator, MIME_TYPE_IMAGES

from biddingapp import settings


def get_file_path(instance, filename):
    path = 'company/' + str(instance.pk) + '/logo/' + filename
    return path


class Company(models.Model):
    name = models.CharField(max_length=100, null=True)
    address = models.CharField(max_length=100, null=True)
    address_city = models.CharField(max_length=100, null=True)
    address_state = models.CharField(max_length=100, null=True)
    address_zip = models.CharField(max_length=5, null=True)
    phone = models.CharField(max_length=255, blank=True, null=True)
    website = models.CharField(max_length=255,
                               validators=[
                                   RegexValidator(regex='^(www).[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$',
                                                  message='URL format: www.example.com')],
                               blank=True,
                               null=True)
    email_id = models.EmailField(max_length=255,
                                validators=[RegexValidator(regex='(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)',
                                                                message='Email format: name@email.com')],
                                 blank=True, null=True)
    terms = models.TextField(null=True, blank=True)
    agreements = models.TextField(null=True, blank=True)
    email_payment = models.EmailField(blank=True, null=True)
    _logo = models.FileField(db_column='logo', upload_to=get_file_path, null=True, blank=True,
                             validators=[MimeTypeValidator(MIME_TYPE_IMAGES)], serialize=False
                             )
    owner = models.OneToOneField(User, related_name='owner_company', null=True, blank=True)
    users = models.ManyToManyField(User, related_name='related_company')
    is_active = models.BooleanField(default=False)
    display_signature_boxes = models.BooleanField(default=True)
    has_open_discount = models.BooleanField(default=False)
    monthly_payment_calc = models.BooleanField(default=False)
    months = models.IntegerField(blank=True, null=True)
    interest = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    enable_invoice_numbering = models.BooleanField(default=False)
    last_invoice_number = models.IntegerField(blank=True, null=True)
    enable_tax = models.BooleanField(default=False)
    tax_rate = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)

    def __str__(self):
        if self.name:
            return self.name
        else:
            return 'no name company'

    def logo(self):
        logo = self._logo
        if not logo:
            logo = settings.HOST_URL + staticfiles_storage.url('img/no-logo.png')
        else:
            logo = settings.HOST_URL + logo.url
        return logo

    @property
    def billing(self):
        if not hasattr(self, 'data_billing'):
            from crm.models import BillingCompany
            return BillingCompany.objects.create(company=self, used_plan_id=1)
        else:
            return self.data_billing
