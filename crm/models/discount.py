from django.db import models

from crm.models.mixins.cud_time_fields import CudTimeFieldsModel
from .company import Company

DISCOUNT_TYPE = (
    ("PERCENTAGE", "Percentage"),
    ("FIXED", "Fixed"),
)


class Discount(CudTimeFieldsModel):
    name = models.CharField(max_length=100)
    company = models.ForeignKey(Company, null=True, related_name="related_discounts")
    type = models.CharField(max_length=10, choices=DISCOUNT_TYPE)
    value = models.DecimalField(max_digits=10, decimal_places=2)
    is_active = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    def get_amount_title(self):
        if self.type == "PERCENTAGE":
            return '%' + str(self.value)
        else:
            return '$' + str(self.value)
