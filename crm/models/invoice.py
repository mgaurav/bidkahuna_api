import json

from decimal import Decimal
from django.core.mail import EmailMessage
from django.template import Context

from django.db import models
from django.template.loader import get_template
from django.utils.crypto import get_random_string
from django.utils.datetime_safe import datetime

from biddingapp import settings
from .discount import Discount
from .company import Company
from .order import Order
from datetime import date
from math import log, ceil

INVOICE_TYPE = (
    ('INVOICE', 'Invoice'),
    ('ESTIMATE', 'Estimate')
)


def make_hash(length=32):
    return get_random_string(length=length)


class ActiveInvoiceManager(models.Manager):
    def get_queryset(self):
        return super(ActiveInvoiceManager, self) \
            .get_queryset().filter(deleted_at__isnull=True)


class DeletedInvoiceManager(models.Manager):
    def get_queryset(self):
        return super(DeletedInvoiceManager, self) \
            .get_queryset().filter(deleted_at__isnull=False)


class InvoiceDiscountMixin(object):
    def get_discounts(self):
        return self.p_data.get('store_discounts', [])

    def set_discounts(self, value):
        data = self.p_data
        data['store_discounts'] = value
        self.p_data = data


class Invoice(models.Model, InvoiceDiscountMixin):

    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    uid = models.CharField(max_length=30, blank=True, null=True, unique=True)
    order = models.OneToOneField(Order, on_delete=models.CASCADE, related_name='related_invoice')
    hash = models.CharField(max_length=32, unique=True, blank=True, null=True)
    data = models.TextField(blank=True, null=True)
    is_approved = models.BooleanField(default=False)
    discounts = models.ManyToManyField(Discount, related_name='related_invoices')
    type = models.CharField(max_length=8, choices=INVOICE_TYPE, null=True)
    price_itemize = models.BooleanField(default=True)
    is_agreement = models.BooleanField(default=True)
    signature_customer = models.TextField(null=True, blank=True)
    signature_user = models.TextField(null=True, blank=True)
    invoice_notes = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    tax = models.DecimalField(max_digits=10, decimal_places=2, blank=True,null=True)
    objects = models.Manager()
    active_objects = ActiveInvoiceManager()
    deleted_objects = DeletedInvoiceManager()

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return 'Invoice: ' + str(self.id)

    @property
    def p_data(self):
        try:
            result = json.loads(self.data)
        except:
            result = {}
        return result

    @p_data.setter
    def p_data(self, value):
        self.data = json.dumps(value)

    def save(self, *args, **kwargs):
        """
        Override Save Method.

        Make hash for invoice before saving model.
        :param args:
        :param kwargs:
        :return:
        """
        if not self.hash:
            self.hash = make_hash()
        if not self.uid:
            if self.company.enable_invoice_numbering:
                self.uid = self.company.last_invoice_number
                self.company.last_invoice_number = self.company.last_invoice_number + 1
                self.company.save()
            else:
                self.uid = '#' + date.today().strftime("%y%m%d") + '/' \
                           + str(self.company.pk) + '-' \
                           + str(self.order.user.pk) + '/' \
                           + str(self.order.pk)
        return super(Invoice, self).save(*args, **kwargs)

    def delete(self, soft_delete=False, *args, **kwargs):
        """
        Override Delete Method.

        Implementations the Soft Delete method
        :param soft_delete:
        :param args:
        :param kwargs:
        :return:
        """
        if soft_delete:
            self.deleted_at = datetime.now()
            return self.save()
        else:
            return super(Invoice, self).delete(*args, **kwargs)

    def url_pdf(self):
        return settings.HOST_URL + '/api/pdf/' + str(self.hash)

    def subtotal_price(self):
        return self.order.get_total_price()

    def subtotal_price_of_discountable_products(self):
        return self.order.get_total_price_of_discountable_proudcts()

    def discount_price(self):
        discounts = self.get_discounts()
        discount_value = 0
        for discount in discounts:
            if discount.get('type') == "PERCENTAGE":
                value = Decimal(discount.get('value'))
                discount_value += round(self.subtotal_price_of_discountable_products() / 100 * value, 2)
            elif discount.get('type') == "FIXED":
                if(self.subtotal_price_of_discountable_products() > float(discount.get('value'))):
                    discount_value += Decimal(discount.get('value'))
            else:
                discount_value = 0
        return discount_value

    def discount_price_abs(self):
        return abs(self.discount_price())

    def discount_name(self):
        discounts = self.get_discounts()
        discount_name = 'Discounts'
        for discount in discounts:
             discount_name = discount.get('name')
        return discount_name

    def total_price(self):
        
        return self.subtotal_price() - self.discount_price()  + self.tax_amount()

    def enable_tax(self):
        return self.company.enable_tax

    def tax_amount(self):
        print('here')
        print('order price',self.order.get_taxable_price())
        print('company Price',self.company.tax_rate)
        return self.order.get_taxable_price() * self.company.tax_rate / 100
        # return round(self.order.get_taxable_price() * self.company.tax_rate / 100, 2)

    def make_data(self):
        pass

    def related_discounts(self):
        return self.discounts

    def send_to_email(self, email, request):
        print(self.url_pdf)

        ctx = {
            'url_pdf': self.url_pdf(),
            'company_name': self.company.name,
            'sales_email': self.company.owner.email
        }

        subject = 'Proposal'
        body = get_template('email/order_estimate.html').render(Context(ctx))

        new_email = EmailMessage(
            subject,
            body,
            self.company.name + '<' + settings.EMAIL_HOST_USER + '>',
            [email],
            reply_to=[self.company.name + '<' + self.order.user.email + '>']
        )
        new_email.content_subtype = 'html'
        return bool(new_email.send())
