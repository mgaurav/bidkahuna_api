from django.db import models


class MeasurementType(models.Model):
    name = models.CharField(max_length=100, blank=False, default=None)
    title = models.CharField(max_length=100)
    class_name = models.CharField(max_length=30, blank=False, default=None)
    ng_order_component = models.CharField(max_length=200, blank=False, default=None)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    class Meta:
        db_table = 'crm_system_measurements'

    def __str__(self):
        return self.name
