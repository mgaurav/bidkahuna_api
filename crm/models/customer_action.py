from django.db import models

from crm.models.mixins.cud_time_fields import CudTimeFieldsModel
from .customer import Customer
from .user import User


class CustomerActionsAbstract(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name="related_actions")
    user = models.ForeignKey(User, related_name='related_customers_actions')
    date = models.DateTimeField(blank=False)
    description = models.TextField(null=True)

    class Meta:
        abstract = True


class CustomerAction(CustomerActionsAbstract, CudTimeFieldsModel):
    class Meta:
        db_table = 'crm_customers_actions'
        ordering = ['-date']
