from django.db import models
from crm.models import Company
from crm.models.mixins.cud_time_fields import CudTimeFieldsModel


class Brand(CudTimeFieldsModel):
    """
    Brand Model
    """
    name = models.CharField(max_length=100)
    company = models.ForeignKey(Company, related_name='related_brands', on_delete=models.CASCADE)

    class Meta:
        unique_together = [
            ("name", "company")
        ]

    def __str__(self):
        return self.name
