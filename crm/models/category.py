from django.contrib.staticfiles.storage import staticfiles_storage
from django.db import models

from biddingapp import settings
from .company import Company


def get_file_path(instance, filename):
    path = 'company/' + str(instance.company.pk) + '/categories/' + filename
    return path


class Category(models.Model):
    company = models.ForeignKey(Company, related_name='related_categories', on_delete=models.CASCADE, null=True)
    order = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=100)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='children')
    image = models.ImageField(upload_to=get_file_path, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ['order']

    @property
    def options(self):
        return self._options

    def image_url(self):
        image = self.image
        if not image:
            logo = settings.HOST_URL + staticfiles_storage.url('img/no-logo.png')
        else:
            logo = settings.HOST_URL + image.url
        return logo

    def __str__(self):
        return self.name
