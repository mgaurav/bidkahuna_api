import json

from django.core.exceptions import ValidationError
from django.db import models

from biddingapp import settings
from crm.models.mixins.cud_time_fields import CudTimeFieldsModel
from .measurement_type import MeasurementType
from .brand import Brand
from .company import Company
from .category import Category

from crm.validators.mime_type_validator import MimeTypeValidator, MIME_TYPE_PDF


def validate_before_publish(value):
    pass


def get_brochure_file_path(instance, filename):
    path = 'company/' + str(instance.company.pk) + '/brochures/' + str(instance.pk) + '_' + filename
    return path


class Product(CudTimeFieldsModel):
    name = models.CharField(max_length=100)
    order = models.IntegerField(blank=True, null=True)
    details = models.TextField(blank=True, null=True)
    warranty = models.TextField(blank=True, null=True)
    brand = models.ForeignKey(Brand, blank=True, null=True)
    company = models.ForeignKey(Company, related_name="related_products")
    category = models.ForeignKey(Category, blank=True, null=True, related_name="related_categories_products")
    subcategory = models.ForeignKey(Category, blank=True, null=True, related_name='subcategory')
    brochure = models.FileField(upload_to=get_brochure_file_path,
                                null=True,
                                blank=True,
                                validators=[MimeTypeValidator(MIME_TYPE_PDF)])
    data = models.TextField(blank=True, null=True, serialize=True)
    is_publish = models.BooleanField(blank=False, default=False, validators=[validate_before_publish])
    taxable = models.BooleanField(blank=False, default=True)
    enable_discount = models.BooleanField(blank=False, default=True)
    show_dimension_note = models.BooleanField(blank=False, default=False)
    measurement = models.ForeignKey(MeasurementType, blank=True, null=True)
    related_products = models.ManyToManyField('self', related_name="related_products")
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    def clean(self, *args, **kwargs):
        if self.is_publish and not self.measurement:
            raise ValidationError(
                {
                    'Form error': 'The product can not be published until the price grid is filled out. Product not saved.'})

        if self.is_publish and not self.category_id:
            raise ValidationError(
                {
                    'Form error': 'The product can not be published until the category is filled out. Product not saved.'})

        # if self.is_publish and not self.subcategory_id:
        #     raise ValidationError(
        #         {
        #             'Form error': 'The product can not be published until the subcategory is filled out. Product not saved.'})

        # @deprecated 07 Feb / Task: Product Brand
        # if self.is_publish and not self.brand_id:
        #     raise ValidationError(
        #         {
        #             'Form error': 'The product can not be published until the brand is filled out. Product not saved.'})

        if self.brand and hasattr(self, 'company') and self.brand.company != self.company:
            raise ValidationError(
                {'Brand': 'This brand is not assigned to this company.'})

        if self.category and hasattr(self, 'company') and self.category.company != self.company:
            raise ValidationError(
                {'Brand': 'This category is not assigned to this company.'})

        if self.subcategory and hasattr(self, 'company') and self.subcategory.company != self.company:
            raise ValidationError(
                {'Brand': 'This subcategory is not assigned to this company.'})

    def brochure_url(self):
        brochure = self.brochure
        if not brochure:
            brochure_url = False
        else:
            brochure_url = settings.HOST_URL + brochure.url
        return brochure_url

    def set_data(self, data):
        self.data = json.dumps(data)

    def get_data(self):
        return json.loads(self.data)

    def product_data(self):
        try:
            if self.data:
                data_validated = self.check_modification_group(json.loads(self.data))
                return data_validated
            else:
                return ()
        except Exception as e:
            return ()

    @staticmethod
    def check_modification_group(data):
        for group in data.get('priceModificationGroups'):
            for attribute in group.get('attributes'):
                for key, value in enumerate(attribute.get('values', [])):
                    if value is None:
                        attribute['values'][key] = 0
        return data

    def change_price(self, target_type, target_value):
        """
        Be able to globally change pricing. (Ex. Increase roofing pricing 5%)
        """
        product_data = self.get_data()
        price_groups = product_data.get('priceModificationGroups')

        for key_group, group in enumerate(price_groups):
            if group.get('type') == 'fixed':
                attributes = group.get('attributes')
                for key_attribute, attribute in enumerate(attributes):
                    values = attribute.get('values')
                    for key_value, value in enumerate(values):
                        if value > 0:
                            if target_type == 'INCREASE_PRICE_PERCENT':
                                new_value = value + value / 100 * target_value
                                price_groups[key_group]["attributes"][key_attribute]["values"][key_value] = new_value
        product_data["priceModificationGroups"] = price_groups
        self.set_data(product_data)

    def __str__(self):
        return self.name

    def brand_name(self):
        brand = self.brand
        if not brand:
            brand_name = ''
        else:
            brand_name = brand.name
        return brand_name
