from django.db import models


class BillingPlan(models.Model):
    name = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    description = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        db_table = 'crm_billing_plan'

    def __str__(self):
        return self.name

    def related_company_count(self):
        return self.related_company.count()
