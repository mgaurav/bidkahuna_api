from django.db import models
from crm.models.mixins.cud_time_fields import CudTimeFieldsModel
from crm.models import ChecklistQuestions

class ChecklistQuestionProduct(CudTimeFieldsModel):
    question = models.ForeignKey(ChecklistQuestions, related_name='fail_products', on_delete=models.CASCADE, null=True)
    product_id = models.IntegerField(blank=True, null=True)
    question_type = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.question
