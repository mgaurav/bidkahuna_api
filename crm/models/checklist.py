from django.db import models
from crm.models.mixins.cud_time_fields import CudTimeFieldsModel
from .company import Company


def validate_before_publish(value):
    pass

class Checklist(CudTimeFieldsModel):
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=False)
    published = models.BooleanField(blank=False, default=False,validators=[validate_before_publish])
    company = models.ForeignKey(Company, related_name="related_checklists", null=True, on_delete=models.CASCADE)
    sort_order = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.name
