from django.contrib import admin
from django.contrib.auth.models import Permission

from .invoice import Invoice
from .order import *
from .order_item import *
from .user import *
from .company import *
from .contact import *
from .brand import *
from .product import *
from .category import *
from .product_images import *
from .measurement_type import *
from .product_tags import *
from .discount import *
from .customer import *
from .customer_note import *
from .billing_company import *
from .billing_transaction import *
from .checklist import *
from .checklist_questions import *
from .checklist_question_product import *
from .estimate_form import *

admin.site.register(Category)
admin.site.register(MeasurementType)
admin.site.register(User)
admin.site.register(Company)
admin.site.register(Permission)
admin.site.register(Contact)
admin.site.register(Brand)
admin.site.register(Product)
admin.site.register(ProductImage)
admin.site.register(Order)
admin.site.register(OrderItem)
admin.site.register(ProductTag)
admin.site.register(Invoice)
admin.site.register(Discount)
admin.site.register(Customer)
admin.site.register(CustomerNote)
admin.site.register(BillingCompany)
admin.site.register(BillingTransaction)

admin.site.register(BillingPlan)
admin.site.register(Checklist)
admin.site.register(ChecklistQuestions)
admin.site.register(ChecklistQuestionProduct)
admin.site.register(EstimateForm)

import crm.signals.billing_add_transaction
import crm.signals.create_new_contractor
import crm.signals.paypal_ipn_validated
import crm.signals.update_order_item
