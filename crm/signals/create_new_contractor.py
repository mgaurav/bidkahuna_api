from django.core.mail import EmailMessage
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template import Context
from django.template.loader import get_template

from crm.models import User, settings
from crm.models.user_activation import UserActivation


@receiver(post_save, sender=User)
def billing_add_transaction(sender, instance, created, **kwargs):
    if created and instance.role == 'CONTRACTOR':
        activations = UserActivation.objects.create(user=instance, code=UserActivation.make_code())

        ctx = {
            'url_activation': activations.get_url(),
            'username': instance.username,
            'fullname': instance.get_full_name()
        }

        subject = 'Activate your account on Bid Kahuna'
        body = get_template('email/new_contractor.html').render(Context(ctx))


        new_email = EmailMessage(
            subject,
            body,
            'Bid Kahuna' + '<' + settings.EMAIL_HOST_USER + '>',
            [instance.email]
        )
        new_email.content_subtype = 'html'
        # Disable sending the email when a new contractor is created. Keeping the code as it is since we might need it later.
        # new_email.send()
        pass
