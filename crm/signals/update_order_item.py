from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import datetime

from crm.models import OrderItem


@receiver(post_save, sender=OrderItem)
def update_order_item(sender, instance, created, **kwargs):
    instance.order.updated_at = datetime.now()
    instance.order.save()
