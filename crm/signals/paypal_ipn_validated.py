import json

from django.dispatch import receiver
from paypal.standard.ipn.signals import valid_ipn_received

from crm.models import BillingTransaction


@receiver(valid_ipn_received)
def paypal_ipn_validated(sender, **kwargs):
    # try:
    obj = json.loads(sender.custom)
    company_id = obj.get('company_id')

    transaction = BillingTransaction()
    if sender.txn_type == 'subscr_payment':
        transaction.comment = 'Payment for subscription'
    elif sender.txn_type == 'subscr_signup':
        transaction.comment = 'Subscribed'
    elif sender.txn_type == 'subscr_cancel':
        transaction.comment = 'Cancel subscription'

    transaction.company_id = company_id
    transaction.ipn = sender
    transaction.save()

    # except Exception as e:
    #     print(e)
    #     print(999, sender.custom)
