import logging
from django.db.models.signals import post_save
from django.dispatch import receiver
from dateutil.relativedelta import relativedelta

from crm.models import BillingTransaction


@receiver(post_save, sender=BillingTransaction)
def billing_add_transaction(sender, instance, created, **kwargs):
    company = instance.company
    billing = company.billing

    if created and hasattr(instance, 'ipn') and instance.ipn:
        logger = logging.getLogger('billing')
        logger.info('Create new transaction: %s. Company: %s [%s]',
                    created, company.name, company.id)

        instance.old_company_status = company.is_active
        instance.save()

        if instance.ipn.txn_type == 'subscr_signup':
            billing.is_subscribe = True

        if instance.ipn.txn_type == 'subscr_cancel':
            billing.is_subscribe = False
            billing.next_check = None

        if instance.ipn.txn_type == 'subscr_payment':
            company.is_active = True
            company.save()

            billing.is_subscribe = True
            billing.last_transaction = instance.ipn.payment_date
            billing.next_check = billing.last_transaction + relativedelta(months=1)

        billing.save()
