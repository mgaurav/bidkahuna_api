from django.conf.urls import url

from crm.views import payment

urlpatterns = [
    url(r'cancel/$', payment.payment_cancel),
    url(r'success/$', payment.invoice_success),
]
