from django.conf.urls import url

from crm.views import auth
from django.contrib.auth import views as auth_views

from crm.views.auth import activation_user

urlpatterns = [
    url(r'reminder/$', auth.reminder_password),
    url(r'password_reset_confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
        auth_views.password_reset_confirm,
        {'template_name': 'auth/auth_password_reset.html'},
        name='password_reset_confirm'),
    url(r'password_reset_complete/$',
        auth_views.password_reset_complete,
        {'template_name': 'auth/auth_password_complete.html'},
        name='password_reset_complete'),
    url(r'password_reset/$',
        auth_views.password_reset,
        {'template_name': 'auth/auth_password_reminder.html'}),
    url(r'password_reset/done/$',
        auth_views.password_reset_done,
        {'template_name': 'auth/auth_password_done.html'},
        name='password_reset_done'),
    url(r'activation/(?P<token>.+)/$', activation_user)
]
