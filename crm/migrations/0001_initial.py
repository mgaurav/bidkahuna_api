# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import crm.models.category
import crm.models.checklist
import crm.models.company
import crm.models.invoice
import crm.models.product
import crm.models.product_images
import crm.validators.mime_type_validator
from django.conf import settings
import django.contrib.auth.models
import django.contrib.auth.validators
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
        ('standard', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('first_name', models.CharField(blank=True, max_length=30, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=30, verbose_name='last name')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('role', models.CharField(choices=[('SUPERADMIN', 'Superadmin'), ('CONTRACTOR', 'Contractor'), ('SALESPERSON', 'Salesperson')], max_length=11)),
                ('show_checklist', models.BooleanField(default=False)),
                #('show_custom_form', models.BooleanField(default=False)),
                #('review_link', models.CharField(blank=True, max_length=200, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='BillingCompany',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('is_subscribe', models.BooleanField(default=False)),
                ('last_transaction', models.DateTimeField(blank=True, null=True)),
                ('next_check', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'crm_billing_company',
            },
        ),
        migrations.CreateModel(
            name='BillingPlan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('title', models.CharField(max_length=100)),
                ('description', models.TextField()),
                ('price', models.DecimalField(decimal_places=2, max_digits=10)),
            ],
            options={
                'db_table': 'crm_billing_plan',
            },
        ),
        migrations.CreateModel(
            name='BillingTransaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment', models.CharField(max_length=100)),
                ('old_company_status', models.NullBooleanField()),
                ('new_company_status', models.NullBooleanField()),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
            ],
            options={
                'db_table': 'crm_billing_transactions',
                'ordering': ['-created_at'],
            },
        ),
        migrations.CreateModel(
            name='Brand',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order', models.IntegerField(blank=True, null=True)),
                ('name', models.CharField(max_length=100)),
                ('image', models.ImageField(blank=True, upload_to=crm.models.category.get_file_path)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'ordering': ['order'],
            },
        ),
        migrations.CreateModel(
            name='Checklist',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('is_active', models.BooleanField(default=False)),
                ('published', models.BooleanField(default=False, validators=[crm.models.checklist.validate_before_publish])),
                ('sort_order', models.IntegerField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ChecklistQuestionProduct',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product_id', models.IntegerField(blank=True, null=True)),
                ('question_type', models.CharField(max_length=100)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ChecklistQuestions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.CharField(max_length=255)),
                ('checklist_id', models.IntegerField(blank=True, null=True)),
                ('related_product_id', models.IntegerField(blank=True, null=True)),
                ('worn_related_product_id', models.IntegerField(blank=True, null=True)),
                ('is_section', models.BooleanField(default=False)),
                ('show_na_button', models.BooleanField(default=False)),
                ('position', models.IntegerField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, null=True)),
                ('address', models.CharField(max_length=100, null=True)),
                ('address_city', models.CharField(max_length=100, null=True)),
                ('address_state', models.CharField(max_length=100, null=True)),
                ('address_zip', models.CharField(max_length=5, null=True)),
                ('phone', models.CharField(blank=True, max_length=255, null=True)),
                ('website', models.CharField(blank=True, max_length=255, null=True, validators=[django.core.validators.RegexValidator(message='URL format: www.example.com', regex='^(www).[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$')])),
                ('email_id', models.EmailField(blank=True, max_length=255, null=True, validators=[django.core.validators.RegexValidator(message='Email format: name@email.com', regex='(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$)')])),
                ('terms', models.TextField(blank=True, null=True)),
                ('agreements', models.TextField(blank=True, null=True)),
                ('email_payment', models.EmailField(blank=True, max_length=254, null=True)),
                ('_logo', models.FileField(blank=True, db_column='logo', null=True, serialize=False, upload_to=crm.models.company.get_file_path, validators=[crm.validators.mime_type_validator.MimeTypeValidator(['image/jpeg', 'image/gif', 'image/png'])])),
                ('is_active', models.BooleanField(default=False)),
                ('display_signature_boxes', models.BooleanField(default=True)),
                ('has_open_discount', models.BooleanField(default=False)),
                ('monthly_payment_calc', models.BooleanField(default=False)),
                ('months', models.IntegerField(blank=True, null=True)),
                ('interest', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('enable_invoice_numbering', models.BooleanField(default=False)),
                ('last_invoice_number', models.IntegerField(blank=True, null=True)),
                ('enable_tax', models.BooleanField(default=False)),
                ('tax_rate', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('owner', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='owner_company', to=settings.AUTH_USER_MODEL)),
                ('users', models.ManyToManyField(related_name='related_company', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('contact_first_name', models.CharField(max_length=100)),
                ('contact_last_name', models.CharField(max_length=100)),
                ('spouse_first_name', models.CharField(max_length=100)),
                ('spouse_last_name', models.CharField(max_length=100)),
                ('address', models.CharField(max_length=100)),
                ('city', models.CharField(max_length=100)),
                ('state', models.CharField(max_length=100)),
                ('zip', models.CharField(max_length=100)),
                ('home_phone', models.CharField(max_length=100)),
                ('cell_phone_1', models.CharField(max_length=100)),
                ('cell_phone_2', models.CharField(max_length=100)),
                ('email', models.CharField(max_length=100)),
                ('status', models.CharField(choices=[('SUSPECT', 'Suspect'), ('PROSPECT', 'Prospect'), ('CUSTOMER', 'Customer')], max_length=8)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('first_name', models.CharField(default='', max_length=100, null=True)),
                ('last_name', models.CharField(default='', max_length=100, null=True)),
                ('spouse_name', models.CharField(blank=True, max_length=100, null=True)),
                ('spouse_phone', models.CharField(blank=True, max_length=20, null=True)),
                ('email', models.EmailField(blank=True, max_length=254, null=True)),
                ('phone', models.CharField(blank=True, max_length=20, null=True)),
                ('address', models.CharField(blank=True, max_length=100, null=True)),
                ('city', models.CharField(blank=True, max_length=100, null=True)),
                ('state', models.CharField(blank=True, max_length=100, null=True)),
                ('zip', models.CharField(blank=True, max_length=5, null=True)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_customers', to='crm.Company')),
                ('lead_owner', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'crm_customers',
            },
        ),
        migrations.CreateModel(
            name='CustomerAction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('date', models.DateTimeField()),
                ('description', models.TextField(null=True)),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_actions', to='crm.Customer')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_customers_actions', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'crm_customers_actions',
                'ordering': ['-date'],
            },
        ),
        migrations.CreateModel(
            name='CustomerLeadSource',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(max_length=100)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_customer_lead_sources', to='crm.Company')),
            ],
        ),
        migrations.CreateModel(
            name='CustomerNote',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('text', models.TextField(null=True)),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_customers_notes', to=settings.AUTH_USER_MODEL)),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_notes', to='crm.Customer')),
            ],
            options={
                'db_table': 'crm_customers_notes',
                'ordering': ['-created_at'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CustomForm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('job_id', models.CharField(default='', max_length=100, null=True)),
                ('form_data', models.CharField(default='', max_length=4000, null=True)),
            ],
            options={
                'db_table': 'crm_custom_forms',
            },
        ),
        migrations.CreateModel(
            name='Discount',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('name', models.CharField(max_length=100)),
                ('type', models.CharField(choices=[('PERCENTAGE', 'Percentage'), ('FIXED', 'Fixed')], max_length=10)),
                ('value', models.DecimalField(decimal_places=2, max_digits=10)),
                ('is_active', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('company', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='related_discounts', to='crm.Company')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='EstimateForm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('form_data', models.CharField(default='', max_length=4000, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'crm_estimate_forms',
            },
        ),
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uid', models.CharField(blank=True, max_length=30, null=True, unique=True)),
                ('hash', models.CharField(blank=True, max_length=32, null=True, unique=True)),
                ('data', models.TextField(blank=True, null=True)),
                ('is_approved', models.BooleanField(default=False)),
                ('type', models.CharField(choices=[('INVOICE', 'Invoice'), ('ESTIMATE', 'Estimate')], max_length=8, null=True)),
                ('price_itemize', models.BooleanField(default=True)),
                ('is_agreement', models.BooleanField(default=True)),
                ('signature_customer', models.TextField(blank=True, null=True)),
                ('signature_user', models.TextField(blank=True, null=True)),
                ('invoice_notes', models.TextField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('tax', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.Company')),
                ('discounts', models.ManyToManyField(related_name='related_invoices', to='crm.Discount')),
            ],
            options={
                'ordering': ['-created_at'],
            },
            bases=(models.Model, crm.models.invoice.InvoiceDiscountMixin),
        ),
        migrations.CreateModel(
            name='MeasurementType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default=None, max_length=100)),
                ('title', models.CharField(max_length=100)),
                ('class_name', models.CharField(default=None, max_length=30)),
                ('ng_order_component', models.CharField(default=None, max_length=200)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
            ],
            options={
                'db_table': 'crm_system_measurements',
            },
        ),
        migrations.CreateModel(
            name='Option',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('value', models.TextField(blank=True, default=None, null=True)),
            ],
            options={
                'db_table': 'crm_system_options',
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('status', models.CharField(choices=[('ACTIVE', 'Active'), ('DRAFT', 'Draft'), ('NEW', 'New'), ('CLOSED', 'Closed'), ('CREATED', 'Created')], max_length=7)),
                ('checklist_id', models.IntegerField(blank=True, null=True)),
                ('checklist_answers', models.TextField(blank=True, null=True)),
                #('show_review_link_on_pdf', models.BooleanField(default=False)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_orders', to='crm.Company')),
                ('customer', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='crm.Customer')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_orders', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-created_at'],
            },
        ),
        migrations.CreateModel(
            name='OrderAction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('date', models.DateTimeField()),
                ('description', models.TextField()),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_actions', to='crm.Order')),
            ],
            options={
                'db_table': 'crm_order_action',
                'ordering': ['-date'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='OrderItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('price', models.DecimalField(blank=True, decimal_places=2, default=False, max_digits=10)),
                ('data', models.TextField(blank=True, null=True)),
                ('index', models.IntegerField(blank=True, null=True)),
                ('note', models.CharField(blank=True, max_length=255, null=True)),
                ('dimension_notes', models.CharField(blank=True, max_length=255, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_items', to='crm.Order')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('name', models.CharField(max_length=100)),
                ('order', models.IntegerField(blank=True, null=True)),
                ('details', models.TextField(blank=True, null=True)),
                ('warranty', models.TextField(blank=True, null=True)),
                ('brochure', models.FileField(blank=True, null=True, upload_to=crm.models.product.get_brochure_file_path, validators=[crm.validators.mime_type_validator.MimeTypeValidator(['application/pdf'])])),
                ('data', models.TextField(blank=True, null=True)),
                ('is_publish', models.BooleanField(default=False, validators=[crm.models.product.validate_before_publish])),
                ('taxable', models.BooleanField(default=True)),
                ('enable_discount', models.BooleanField(default=True)),
                ('show_dimension_note', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('brand', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='crm.Brand')),
                ('category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='related_categories_products', to='crm.Category')),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_products', to='crm.Company')),
                ('measurement', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='crm.MeasurementType')),
                ('related_products', models.ManyToManyField(related_name='_product_related_products_+', to='crm.Product')),
                ('subcategory', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='subcategory', to='crm.Category')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ProductCategories',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.Category')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_categories', to='crm.Product')),
                ('subcategory', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.Category')),
            ],
            options={
                'db_table': 'crm_product_categories',
            },
        ),
        migrations.CreateModel(
            name='ProductImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to=crm.models.product_images.get_file_path)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_images', to='crm.Product')),
            ],
        ),
        migrations.CreateModel(
            name='ProductTag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('tag', models.CharField(max_length=100)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.Company')),
                ('product', models.ManyToManyField(related_name='related_tags', to='crm.Product')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ProspectStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_prospect_statuses', to='crm.Company')),
            ],
            options={
                'db_table': 'crm_company_prospect_status',
            },
        ),
        migrations.CreateModel(
            name='RegistrationFormStorage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('email', models.EmailField(max_length=50)),
                ('phone', models.CharField(max_length=20)),
                ('details', models.TextField(blank=True, null=True)),
            ],
            options={
                'db_table': 'crm_registration_form_storage',
            },
        ),
        migrations.CreateModel(
            name='UserActivation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('code', models.CharField(max_length=64)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='orderitem',
            name='product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crm.Product'),
        ),
        migrations.AddField(
            model_name='invoice',
            name='order',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='related_invoice', to='crm.Order'),
        ),
        migrations.AddField(
            model_name='customer',
            name='lead_source',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='crm.CustomerLeadSource'),
        ),
        migrations.AddField(
            model_name='customer',
            name='prospect',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='crm.ProspectStatus'),
        ),
        migrations.AddField(
            model_name='customer',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='related_users_customers', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='checklistquestionproduct',
            name='question',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='fail_products', to='crm.ChecklistQuestions'),
        ),
        migrations.AddField(
            model_name='checklist',
            name='company',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='related_checklists', to='crm.Company'),
        ),
        migrations.AddField(
            model_name='category',
            name='company',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='related_categories', to='crm.Company'),
        ),
        migrations.AddField(
            model_name='category',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='children', to='crm.Category'),
        ),
        migrations.AddField(
            model_name='brand',
            name='company',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_brands', to='crm.Company'),
        ),
        migrations.AddField(
            model_name='billingtransaction',
            name='company',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='related_billing_transactions', to='crm.Company'),
        ),
        migrations.AddField(
            model_name='billingtransaction',
            name='ipn',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='standard.PayPalIPN'),
        ),
        migrations.AddField(
            model_name='billingcompany',
            name='company',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='data_billing', to='crm.Company'),
        ),
        migrations.AddField(
            model_name='billingcompany',
            name='used_plan',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='related_company', to='crm.BillingPlan'),
        ),
        migrations.AddField(
            model_name='user',
            name='active_order',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='related_active_order', to='crm.Order'),
        ),
        migrations.AddField(
            model_name='user',
            name='groups',
            field=models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups'),
        ),
        migrations.AddField(
            model_name='user',
            name='user_permissions',
            field=models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions'),
        ),
        migrations.AlterUniqueTogether(
            name='brand',
            unique_together=set([('name', 'company')]),
        ),
    ]
