from django.test import TestCase

from crm.models import User


class CreateSuperUser(TestCase):
    def test_create_super_user(self):
        superuser = User.objects.create(
            username='superadmin',
            first_name='Super',
            last_name='User',
            email='superuser@superuser.loc',
            is_superuser=1,
            role='SUPERADMIN'
        )
        superuser.set_password('superadmin')
        superuser.save()
