from django.core.exceptions import ValidationError
import magic
from django.utils.deconstruct import deconstructible

MIME_TYPE_IMAGES = ['image/jpeg', 'image/gif', 'image/png']
MIME_TYPE_PDF = ['application/pdf']


@deconstructible
class MimeTypeValidator(object):
    def __init__(self, mimetypes):
        self.mimetypes = mimetypes

    def __call__(self, value):
        try:
            mime = magic.from_buffer(value.read(1024), mime=True)

            if not mime in self.mimetypes:
                raise ValidationError('Is not an acceptable file type. Required: ' + ', '.join(self.mimetypes))
        except AttributeError as e:
            raise ValidationError('This value could not be validated for file type' % value)
