import datetime

import logging


def LogRequestMiddleware(get_response):
    def middleware(request):
        t_start = datetime.datetime.now()

        response = get_response(request)

        t_end = datetime.datetime.now()

        request_path = request.META.get('PATH_INFO')
        if request_path.startswith('/api'):
            seq = (str(request.user), request_path, str(t_start), str(t_end), str(t_end - t_start))
            r_string = ', '.join(seq)
            logger = logging.getLogger('api_logger')
            logger.info(r_string)

        return response

    return middleware
