from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from biddingapp import settings


@csrf_exempt
def invoice_success(request):
    context = {
        'bidding_url': settings.CLIENT_URL
    }
    return render(request, "payment/payment-success.html", context)


def payment_cancel(request):
    context = {
        'bidding_url': settings.CLIENT_URL
    }
    return render(request, "payment/payment-cancel.html", context)
