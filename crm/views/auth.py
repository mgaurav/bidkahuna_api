from django.contrib.auth.forms import SetPasswordForm
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from paypal.standard.forms import PayPalPaymentsForm

from apiapp.serializers.billing import BillingPlanSerializer
from apiapp.views.billing_views import TransactionSerializer
from biddingapp import settings
from crm.models import User, json, BillingTransaction
from crm.models.user_activation import UserActivation


@csrf_exempt
def reminder_password(request):
    return HttpResponse("Hello, " + str(request.user))


def activation_user(request, token=None):
    try:
        activations = UserActivation.objects.all().get(code=token)
        if request.method == 'GET':
            form = SetPasswordForm(activations.user)
            return render(request, "auth/ajax_activation_layout.html", context={'form': form})

        if request.method == 'POST':
            form = SetPasswordForm(activations.user, request.POST)
            if request.is_ajax():
                return render(request, "auth/ajax_activation_layout.html", context={'form': form})
            if form.is_valid():
                form.save()
                activations.user.is_active = True
                activations.user.save()
                activations.delete()
                company = activations.user.company
                billing = company.billing
                out_form = ''
                if not billing.is_subscribe:
                    paypal_dict = {
                        "cmd": "_xclick-subscriptions",
                        "business": settings.PAYPAL_EMAIL,
                        "a3": billing.used_plan.price,
                        "p3": 1,
                        "t3": 'M',
                        "src": "1",
                        "sra": "1",
                        "no_note": "1",
                        "item_name": "BID KAHUNA Subscription",
                        "notify_url": settings.HOST_URL + reverse('paypal-ipn'),
                        "return_url": settings.HOST_URL + '/payment/success/',
                        "cancel_return": settings.HOST_URL + '/payment/cancel/',
                        "custom": json.dumps({"company_id": company.id}),
                    }
                    form = PayPalPaymentsForm(initial=paypal_dict, button_type="subscribe")
                    out_form = form.render()

                transactions = BillingTransaction.objects.all()
                response_data={
                    'active': company.is_active,
                    'last_transaction': billing.last_transaction,
                    'next_transaction': billing.next_check,
                    'plan': BillingPlanSerializer(billing.used_plan).data,
                    'subscribe_form': out_form,
                    'transactions': TransactionSerializer(transactions, many=True).data
                }
                return render(request, "auth/activation_billing.html", context={'billing': response_data})
            else:
                return render(request, "auth/activation_set_password.html", context={'form': form})
    except UserActivation.DoesNotExist:
        return render(request, "auth/activation_error.html")
