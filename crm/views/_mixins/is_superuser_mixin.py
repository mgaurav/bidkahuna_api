from django.contrib.auth.decorators import user_passes_test
from django.utils.decorators import method_decorator


class IsSuperUserMixin():
    """
    Checking if user is superuser
    """

    @method_decorator(user_passes_test(lambda u: u.is_superuser(), login_url='/'))
    def dispatch(self, *args, **kwargs):
        return super(IsSuperUserMixin, self).dispatch(*args, **kwargs)
