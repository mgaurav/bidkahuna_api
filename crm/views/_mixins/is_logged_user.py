from django.contrib.auth.decorators import user_passes_test
from django.utils.decorators import method_decorator


class IsLoggedUserMixin():
    """
    Check if user is logged
    """

    @method_decorator(user_passes_test(lambda u: u.is_logged(), login_url='/'))
    def dispatch(self, *args, **kwargs):
        return super(IsLoggedUserMixin, self).dispatch(*args, **kwargs)
