from django.contrib.auth.decorators import user_passes_test
from django.utils.decorators import method_decorator


class IsAnonymousUser():
    """
    Checking if user is anonymous
    """

    @method_decorator(user_passes_test(lambda u: u.is_anonymous(), login_url='/'))
    def dispatch(self, *args, **kwargs):
        return super(IsAnonymousUser, self).dispatch(*args, **kwargs)
