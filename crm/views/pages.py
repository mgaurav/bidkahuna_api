from django.shortcuts import render

from crm.models.option import Option


def show_terms(request):
    title = 'Terms and Conditions'
    content = Option.objects.all().get(name='terms').value
    return render(request, "content.html", context={'title': title, 'content': content})


def show_privacy(request):
    title = 'Privacy Policy'
    content = Option.objects.all().get(name='privacy').value
    return render(request, "content.html", context={'title': title, 'content': content})
