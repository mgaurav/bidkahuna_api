from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.views.generic import ListView

from crm.models import Product, Company


class ProductList(ListView):
    template_name = 'contractor/product/list.html'
    model = Product

    def get_queryset(self):
        company = Company.objects.get(owner=self.request.user)
        return Product.objects.filter(company=company)


class ProductCreate(CreateView):
    template_name = 'contractor/product/form.html'
    model = Product
    fields = ['name', 'brand']
    success_url = reverse_lazy('contractor:product.list')

    def form_valid(self, form):
        product = form.save(commit=False)
        company = Company.objects.get(owner=self.request.user)
        product.company = company
        # product.save()
        return super(ProductCreate, self).form_valid(form)
