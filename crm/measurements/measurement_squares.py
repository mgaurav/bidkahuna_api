from crm.measurements.measurement_mixin import MeasurementMixin


class MeasurementSquares(MeasurementMixin):
    def __init__(self, product, order):
        self.product = product
        self.order = order
        self.order_data = self.order.get_data()

    def price_base_method(self, price_value, value):
        return price_value * value

    def process(self):
        self.order.price = self.get_price()

    def get_price(self):
        value = next(row for row
                     in self.order_data
                     if row.get('measurement_key') == 'squares').get('value')
        return self.get_order_price(value)
