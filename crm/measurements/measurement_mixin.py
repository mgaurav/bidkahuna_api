import re


class MeasurementMixin:
    def get_order_price(self, value):
        value = int(value)
        index = self.find_base_index_by_value(value)
        if index != -1:
            return self.parse_attributes(index, value)
        else:
            return 0

    def get_order_grid_price(self, width, height):
        entered_width = float(width)
        entered_height = float(height)
        product_attributes = [row for row
                              in self.order_data
                              if row.get('group_id') and row.get('attr_id')]

        price_modification_groups = self.product.get_data().get('priceModificationGroups')
        price = 0
        for price_modification in price_modification_groups:
            if price_modification.get('isBase'):
                base_grid_obj = price_modification.get('attributes')[0]
                gridObj = base_grid_obj.get('grid')
                if gridObj :
                    price += self.get_grid_price(gridObj, entered_width, entered_height)
            else:
                for attr in product_attributes:
                    if price_modification.get('id') == attr.get('group_id'):
                        for attrbute in price_modification.get('attributes'):
                            if attrbute.get('id') == attr.get('attr_id'):
                                gridObj = attrbute.get('grid')
                                price += self.get_grid_price(gridObj, entered_width, entered_height)
        return price
    def get_grid_price(self,gridObj,entered_width,entered_height):
        ordHeightKeys=[]
        price = 0
        for heightKey in gridObj.keys():
            if heightKey != 'undefined':
                ordHeightKeys.append(int(heightKey))
        ordHeightKeys = sorted(ordHeightKeys)
        selectedGrid={};
        for ordHeightKey in ordHeightKeys:
            if entered_height <= float(ordHeightKey):
                selectedGrid = gridObj[str(ordHeightKey)]
                break
        if selectedGrid :
            ordWidthKeys = []
            for widthKey in selectedGrid.keys() :
                if widthKey != 'Height':
                    ordWidthKeys.append(int(widthKey))
            ordWidthKeys = sorted(ordWidthKeys)
            for ordWidthKey in ordWidthKeys:
                if entered_width <= float(ordWidthKey):
                    price = float(selectedGrid[str(ordWidthKey)])
                    break
            return price
        else:
            return price
    def find_base_index_by_value(self, value):
        """
        Get index for find by index in priceModificationGroups
        :param value:
        :param size:
        :return:
        """
        product_data = self.product.get_data()
        base_measurement_values = product_data.get('baseMeasurementValues')
        index = 0
        for base_measurement_value in base_measurement_values:
            base_value = str(base_measurement_value.get('value')).strip()

            out_match = re.search(r"(?P<from>\d*)[ -]*(?P<to>\d*)", base_value, re.IGNORECASE | re.MULTILINE)

            try:
                size_from = int(out_match.group('from'))
            except ValueError:
                size_from = None

            try:
                size_to = int(out_match.group('to'))
            except ValueError:
                size_to = None

            if not size_from is None and not size_to is None:
                if size_from <= value <= size_to:
                    return index
            else:
                if size_from == value:
                    return index

            index += 1
        return -1

    def parse_attributes(self, index, value):
        """
        Parse attribute and make the price
        :param index:
        :param value:
        :return:
        """
        product_attributes = [row for row
                              in self.order_data
                              if row.get('group_id') and row.get('attr_id')]

        price_modification_groups = self.product.get_data().get('priceModificationGroups')
        price_parts = []

        for attribute in product_attributes:
            group_id = attribute["group_id"]
            value_id = attribute["attr_id"]

            match_group = next(
                (group for group in price_modification_groups if int(group["id"]) == int(group_id)),
                None
            )

            if match_group:
                match_attribute = next((
                    attribute for attribute in match_group['attributes'] if int(value_id) == int(attribute["id"])
                ), None)

                if match_attribute:
                    if not match_attribute["values"][index]:
                        match_attribute["values"][index] = 0
                    price_part = {
                        "price": match_attribute["values"][index],
                        "isBase": match_group["isBase"],
                        "type": match_group["type"]
                    }
                    if match_group["isBase"]:
                        price_parts.insert(0, price_part)
                    else:
                        price_parts.append(price_part)
            else:
                group_title = attribute['title']
                group_value = attribute['value']

                match_group = next(
                    (group for group in price_modification_groups if group["title"] == group_title),
                    None
                )

                if match_group:
                    match_attribute = next((
                        attribute for attribute in match_group['attributes'] if group_value == attribute["label"]
                    ), None)

                    if match_attribute:
                        price_part = {
                            "price": match_attribute["values"][index],
                            "isBase": match_group["isBase"],
                            "type": match_group["type"]
                        }
                        if match_group["isBase"]:
                            price_parts.insert(0, price_part)
                        else:
                            price_parts.append(price_part)

        price = 0
        if len(price_parts) > 0:
            for price_part in price_parts:
                if price_part['isBase']:
                    price = self.price_base_method(price_part['price'], value)
                else:
                    price = price + price_part['price']

        return price
