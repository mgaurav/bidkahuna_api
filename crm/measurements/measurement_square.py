from crm.measurements.measurement_mixin import MeasurementMixin


class MeasurementSquare(MeasurementMixin):
    def __init__(self, product, order):
        self.product = product
        self.order = order
        self.order_data = self.order.get_data()

    def price_base_method(self, price_value, value):
        return price_value

    def process(self):
        self.order.price = self.get_price()

    def get_price(self):
        width = next(row for row
                     in self.order_data
                     if row.get('measurement_key') == 'width').get('value')
        height = next(row for row
                      in self.order_data
                      if row.get('measurement_key') == 'height').get('value')
        value = width * height
        return self.get_order_price(value)
