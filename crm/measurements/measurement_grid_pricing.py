from crm.measurements.measurement_mixin import MeasurementMixin


class MeasurementGridPricing(MeasurementMixin):
    def __init__(self, product, order):
        self.product = product
        self.order = order
        self.order_data = self.order.get_data()

    def process(self):
        self.order.price = self.get_price()

    def get_price(self):
        width = next(row for row
                     in self.order_data
                     if row.get('measurement_key') == 'width').get('value')
        height = next(row for row
                      in self.order_data
                      if row.get('measurement_key') == 'height').get('value')

        return self.get_order_grid_price(width, height)
