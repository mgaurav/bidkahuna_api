from django import template

register = template.Library()


@register.filter(name='hide_order_default_values')
def hide_order_default_values(list_values):
    result_list_values = []
    for index, value in enumerate(list_values):

        if 'group_id' in value:
            _value = value.get('value', '')
            if _value is not None and not _value.strip().lower() in ['no', 'default']:
                result_list_values.append(value)
        else:
            result_list_values.append(value)
    return result_list_values
