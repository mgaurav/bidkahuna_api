from rest_framework import status
from rest_framework.response import Response


def model_not_found(model=""):
    message = (model + ' not found.').capitalize()
    return Response({
        "result": "error",
        "message": message
    }, status=status.HTTP_200_OK)
