from rest_framework import status
from rest_framework.response import Response


def model_deleted(model=""):
    message = (model + ' deleted.').capitalize()
    return Response({
        "result": "deleted",
        "message": message
    }, status=status.HTTP_200_OK)
