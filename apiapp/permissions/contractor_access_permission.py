from rest_framework import permissions


class ContractorAccessPermission(permissions.BasePermission):
    message = 'You does not have permissions'

    def has_permission(self, request, view=None):
        user = request.user
        role = user.role
        return role == 'CONTRACTOR'
