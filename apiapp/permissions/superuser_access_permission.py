from rest_framework import permissions


class SuperuserAccessPermission(permissions.BasePermission):
    message = 'You has no permission.'

    def has_permission(self, request, view):
        user = request.user
        role = user.role
        return role == 'SUPERADMIN'
