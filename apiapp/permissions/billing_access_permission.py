from rest_framework import permissions


class BillingAccessPermission(permissions.BasePermission):
    message = 'You company is not active.'

    def has_permission(self, request, view):
        result = True
        user = request.user
        role = user.role
        if role == 'CONTRACTOR' or role == 'SALESPERSON':
            company = user.company
            result = company.is_active

        return result
