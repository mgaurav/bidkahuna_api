from django.conf.urls import url

from apiapp.views.actions_views import ActionViews
from apiapp.views.admin.admin_auth_view import AdminAuthView
from apiapp.views.admin.admin_users_views import AdminUsersViewSet
from apiapp.views.admin.billing_company_views import AdminBillingCompany
from apiapp.views.admin.billing_configurator_views import AdminBillingConfigurator
from apiapp.views.admin.billing_transaction_views import AdminBillingTransaction
from apiapp.views.admin.system_view import OptionsViewSet
from apiapp.views.billing_views import CompanyBillingViewSet
from apiapp.views.brand_views import BrandViewSet
from apiapp.views.category_views import CategoryViewSet
from apiapp.views.company_prospect_views import CompanyProspectView
from apiapp.views.customer_action_views import ActionViewSet
from apiapp.views.customer_lead_source_view import CustomerLeadSourceView
from apiapp.views.customer_note_views import NoteViewSet
from apiapp.views.customer_views import CustomerViewSet
from apiapp.views.custom_form_views import CustomFormViewSet
from apiapp.views.estimate_views import EstimateFormViewSet
from apiapp.views.dashboard_views import DashboardView
from apiapp.views.discount_open_views import DiscountOpenView
from apiapp.views.discount_views import DiscountViewSet
from apiapp.views.invoices_views import InvoiceViewSet
from apiapp.views.measurements_views import MeasurementViewSet
from apiapp.views.mobile.synchromize_actions import SynchronizeActions
from apiapp.views.mobile.synchromize_brands import SynchronizeBrands
from apiapp.views.mobile.synchronize_checklists import SynchronizeChecklists
from apiapp.views.mobile.synchronize_checklist_questions import SynchronizeChecklistQuestions
from apiapp.views.mobile.synchronize_checklist_question_products import SynchronizeChecklistQuestionProducts
from apiapp.views.mobile.synchromize_jobs import SynchronizeJobs
from apiapp.views.mobile.synchronize_company import SynchronizeCompany
from apiapp.views.mobile.synchronize_customers import SynchronizeCustomers
from apiapp.views.mobile.synchronize_discounts import SynchronizeDiscounts
from apiapp.views.mobile.synchronize_notes import SynchronizeCustomersNotes
from apiapp.views.mobile.synchronize_views import SynchronizeView
from apiapp.views.order_action_views import OrderActionViewSet
from apiapp.views.order_item_views import OrderItemViewSet
from apiapp.views.order_views import OrderViewSet, ShortOrderSerializer
from apiapp.views.pdf_views import PdfViewSet
from apiapp.views.permission_views import PermissionViewSet
from apiapp.views.products_views import ProductViewSet
from apiapp.views.registration_view import RegistrationView, get_terms, get_privacy_policy
from apiapp.views.tag_views import TagViewSet
from apiapp.views.uploader_views import UploaderViewSer
from apiapp.views.user_views import UserData, UserViewSet
from .views.company_views import CompanyViewSet
from .views.checklist_views import ChecklistViewSet
from .views.checklist_question_views import ChecklistQuestionViewSet

brands = BrandViewSet.as_view({
    'put': 'create',
    'get': 'list',
    'post': 'update',
    'delete': 'destroy'
})

categories = CategoryViewSet.as_view({
    'get': 'list',
    'post': 'update',
    'delete': 'destroy'
})

products = ProductViewSet.as_view({
    'get': 'list',
    'post': 'save',
})
urlpatterns = [
    ##############
    # OPEN API
    ##############
    url(r'^registration/form/$', RegistrationView.as_view()),
    url(r'^registration/terms/$', get_terms),
    url(r'^custom_form/(?P<job_id>[0-9]+)/$', CustomFormViewSet.as_view({'get': 'load', 'put': 'create'})),
    url(r'^estimate_form/$', EstimateFormViewSet.as_view({'get': 'load','post':'create'})),
    url(r'^estimate_form/(?P<pk>[0-9]+)/$', EstimateFormViewSet.as_view({'get': 'load', 'put': 'update','delete':'destroy'})),
    url(r'^registration/privacy_policy/$', get_privacy_policy),
    url(r'^pdf/(?P<invoice_hash>[\w]+)/$', PdfViewSet.as_view({'get': 'render_pdf'})),
    url(r'^html/(?P<invoice_hash>[\w]+)/$', PdfViewSet.as_view({'get': 'render_html'})),
    url(r'^html_debug/(?P<invoice_hash>[\w]+)/$', PdfViewSet.as_view({'get': 'render_html_debug'})),

    ##############
    # MOBILE API
    ##############
    url(r'^mobile/synchronize/company/$', SynchronizeCompany.as_view()),
    url(r'^mobile/synchronize/brands/$', SynchronizeBrands.as_view()),
    url(r'^mobile/synchronize/checklists/$', SynchronizeChecklists.as_view()),
    url(r'^mobile/synchronize/checklistQuestions/$', SynchronizeChecklistQuestions.as_view()),
    url(r'^mobile/synchronize/checklistQuestionProducts/$', SynchronizeChecklistQuestionProducts.as_view()),
    url(r'^mobile/synchronize/actions/$', SynchronizeActions.as_view()),
    url(r'^mobile/synchronize/discounts/$', SynchronizeDiscounts.as_view()),
    url(r'^mobile/synchronize/customers/$', SynchronizeCustomers.as_view()),
    url(r'^mobile/synchronize/customers/notes/$', SynchronizeCustomersNotes.as_view()),
    url(r'^mobile/synchronize/jobs/$', SynchronizeJobs.as_view()),
    url(r'^mobile/synchronize/$', SynchronizeView.as_view({'get': 'synchronize_data'})),
    url(r'^mobile/products/$', ProductViewSet.as_view({'get': 'synchronize_products'})),
    url(r'^mobile/tags/$', TagViewSet.as_view({'get': 'synchronize_tags'})),

    ##############
    # ADMIN API
    ##############

    # Contractors API v.2
    url(r'^admin/users/contractors/$', AdminUsersViewSet.as_view({'get': 'list'}), kwargs={'group': 'CONTRACTOR'}),
    url(r'^admin/users/contractor/create/$', AdminUsersViewSet.as_view({'put': 'create'})),
    url(r'^admin/users/contractor/(?P<pk>[0-9]+)/get/$', AdminUsersViewSet.as_view({'get': 'retrieve'})),
    url(r'^admin/users/contractor/(?P<pk>[0-9]+)/update/$', AdminUsersViewSet.as_view({'post': 'update'})),
    url(r'^admin/users/contractor/(?P<pk>[0-9]+)/destroy/$', AdminUsersViewSet.as_view({'delete': 'destroy'})),
    url(r'^admin/users/contractor/(?P<user_id>[0-9]+)/toggleTrial/$', AdminUsersViewSet.as_view({'get': 'toggle_trial'})),

    # Billing API
    url(r'^admin/billing/$', AdminBillingConfigurator.as_view({'get': 'list', 'put': 'create'})),
    url(r'^admin/billing/(?P<pk>[0-9]+)/update/$', AdminBillingConfigurator.as_view({'post': 'update'})),
    url(r'^admin/billing/(?P<pk>[0-9]+)/destroy/$', AdminBillingConfigurator.as_view({'delete': 'destroy'})),
    url(r'^admin/billing/(?P<pk>[0-9]+)/get/$', AdminBillingCompany.as_view({'get': 'retrieve'})),
    url(r'^admin/billing/(?P<company_id>[0-9]+)/$', AdminBillingTransaction.as_view({'put': 'change_balance'})),

    url(r'^admin/terms/$', OptionsViewSet.as_view(), kwargs={'option': 'terms'}),
    url(r'^admin/privacy/$', OptionsViewSet.as_view(), kwargs={'option': 'privacy'}),

    # Login As function
    url(r'^admin/user/login_as/', AdminAuthView.as_view()),

    ##############
    # BIDDING API
    ##############

    # Common method for all users. Get current state for a user.
    url(r'^user/$', UserData.as_view({'get': 'retrieve', 'post': 'update'})),
    url(r'^actions/$', ActionViews.as_view({'get': 'list_actions'})),
    url(r'^action/(?P<action_type>\w+)/(?P<action_id>[0-9]+)/$', ActionViews.as_view({'get': 'retrieve_action'})),

    # Companies API v.2
    url(r'^company/$', CompanyViewSet.as_view({'get': 'retrieve', 'post': 'update'})),
    url(r'^company/terms/$', CompanyViewSet.as_view({'get': 'get_terms', 'post': 'update_terms'})),
    url(r'^company/agreements/$', CompanyViewSet.as_view({'get': 'get_agreements', 'post': 'update_agreements'})),
    url(r'^company/billing/$', CompanyBillingViewSet.as_view({'get': 'billing_status'})),
    url(r'^company/billing/invoice/$', CompanyBillingViewSet.as_view({'post': 'make_invoice'})),
    url(r'^company/prospect/$', CompanyProspectView.as_view({'get': 'list', 'post': 'update'})),
    url(r'^company/settings/$', CompanyViewSet.as_view({'get': 'get_settings', 'post': 'update_settings'})),

    # Users API
    url(r'^users/list/$', UserViewSet.as_view({'get': 'list'})),
    url(r'^user/create/$', UserViewSet.as_view({'put': 'create'})),
    url(r'^user/(?P<pk>[0-9]+)/get/$', UserViewSet.as_view({'get': 'retrieve'})),
    url(r'^user/(?P<pk>[0-9]+)/update/$', UserViewSet.as_view({'post': 'update'})),
    url(r'^user/(?P<pk>[0-9]+)/remove/$', UserViewSet.as_view({'delete': 'destroy'})),

    # Discounts API
    url(r'^discounts/list/$', DiscountViewSet.as_view({'get': 'list'})),
    url(r'^discount/create/$', DiscountViewSet.as_view({'put': 'create'})),
    url(r'^discount/open_discount/$', DiscountOpenView.as_view()),
    url(r'^discount/(?P<pk>[0-9]+)/get/$', DiscountViewSet.as_view({'get': 'retrieve'})),
    url(r'^discount/(?P<pk>[0-9]+)/update/$', DiscountViewSet.as_view({'post': 'update'})),
    url(r'^discount/(?P<pk>[0-9]+)/remove/$', DiscountViewSet.as_view({'delete': 'destroy'})),

    url(r'^permissions/$', PermissionViewSet.as_view({'get': 'list'})),
    # url(r'^company/(?P<pk>[0-9]+)?$', company),
    url(r'^categories/(?P<pk>[0-9]+)?$', categories),
    url(r'^categories/sort/$', CategoryViewSet.as_view({'post': 'sort_category'})),

    url(r'^services/measurements$', MeasurementViewSet.as_view({'get': 'list'})),

    # Products v2
    url(r'^dashboard/$', DashboardView.as_view()),
    url(r'^products/$', ProductViewSet.as_view({'get': 'list_with_serializer'})),
    url(r'^products/(?P<pk>[0-9]+)/get$', ProductViewSet.as_view({'get': 'retrieve'})),
    url(r'^products/brands$', ProductViewSet.as_view({'get': 'list_related_brands'})),
    url(r'^products/sort/$', ProductViewSet.as_view({'post': 'sort_products'})),
    url(r'^products/alter/price/$', ProductViewSet.as_view({'post': 'change_price'})),
    url(r'^product/(?P<pk>[0-9]+)$', ProductViewSet.as_view({'get': 'list_with_serializer'})),
    url(r'^product/(?P<pk>[0-9]+)/clone/$', ProductViewSet.as_view({'put': 'clone'})),
    url(r'^product/(?P<pk>[0-9]+)/remove', ProductViewSet.as_view({'delete': 'delete'})),
    url(r'^product/$', ProductViewSet.as_view({'put': 'save', 'post': 'save'})),
    url(r'^tag/search/$', TagViewSet.as_view({'get': 'search'})),
    url(r'^tag/create/$', TagViewSet.as_view({'post': 'create_tag'})),
    url(r'^invoice/list/$', InvoiceViewSet.as_view({'get': 'list_invoices'})),
    url(r'^invoice/make/(?P<order_id>[0-9]+)/$', InvoiceViewSet.as_view({'get': 'make_invoice'})),
    url(r'^invoice/(?P<invoice_id>[0-9]+)/$', InvoiceViewSet.as_view({'get': 'get_invoice'})),
    url(r'^invoice/(?P<invoice_id>[0-9]+)/send/$', InvoiceViewSet.as_view({'post': 'send_invoice'})),
    url(r'^invoice/(?P<invoice_id>[0-9]+)/delete/$', InvoiceViewSet.as_view({'delete': 'delete_invoice'}),
        kwargs={'soft_delete': True}),
    url(r'^invoice/submit/$', InvoiceViewSet.as_view({'post': 'submit_invoice'})),
    url(r'^invoice/save/$', InvoiceViewSet.as_view({'post': 'save_invoice'})),

    # @Debug
    url(r'^invoice/load/$', InvoiceViewSet.as_view({'get': 'load_invoice'})),

    # Brands API
    url(r'^brands/(?P<pk>[0-9]+)?$', brands, kwargs={'filter_by_company': True}),

    # Customers API
    url(r'^customers/list/$', CustomerViewSet.as_view({'get': 'list'})),

    # Customer API
    url(r'^customer/create/$', CustomerViewSet.as_view({'put': 'create'})),
    url(r'^customer/form/options/$', CustomerViewSet.as_view({'get': 'form_options'})),
    url(r'^customer/(?P<pk>[0-9]+)/get/$', CustomerViewSet.as_view({'get': 'retrieve'})),
    url(r'^customer/(?P<pk>[0-9]+)/update/$', CustomerViewSet.as_view({'post': 'update'})),
    url(r'^customer/(?P<pk>[0-9]+)/destroy/$', CustomerViewSet.as_view({'delete': 'destroy'})),
    url(r'^customers/search/(?P<search_query>.*)/$', CustomerViewSet.as_view({'get': 'search_customer'})),
    url(r'^customer/lead-source/$', CustomerLeadSourceView.as_view()),
    # Customer Notes API
    url(r'^customer/notes/list/', NoteViewSet.as_view({'get': 'list'}), kwargs={'filter_by_company': True}),
    url(r'^customer/(?P<customer_id>[0-9]+)/notes/list/$', NoteViewSet.as_view({'get': 'list_notes'})),
    url(r'^customer/(?P<customer_id>[0-9]+)/note/$', NoteViewSet.as_view({'post': 'save_note'})),
    url(r'^customer/(?P<customer_id>[0-9]+)/note/(?P<note_id>[0-9]+)/remove/$',
        NoteViewSet.as_view({'delete': 'delete_note'})),
    # Customer Actions API
    url(r'^customer/(?P<customer_id>[0-9]+)/actions/list/$', ActionViewSet.as_view({'get': 'list'})),
    url(r'^customer/(?P<customer_id>[0-9]+)/action/(?P<action_id>[0-9]+)?/?$',
        ActionViewSet.as_view({'put': 'save', 'post': 'save', 'delete': 'delete_note'})),

    url(r'^products/(?P<pk>[0-9]+)?$', products),
    url(r'^uploader/$', UploaderViewSer.as_view()),

    url(r'^order/saveChecklistAnswer/$',OrderViewSet.as_view({'post': 'save_checklist_data'})),
    url(r'^order/(?P<order_id>[0-9]+)/get/$', OrderViewSet.as_view({'get': 'retrieve'})),
    url(r'^order/$', OrderViewSet.as_view({'post': 'save', 'get': 'retrieve'})),
    url(r'^order/list/$', OrderViewSet.as_view({'get': 'list'}), {'serializer_class': ShortOrderSerializer}),
    url(r'^order/current/$', OrderViewSet.as_view({'get': 'current_order'})),
    url(r'^order/create/$', OrderViewSet.as_view({'put': 'create_order'})),
    url(r'^order/exit/$', OrderViewSet.as_view({'get': 'exit_current_order'})),
    url(r'^order/(?P<order_id>[0-9]+)/remove/$', OrderViewSet.as_view({'delete': 'remove_order'})),
    url(r'^order/(?P<order_id>[0-9]+)/current/$', OrderViewSet.as_view({'put': 'set_current_order'})),
    url(r'^order/(?P<order_id>[0-9]+)/draft/$', OrderViewSet.as_view({'get': 'save_as_draft'})),
    url(r'^order/(?P<order_id>[0-9]+)/item/(?P<order_item_id>[0-9]+)/change/$',
        OrderItemViewSet.as_view({'post': 'change_order_item'})),
    url(r'^order/(?P<order_id>[0-9]+)/item-exchange/$',
        OrderItemViewSet.as_view({'post': 'brand_based_change_order_items'})),
    url(r'^order/item/$', OrderItemViewSet.as_view({'post': 'save_order_item'})),
    url(r'^order/item/(?P<order_item_id>[0-9]+)/remove/$', OrderItemViewSet.as_view({'delete': 'remove_order_item'})),
    # Orders Actions
    url(r'^order/(?P<order_id>[0-9]+)/action/(?P<action_id>[0-9]+)?/?$',
        OrderActionViewSet.as_view({'get': 'list', 'put': 'create', 'post': 'update', 'delete': 'destroy'})),
    url(r'^order/actions/$', OrderActionViewSet.as_view({'get': 'list_actions'})),
    url(r'^order/action/(?P<action_id>[0-9]+)/$', OrderActionViewSet.as_view({'get': 'full_data_action'})),

    url(r'^order/action/(?P<action_id>[0-9]+)/$', OrderActionViewSet.as_view({'get': 'full_data_action'})),

    #Checklists API
    url(r'^checklist/create/$',ChecklistViewSet.as_view({'put': 'create'})),
    url(r'^checklist/(?P<pk>[0-9]+)/update',ChecklistViewSet.as_view({'post': 'update'})),
    url(r'^checklist/(?P<checklist_id>[0-9]+)/remove/$', ChecklistViewSet.as_view({'delete': 'destroy'})),
    url(r'^checklist/list/$', ChecklistViewSet.as_view({'get': 'list'})),
    url(r'^checklist/(?P<pk>[0-9]+)/get/$', ChecklistViewSet.as_view({'get': 'retrieve'})),
    url(r'^checklist/sort/$', ChecklistViewSet.as_view({'post': 'sort_checklist'})),

    url(r'^checklist/question/create/$', ChecklistQuestionViewSet.as_view({'post': 'create'})),
    url(r'^checklist/question/(?P<pk>[0-9]+)/update', ChecklistQuestionViewSet.as_view({'post': 'update'})),
    url(r'^checklist/question/(?P<question_id>[0-9]+)/remove/$',ChecklistQuestionViewSet.as_view({'delete': 'destroy'})),
    url(r'^checklists/questions/$', ChecklistQuestionViewSet.as_view({'get': 'list'})),
    url(r'^checklist/question/(?P<pk>[0-9]+)/get/$', ChecklistQuestionViewSet.as_view({'get': 'retrieve'})),
    url(r'^questions/sort/$', ChecklistQuestionViewSet.as_view({'post': 'sort_questions'})),
]
