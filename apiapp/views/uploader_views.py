from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
import uuid

from apiapp.permissions.billing_access_permission import BillingAccessPermission
from apiapp.permissions.contractor_access_permission import ContractorAccessPermission
from biddingapp import settings


class UploaderViewSer(APIView):
    permission_classes = (permissions.IsAuthenticated, BillingAccessPermission, ContractorAccessPermission)

    def post(self, request):
        file_obj = request.FILES['file']
        file_name = uuid.uuid1().hex + '_' + file_obj.name
        file_path = 'media/tmp/' + file_name
        with open(file_path, 'wb+') as destination:
            for chunk in file_obj.chunks():
                destination.write(chunk)
        response = {
            "url": settings.HOST_URL + '/' + file_path,
            "link": file_name
        }
        return Response(response, status=200)
