from datetime import datetime

from rest_framework import permissions
from rest_framework import serializers
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response

from apiapp.permissions.billing_access_permission import BillingAccessPermission
from apiapp.permissions.contractor_access_permission import ContractorAccessPermission
from apiapp.serializers.requests.save_user_request import SaveUserRequest
from apiapp.serializers.user import UserDataSerializer
from apiapp.utils.time_stamp_fields import stamp_to_time
from crm.models import User
from crm.models.customer_action import CustomerAction
from crm.models.order_action import OrderAction


class PermissionDataSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        return instance.content_type.model + '.' + instance.codename


class UserSerializer(serializers.ModelSerializer):
    user_permissions = PermissionDataSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'group', 'first_name', 'last_name', 'show_checklist', 'show_custom_form', 'user_permissions','review_link')


class UserData(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    pagination_class = None

    def get_object(self):
        return self.request.user

    def get_serializer_class(self):
        if self.action == 'update':
            return SaveUserRequest
        if self.action == 'retrieve':
            return UserSerializer
        return self.serializer_class

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.id:
            serializer = self.get_serializer(instance)
            result = serializer.data
            company = request.user.company
            if company:
                date_start = request.GET.get('s')
                date_end = request.GET.get('e')
                if date_start and date_end:
                    date_start = stamp_to_time(date_start)
                    date_end = stamp_to_time(date_end)
                    actions_orders = OrderAction.active_objects.all() \
                        .filter(order__company=request.user.company,
                                order__deleted_at__isnull=True,
                                date__gte=date_start,
                                date__lte=date_end,
                                order__user=request.user)
                    actions_customers = CustomerAction.active_objects.all() \
                        .filter(customer__company=request.user.company,
                                customer__deleted_at__isnull=True,
                                date__gte=date_start,
                                date__lte=date_end,
                                user=request.user)
                    result["actions_count"] = actions_orders.count() + actions_customers.count()
                result["company_actived"] = company.is_active
            return Response(result)
        else:
            return Response({})

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.id:
            serializer = self.get_serializer(instance, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            password = self.request.data.get("password")
            if password:
                instance.set_password(password)
            instance.save()
            result = UserSerializer(instance).data
            result["company_actived"] = instance.company.is_active
            return Response(result)
        else:
            return Response({}, status=status.HTTP_400_BAD_REQUEST)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated, ContractorAccessPermission,
                          BillingAccessPermission, permissions.DjangoModelPermissions,)
    pagination_class = None

    def get_queryset(self):
        return self.queryset.filter(related_company=self.request.user.company)

    def get_serializer_class(self):
        if self.action == 'create':
            return SaveUserRequest
        if self.action == 'update':
            return SaveUserRequest
        if self.action == 'retrieve':
            return UserDataSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        super(UserViewSet, self).perform_create(serializer)
        serializer.instance.role = "SALESPERSON"
        new_password = serializer.initial_data.get('password')
        if new_password:
            serializer.instance.set_password(new_password)
        # Update company
        self.request.user.company.users.add(serializer.instance)
        # Update permissions
        user_permissions = serializer.initial_data.get('permissions')
        if user_permissions:
            serializer.instance.user_permissions.set(user_permissions)
        else:
            serializer.instance.user_permissions.set([])
        serializer.save()

    def perform_update(self, serializer):
        self.perform_create(serializer)

    def perform_destroy(self, instance):
        instance.delete_with_assign(assign_to=self.request.GET.get('assign_to'))
