import distutils
from distutils import util

from django.core.exceptions import ValidationError
from django.core.files import File
from django.db.models import Q
from rest_framework import permissions
from rest_framework import serializers
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response

from apiapp.paginations.page_number_pagination import PageNumberPagination
from apiapp.serializers.requests.save_product_request import SaveProductRequest
from apiapp.utils.time_stamp_fields import stamp_to_time
from apiapp.views import product_serializers
from apiapp.views.brand_views import BrandSerializer
from apiapp.views.category_views import CategorySerializer
from crm.models import Product, ProductImage, ProductTag, OrderItem, Order
from crm.models.product_categories import ProductCategories


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductImage
        fields = ['pk', 'url']


class ProductsSerializer(serializers.ModelSerializer):
    brand = BrandSerializer(read_only=True)
    category = CategorySerializer(read_only=True)
    subcategory = CategorySerializer(read_only=True)
    related_images = ImageSerializer(read_only=True, many=True)

    class Meta:
        model = Product
        fields = ['pk', 'name', 'brand', 'details', 'warranty',
                  'category', 'subcategory', 'get_data',
                  'brochure_url', 'related_images', 'is_publish', 'taxable', 'enable_discount', 'show_dimension_note']


class ProductViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated, permissions.DjangoModelPermissions,)
    serializer_class = ProductsSerializer
    queryset = Product.active_objects.active()
    pagination_class = PageNumberPagination

    def _get_products(self, request):
        """
        ?filter&category=1&category=2
        :param request:
        :return:
        """
        products = self.queryset.filter(company=self.request.user.company).order_by('order')

        filter_param = request.GET.getlist('filter')
        if filter_param:
            # Filter By Category
            filter_by_category = request.GET.getlist('category')
            if len(filter_by_category) > 0:
                if 'None' in filter_by_category:
                    products = products.filter(category__isnull=True)
                else:
                    products = products.filter(category__in=filter_by_category)
            # Filter By SubCategory
            filter_by_subcategory = request.GET.getlist('subcategory')
            if len(filter_by_subcategory) > 0:
                if 'None' in filter_by_subcategory:
                    products = products.filter(subcategory__isnull=True)
                else:
                    products = products.filter(subcategory__in=filter_by_subcategory)

        return products

    def change_price(self, request, *args, **kwargs):
        products = request.user.company.related_products.all().filter(pk__in=request.data.get('products'))
        for product in products:
            target_price = request.data.get('price')
            product.change_price(target_price.get('type'), target_price.get('value'))
            product.save()
        return Response({}, status=status.HTTP_200_OK)

    def parse_filter(self, request, products_query):
        # Category Filter
        filter_by_category = request.GET.get('category_id')
        if filter_by_category:
            products_extra_categories = ProductCategories.objects.all().filter(category_id=filter_by_category)
            extra_products = []
            for extra_product in products_extra_categories:
                extra_products.append(extra_product.product_id)
            products_query = products_query \
                .filter(Q(category_id=filter_by_category) | Q(pk__in=extra_products))
        # Subcategory Filter
        filter_by_subcategory = request.GET.get('subcategory_id')
        if filter_by_subcategory:
            products_extra_categories = ProductCategories.objects.all().filter(subcategory_id=filter_by_subcategory)
            extra_products = []
            for extra_product in products_extra_categories:
                extra_products.append(extra_product.product_id)
            products_query = products_query \
                .filter(Q(subcategory_id=filter_by_subcategory) | Q(pk__in=extra_products))
        # Published

        filter_by_published = distutils.util.strtobool(request.GET.get('is_published', 'false'))
        if filter_by_published:
            products_query = products_query.filter(is_publish=filter_by_published)

        # Action
        filter_by_action = request.GET.get('action')
        if filter_by_action == 'CHANGE_PRODUCT':
            order_item_id = request.GET.get('order_item_id')
            if order_item_id:
                order_item = OrderItem.objects.all().get(id=order_item_id)
                subcategory = order_item.product.subcategory
                products_query = products_query.filter(measurement=order_item.product.measurement)
                products_query = products_query.filter(Q(subcategory=subcategory))

        return products_query

    def synchronize_products(self, request, *args, **kwargs):
        """
        @Mobile API
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        products = Product.objects.all().filter(company=self.request.user.company)
        filter_stamp = request.GET.get('t')
        if filter_stamp:
            timestamp = stamp_to_time(filter_stamp)
            products = products.filter(Q(created_at__gt=timestamp) | Q(updated_at__gt=timestamp) | Q(deleted_at__gt=timestamp))

        page = self.paginate_queryset(products)
        if page is not None:
            serializer = product_serializers.ProductSynchroSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = product_serializers.ProductSynchroSerializer(students, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


        #     products = products.filter(
        #         Q(created_at__gt=timestamp) | Q(updated_at__gt=timestamp) | Q(deleted_at__gt=timestamp))
        # return Response(product_serializers.ProductSynchroSerializer(products, many=True).data,
        #                 status=status.HTTP_200_OK)


    def list_with_serializer(self, request, *args, **kwargs):
        products = self._get_products(request)
        products = self.parse_filter(request, products)

        serializer_name = self.request.GET.get("serializer", "Simple")
        serializer_class_name = "Product" + serializer_name.capitalize() + "Serializer"
        serializer_class = getattr(product_serializers, serializer_class_name)
        pk = kwargs.get('pk', None)
        many = True
        if pk:
            many = False
            products = products.get(pk=pk)
        else:
            pass

        return Response(serializer_class(products, many=many).data, status=status.HTTP_200_OK)

        # page = self.paginate_queryset(products)
        # if page is not None:
        #     serializer = serializer_class(page, many=True)
        #     return self.get_paginated_response(serializer.data)
        # serializer = serializer_class(students, many=True)
        # return Response(serializer.data, status=status.HTTP_200_OK)


    def list(self, request, *args, **kwargs):
        pk = kwargs.get('pk', request.data.get('pk', None))
        if pk is None:
            product = Product.active_objects.active(company=self.request.user.company)
            # Published
            filter_by_published = distutils.util.strtobool(request.GET.get('is_published', 'false'))
            if filter_by_published:
                product = product.filter(is_publish=filter_by_published)
            # Category Filter
            filter_by_category = request.GET.get('category_id')
            if filter_by_category:
                product = product.filter(category_id=filter_by_category)
            # Subcategory Filter
            filter_by_subcategory = request.GET.get('subcategory_id')
            if filter_by_subcategory:
                product = product.filter(subcategory_id=filter_by_subcategory)
            many = True
        else:
            product = Product.active_objects.active().get(pk=pk)
            many = False

        return Response(ProductsSerializer(product, many=many).data)

    def save(self, request, *args, **kwargs):
        pk = request.data.get('id')
        if pk:
            product = Product.objects.get(pk=pk, company=request.user.company)
            product_serializer = SaveProductRequest(product, data=request.data)
        else:
            product_serializer = SaveProductRequest(data=request.data)

        product_serializer.is_valid(raise_exception=True)

        try:
            product_serializer.save(company=request.user.company)
        except ValidationError as e:
            raise ValidationError(e)

        product_serializer.instance.taxable = request.data.get('taxable')
        product_serializer.instance.enable_discount = request.data.get('enable_discount')
        product_serializer.instance.show_dimension_note = request.data.get('show_dimension_note')
        # Related Categories
        related_categories = request.data.get('related_categories')
        if type(related_categories) is list:
            relations = []
            for obj in related_categories:
                relation = ProductCategories(product=product,
                                             category_id=obj.get('category_id'),
                                             subcategory_id=obj.get('subcategory_id')
                                             )
                relations.append(relation)
            product_serializer.instance.related_categories.all().delete()
            product_serializer.instance.related_categories.set(relations, bulk=False)

        # Tags
        related_tags = request.data.get('related_tags')
        if related_tags:
            tags = ProductTag.objects.all().filter(company=request.user.company, pk__in=related_tags)
            product_serializer.instance.related_tags.set(tags)

        # Images
        images = request.data.get('related_images')
        for _image in images:
            if _image.get('_mark_as') == 'new':
                image = ProductImage()
                image_link = _image.get('link')
                image.image = File(open('media/tmp/' + image_link, "rb"), name=image_link)
                image.product = product_serializer.instance
                image.save()
            if _image.get('_mark_as') == 'delete':
                image_pk = _image.get('id')
                if image_pk:
                    product_serializer.instance.related_images.get(pk=image_pk).delete()

        # Brochure
        brochure = request.data.get('brochure')
        if not brochure:
            product_serializer.instance.brochure = None
        elif not isinstance(brochure, str):
            link = brochure.get('link')
            product_serializer.instance.brochure = File(open('media/tmp/' + link, "rb"), name=link)

        # Related products
        related_products = request.data.get('related_products')
        if related_products:
            products = Product.objects.all()
            products = products.filter(company=request.user.company, pk__in=related_products)
            product_serializer.instance.related_products.set(products)

        # # Measurement
        # measurement_id = request.data.get('measurement_id')
        # if measurement_id:
        #     product_serializer.instance.measurement = MeasurementType.objects.get(pk=measurement_id)

        # Data
        data = {
            "baseMeasurementValues": request.data.get('baseMeasurementValues'),
            "priceModificationGroups": request.data.get('priceModificationGroups')
        }
        product_serializer.instance.set_data(data)

        product_serializer.instance.save()

        return Response({'product_id': product_serializer.instance.pk}, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        product = self._get_products(request).get(pk=pk)
        product.delete(soft_delete=True)
        return Response('', status=status.HTTP_200_OK)

    # @todo Users and permission
    def clone(self, request, *args, **kwargs):
        pk = kwargs.get('pk')

        product = self._get_products(request).get(pk=pk)
        images = product.related_images.all()
        tags = product.related_tags.all()

        product.pk = None
        product.name += " [ cloned ]"
        # Check the length for a cloned product
        new_name_len = len(product.name)
        if new_name_len.__gt__(100):
            product.name = product.name[new_name_len - 100:]
        product.is_publish = False
        product.save()

        # Clone Images
        for image in images:
            image.pk = None
            image.product = product
            image.save()

        # Clone Tags
        for tag in tags:
            product.related_tags.set(tags)

        return Response('', status=status.HTTP_200_OK)

    def sort_products(self, request, *args, **kwargs):
        products = self.queryset.filter(company=request.user.company)
        for product in products:
            for row in request.data:
                if row.get('id') == product.id:

                    product.order = row.get('order')
                    product.save()

        return Response('')
    def list_related_brands(self, request, *args, **kwargs):
        order_items = []
        current_order = request.user.active_order
        current_product_brands =[]
        if current_order:
            order = Order.objects.get(pk=current_order.id)
            order_items = order.related_items.filter(order=order)
            for order_item in order_items:
                product = Product.objects.get(pk=order_item.product_id)
                if(product.brand):
                 current_product_brands.append(product.brand)

        current_product_brands=list(set(current_product_brands))
        related_brands = []
        for order_item in order_items:
            current_product = Product.objects.get(pk=order_item.product_id)
            products = Product.objects.all().filter(company=request.user.company,
                                                    subcategory=current_product.subcategory, is_publish=True)
            for product in products:
                if(product.brand):
                    related_brands.append(product.brand)
            related_brands = list(set(related_brands))
        if (len(current_product_brands) == 1):
            related_brands.remove(current_product_brands[0])

        return Response(product_serializers.BrandSimpleSerializer(related_brands, many=True).data, status=status.HTTP_200_OK)

    def retrieve(self, request, *args, **kwargs):
        instance_object = self.get_object()
        serializer_object = self.get_serializer(instance_object)
        return Response(serializer_object.data,status=status.HTTP_200_OK)
