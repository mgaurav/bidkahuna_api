from rest_framework import viewsets, status, permissions
from rest_framework.response import Response
from apiapp.serializers.checklist_question import ChecklistQuestionSerialize
from apiapp.serializers.requests.save_checklist_question_request import SaveChecklistQuestionRequest
from crm.models import ChecklistQuestions, ValidationError, ObjectDoesNotExist

class ChecklistQuestionViewSet(viewsets.ModelViewSet):
    queryset = ChecklistQuestions.objects.all()
    serializer_class = ChecklistQuestionSerialize
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = None

    def get_queryset(self):
        queryset = self.queryset
        checklist_id = self.request.GET.get('id')
        if checklist_id:
            queryset =  self.queryset.filter(checklist_id=checklist_id, deleted_at__isnull=True).order_by('position')
        return queryset

    def get_serializer_class(self):
        if self.action == 'create':
            return SaveChecklistQuestionRequest
        if self.action == 'update':
            return SaveChecklistQuestionRequest
        return self.serializer_class

    def perform_create(self, serializer):
        try:
            serializer.save()
        except ValidationError as e:
            raise ValidationError(e)

    def perform_update(self, serializer):
        try:
            serializer.save()
        except ValidationError as e:
            raise ValidationError(e)

    def retrieve(self, request, *args, **kwargs):
        instance_object = self.get_object()
        serializer_object = self.get_serializer(instance_object)
        result = {
            'data': serializer_object.data
        }
        return Response(result)

    def destroy(self, request, *args, **kwargs):
        """
        Remove Worksheet by Id
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        question_id = kwargs.get('question_id')
        try:
            question_queryset = self.get_queryset()
            question = question_queryset.get(pk=question_id)
            question.delete()
            return Response({}, status=status.HTTP_202_ACCEPTED)
        except ObjectDoesNotExist:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

    def sort_questions(self, request, *args, **kwargs):

        questions = self.queryset.filter(checklist_id=request.data.get('checklist_id'))
        for question in questions:
            for row in request.data.get('order_data'):
                if row.get('id') == question.id:
                    question.position = row.get('order')
                    question.save()

        return Response('')
