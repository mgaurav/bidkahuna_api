from rest_framework import permissions
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response

from apiapp.permissions.billing_access_permission import BillingAccessPermission
from apiapp.serializers.customer_note import NoteSerializer
from apiapp.utils.time_stamp_fields import stamp_to_time
from crm.models import CustomerNote


class NoteViewSet(viewsets.ModelViewSet):
    queryset = CustomerNote.active_objects.active()
    permission_classes = (permissions.IsAuthenticated, BillingAccessPermission, permissions.DjangoModelPermissions,)
    serializer_class = NoteSerializer
    pagination_class = None

    def get_queryset(self):
        queryset = self.queryset
        if self.kwargs.get('filter_by_company'):
            queryset = queryset.filter(customer__company=self.request.user.company)
        timestamp = self.request.GET.get('t')
        if timestamp:
            timestamp = stamp_to_time(timestamp)
            queryset = queryset.get_update_summary(timestamp=timestamp)
        return queryset

    def list_notes(self, request, *args, **kwargs):
        customer_id = kwargs.get('customer_id')
        customer = request.user.company.related_customers.get(pk=customer_id)
        notes = self.queryset.filter(customer=customer)
        return Response(NoteSerializer(notes, many=True).data, status=status.HTTP_200_OK)

    def delete_note(self, request, *args, **kwargs):
        note_id = kwargs.get('note_id')
        customer_id = kwargs.get('customer_id')
        customer = request.user.company.related_customers.get(pk=customer_id)
        note = customer.related_notes.get(pk=note_id)
        if request.user.is_contractor():
            note.delete(soft_delete=True)
        elif request.user.is_sales() and note.author == request.user:
            note.delete(soft_delete=True)

        return Response('', status=status.HTTP_202_ACCEPTED)

    def save_note(self, request, *args, **kwargs):
        author = request.user
        customer_id = kwargs.get('customer_id')
        customer = request.user.company.related_customers.get(pk=customer_id)

        note_pk = request.data.get('id')
        if note_pk:
            note = customer.related_notes.get(pk=note_pk)
            note.text = request.data.get('text')
            note.save()
        else:
            if request.user.has_perm('crm.add_customernote'):
                note = CustomerNote(**request.data)
                note.author = author
                note.customer = customer
                note.save()
            else:
                return Response('Has no permission.', status=status.HTTP_403_FORBIDDEN)
        return Response(NoteSerializer(note).data, status=status.HTTP_200_OK)
