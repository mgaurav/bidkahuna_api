import json

from django.urls import reverse
from paypal.standard.forms import PayPalPaymentsForm
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework import viewsets

from apiapp.serializers.billing import BillingPlanSerializer
from biddingapp import settings
from crm.models import BillingTransaction


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = BillingTransaction
        fields = ['comment', 'created_at']


class CompanyBillingViewSet(viewsets.ViewSet):
    permission_classes = (permissions.IsAuthenticated,)

    def billing_status(self, request, *args):
        company = request.user.company
        billing = company.billing

        out_form = ''
        if not billing.is_subscribe:
            paypal_dict = {
                "cmd": "_xclick-subscriptions",
                "business": settings.PAYPAL_EMAIL,
                "a3": billing.used_plan.price,
                "p3": 1,
                "t3": 'M',
                "src": "1",
                "sra": "1",
                "no_note": "1",
                "item_name": "BID KAHUNA Subscription",
                "notify_url": settings.HOST_URL + reverse('paypal-ipn'),
                "return_url": settings.HOST_URL + '/payment/success/',
                "cancel_return": settings.HOST_URL + '/payment/cancel/',
                "custom": json.dumps({"company_id": company.id}),
            }
            form = PayPalPaymentsForm(initial=paypal_dict, button_type="subscribe")
            out_form = form.render()

        transactions = BillingTransaction.objects.all()

        return Response({
            'active': company.is_active,
            'last_transaction': billing.last_transaction,
            'next_transaction': billing.next_check,
            'plan': BillingPlanSerializer(billing.used_plan).data,
            'subscribe_form': out_form,
            'transactions': TransactionSerializer(transactions, many=True).data
        })
