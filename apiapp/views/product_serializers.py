from rest_framework import serializers

from apiapp.utils.time_stamp_fields import TimestampField
from apiapp.views.tag_views import TagSerializer
from crm.models import Product, Brand, Category, ProductImage, ProductTag
from crm.models.product_categories import ProductCategories


class BrandSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ['id', 'name']


class ImageSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductImage
        fields = ['url']


class CategorySimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'name']


class ImageFormSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductImage
        fields = ['id', 'url']


class TagsSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductTag
        fields = ['id', 'tag']


class ProductSimpleSerializer(serializers.ModelSerializer):
    brand = BrandSimpleSerializer(read_only=True)
    category = CategorySimpleSerializer(read_only=True)
    subcategory = CategorySimpleSerializer(read_only=True)
    related_images = ImageSimpleSerializer(read_only=True, many=True)
    related_tags = TagsSimpleSerializer(read_only=True, many=True)

    class Meta:
        model = Product
        fields = ['id', 'pk', 'name', 'brand', 'category', 'subcategory', 'is_publish', 'taxable', 'enable_discount', 'show_dimension_note', 'related_images',
                  'related_tags', 'brochure_url',
                  'warranty']


class RelatedCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCategories
        fields = ['category_id', 'subcategory_id']


class ProductFormSerializer(serializers.ModelSerializer):
    related_images = ImageFormSerializer(read_only=True, many=True)
    related_products = ProductSimpleSerializer(read_only=True, many=True)
    related_tags = TagSerializer(read_only=True, many=True)
    related_categories = RelatedCategorySerializer(read_only=True, many=True)

    class Meta:
        model = Product
        fields = ['id', 'pk', 'name', 'details', 'warranty', 'brand', 'category',
                  'subcategory', 'measurement', 'is_publish', 'taxable', 'enable_discount', 'show_dimension_note', 'product_data',
                  'related_images', 'brochure_url', 'related_products',
                  'related_categories', 'related_tags', 'brand_name', 'order']


class ProductSynchroSerializer(ProductFormSerializer):
    created_at = TimestampField(read_only=True)
    updated_at = TimestampField(read_only=True)
    deleted_at = TimestampField(read_only=True)

    def __init__(self, *args):
        super(ProductFormSerializer, self).__init__(args)
        self.Meta.fields += ['created_at', 'updated_at', 'deleted_at']
