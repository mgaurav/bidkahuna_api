from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from apiapp.permissions.billing_access_permission import BillingAccessPermission
from apiapp.permissions.contractor_access_permission import ContractorAccessPermission


class DiscountOpenView(APIView):
    permission_classes = (permissions.IsAuthenticated, BillingAccessPermission, ContractorAccessPermission)

    def post(self, request):
        open_discount_state = request.data.get('state')
        request.user.company.has_open_discount = open_discount_state
        request.user.company.save()
        return Response({'state': request.user.company.has_open_discount})
