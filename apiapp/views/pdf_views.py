from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from wkhtmltopdf.views import PDFTemplateResponse
from crm.models import Invoice
from crm.models import ChecklistQuestions
from crm.models import Checklist
import json


class PdfViewSet(viewsets.GenericViewSet):

    def render_html_debug(self, request, invoice_hash=None, *args, **kwargs):
        try:
            invoice = Invoice.objects.get(hash=invoice_hash)
            company = invoice.company
            order = invoice.order
            items = order.related_items.all()
            discounts = invoice.get_discounts()

            checklist = Checklist.objects.filter(pk=order.checklist_id).first
            questions = ChecklistQuestions.objects.all().filter(checklist_id=order.checklist_id)
            answers = eval(order.checklist_answers)
            answers = json.dumps(answers)
            checklist_answers = json.loads(answers)
            keys = []
            values = []
            for mykey in checklist_answers:
                keys.append(int(mykey))
                values.append(checklist_answers[mykey])
            checklist_answers = {}
            index = 0;
            for key in keys:
                checklist_answers[key] = values[index]
                index = index + 1

            context = {
                'invoice': invoice,
                'company': company,
                'order': order,
                'items': items,
                'discounts': discounts,
                'checklist': checklist,
                'questions': questions,
                'checklist_answers': checklist_answers
            }

            return render(template_name='invoice_pdf_debug.html', context=context, request=request)
        except Invoice.DoesNotExist:
            return Response('Document not found.')

    def render_html(self, request, invoice_hash=None, *args, **kwargs):
        try:
            invoice = Invoice.objects.get(hash=invoice_hash)
            company = invoice.company
            order = invoice.order
            items = order.related_items.all()
            discounts = invoice.get_discounts()
            checklist = Checklist.objects.filter(pk = order.checklist_id).first()
            questions = ChecklistQuestions.objects.all().filter(checklist_id=order.checklist_id)
            answers = eval(order.checklist_answers)
            answers = json.dumps(answers)
            checklist_answers = json.loads(answers)
            keys = []
            values = []
            for mykey in checklist_answers:
                keys.append(int(mykey))
                values.append(checklist_answers[mykey])
            checklist_answers = {}
            index = 0;
            for key in keys:
                checklist_answers[key] = values[index]
                index = index + 1

            context = {
                'invoice': invoice,
                'company': company,
                'order': order,
                'items': items,
                'discounts': discounts,
                'checklist': checklist,
                'questions': questions,
                'checklist_answers': checklist_answers
            }

            return render(template_name='invoice_pdf.html', context=context, request=request)
        except Invoice.DoesNotExist:
            return Response('Document not found.')

    def render_pdf(self, request, invoice_hash=None, *args, **kwargs):

        try:
            invoice = Invoice.objects.get(hash=invoice_hash)
            company = invoice.company
            order = invoice.order
            items = order.related_items.all()
            discounts = invoice.get_discounts()

            checklist = Checklist.objects.filter(pk=order.checklist_id).first()
            questions = ChecklistQuestions.objects.all().filter(checklist_id=order.checklist_id)
            checklist_answers = {}
            checklist_notes = {}

            if order.checklist_answers != None:

                answers = eval(order.checklist_answers)
                answers = json.dumps(answers)
                checklist_answers = json.loads(answers)
                checklist_notes = {}
                keys=[]
                values=[]
                for mykey in checklist_answers:
                    if(mykey.isdigit()):
                        keys.append(int(mykey))
                        values.append(checklist_answers[mykey])
                    else:
                        onlyid = mykey.split("_")[0]
                        checklist_notes[int(onlyid)] = checklist_answers[mykey]

                checklist_answers = {}
                index = 0
                for key in keys:
                    checklist_answers[key] = values[index]
                    index = index + 1

            response = PDFTemplateResponse(request=request,
                                           template="invoice_pdf.html",
                                           filename='order ' + invoice.uid + ".pdf",
                                           context={
                                               'invoice': invoice,
                                               'company': company,
                                               'order': order,
                                               'items': items,
                                               'discounts': discounts,
                                               'checklist': checklist,
                                               'questions': questions,
                                               'checklist_answers': checklist_answers,
                                               'checklist_notes': checklist_notes
                                           },
                                           show_content_in_browser=True,
                                           cmd_options={'margin-top': 20, }
                                           )
            return response
        except Invoice.DoesNotExist:
            return Response('Document not found.')
