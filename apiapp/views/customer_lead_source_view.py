from rest_framework import permissions
from rest_framework import serializers
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apiapp.permissions.billing_access_permission import BillingAccessPermission
from crm.models.customer_lead_source import CustomerLeadSource


class LeadSourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerLeadSource
        fields = ['id', 'value']


class CustomerLeadSourceView(APIView):
    permission_classes = (permissions.IsAuthenticated, BillingAccessPermission,)

    def get(self, request):
        query = request.GET.get('q')
        values = CustomerLeadSource.objects.all() \
            .filter(company=request.user.company, value__icontains=query)
        result = {
            "data": LeadSourceSerializer(values, many=True).data,
            "count": values.count()
        }
        return Response(result, status=status.HTTP_200_OK)
