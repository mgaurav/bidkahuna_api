from rest_framework import permissions
from rest_framework import serializers
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from django.db.models import Q

from apiapp.paginations.page_number_pagination import PageNumberPagination
from apiapp.permissions.access_object_permission import AccessObjectPermission
from apiapp.permissions.billing_access_permission import BillingAccessPermission
from apiapp.serializers.customer import CustomerSerialize
from apiapp.serializers.prospect_status import ProspectStatusSerializer
from apiapp.serializers.requests.save_customer_request import SaveCustomerRequest
from apiapp.utils.time_stamp_fields import stamp_to_time
from crm.models import Customer, ProspectStatus, ValidationError, CustomerLeadSource, User
from rest_framework.exceptions import ValidationError as RestValidationError
from django.utils.datetime_safe import datetime

from datetime import date

class UserLeadSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'full_name']


class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.active_objects.active()
    permission_classes = (permissions.IsAuthenticated, BillingAccessPermission, AccessObjectPermission,)
    serializer_class = CustomerSerialize
    pagination_class = PageNumberPagination

    def get_queryset(self):
        queryset = self.queryset.filter(company=self.request.user.company)
        if self.request.user.is_sales():
            queryset = Customer.objects.all().filter(user_id=self.request.user.id, deleted_at__isnull=True)
        timestamp = self.request.GET.get('t')
        if timestamp:
            timestamp = stamp_to_time(timestamp)
            queryset = queryset.filter(
                Q(created_at__gt=timestamp) | Q(updated_at__gt=timestamp) | Q(deleted_at__gt=timestamp))

        search_query = self.request.GET.get('search_query')
        if search_query:
            queryset = queryset.filter(
                Q(first_name__icontains=search_query) |
                Q(last_name__icontains=search_query) |
                Q(email__icontains=search_query) |
                Q(phone__icontains=search_query) |
                Q(address__icontains=search_query)
            )

        sort_mode = self.request.GET.get('sort_mode')
        if sort_mode:
            queryset = queryset.order_by(sort_mode)

        return queryset

    def get_serializer_class(self):
        if self.action == 'create':
            return SaveCustomerRequest
        if self.action == 'update':
            return SaveCustomerRequest
        return self.serializer_class

    def perform_destroy(self, instance):
            instance.deleted_at = datetime.now()
            instance.save()

    def perform_update(self, serializer):
        serializer.is_valid(raise_exception=True)
        for (key, value) in serializer.validated_data.items():
            setattr(serializer.instance, key, value)

        # Prospect Status
        prospect = self.request.data.get('prospect')
        if prospect:
            serializer.instance.prospect_id = prospect

        # Lead Source
        try:
            lead_source = self.request.data.get('lead_source')
            if lead_source:
                resource, created = CustomerLeadSource.objects.get_or_create(
                    value=lead_source.get('value', None),
                    company=self.request.user.company)
                serializer.instance.lead_source = resource
        except Exception as e:
            pass

        # Lead Owner
        lead_owner = self.request.data.get('lead_owner')
        if lead_owner:
            lead_obj = User.objects.all().get(id=lead_owner)
            serializer.instance.lead_owner = lead_obj

        try:
            #serializer.instance.full_clean()
            serializer.instance
            serializer.save()
        except ValidationError as e:
            pass
            #raise RestValidationError(e.message_dict)

    def perform_create(self, serializer):
        serializer.save(company=self.request.user.company, user=self.request.user)

    def retrieve(self, request, *args, **kwargs):
        instance_object = self.get_object()
        serializer_object = self.get_serializer(instance_object)

        option_prospect_objects = ProspectStatus.objects.all().filter(company=self.request.user.company)
        serializer_option_prospect = ProspectStatusSerializer(instance=option_prospect_objects, many=True)

        lead_owner_objects = self.request.user.company.users.all().filter(role='SALESPERSON', is_active=True)
        serializer_lead_owner = UserLeadSerializer(instance=lead_owner_objects, many=True)

        result = {
            'data': serializer_object.data,
            'options': {
                'prospect': serializer_option_prospect.data,
                'lead_owners': serializer_lead_owner.data
            },
            'use_lead_owner': lead_owner_objects.count() > 0
        }
        return Response(result)

    def form_options(self, request):
        option_prospect_objects = ProspectStatus.objects.all().filter(company=self.request.user.company)
        serializer_option_prospect = ProspectStatusSerializer(instance=option_prospect_objects, many=True)

        lead_owner_objects = self.request.user.company.users.all().filter(role='SALESPERSON', is_active=True)
        serializer_lead_owner = UserLeadSerializer(instance=lead_owner_objects, many=True)

        result = {
            'options': {
                'prospect': serializer_option_prospect.data,
                'lead_owners': serializer_lead_owner.data
            },
            'use_lead_owner': lead_owner_objects.count() > 0
        }
        return Response(result)

    def search_customer(self, request, *args, **kwargs):
        """
        Search customer by query
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        query = kwargs.get('search_query')
        customers = self.get_queryset() \
            .filter(
            Q(first_name__icontains=query) |
            Q(last_name__icontains=query) |
            Q(email__icontains=query) |
            Q(phone__icontains=query) |
            Q(address__icontains=query)
        )

        return Response(CustomerSerialize(customers, many=True).data, status=status.HTTP_200_OK)
