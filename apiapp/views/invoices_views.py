from rest_framework import permissions
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import serializers

from apiapp.views.order_views import OrderItemSerializer, CustomerSerializer
from apiapp.views.user_views import UserSerializer
from crm.models import Invoice, Order, Company, Discount


class OrderSerializer(serializers.ModelSerializer):
    related_items = OrderItemSerializer(read_only=True, many=True)
    user = UserSerializer(read_only=True, many=False)
    customer = CustomerSerializer(read_only=True)

    class Meta:
        model = Order
        fields = ['id', 'status', 'related_items', 'user', 'status', 'customer','show_review_link_on_pdf']


class DiscountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Discount
        fields = ['id', 'name', 'type', 'value']


class CompanySerializer(serializers.ModelSerializer):
    related_discounts = DiscountSerializer(many=True)

    class Meta:
        model = Company
        fields = ['name',
                  'address', 'address_city', 'address_state', 'address_zip', 'logo',
                  'related_discounts', 'agreements', 'phone', 'website', 'email_id']


class InvoiceSerialize(serializers.ModelSerializer):
    order = OrderSerializer(read_only=True, many=False)
    company = CompanySerializer(read_only=True, many=False)
    discounts = serializers.ListField(source='get_discounts')
    display_signature_boxes = serializers.SerializerMethodField()

    def get_display_signature_boxes(self, obj):
        return obj.company.display_signature_boxes

    class Meta:
        model = Invoice
        fields = ['id', 'uid', 'is_approved', 'created_at', 'company', 'order', 'type', 'price_itemize',
                  'discounts', 'display_signature_boxes','url_pdf']


class InvoiceViewSet(viewsets.ModelViewSet):
    queryset = Invoice.active_objects.all()
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = None

    def get_queryset(self):
        user = self.request.user
        company = user.company
        return self.queryset.filter(company=company)

    def delete_invoice(self, request, invoice_id=None, soft_delete=False, *args, **kwargs):
        invoice = self.get_queryset().get(pk=invoice_id)
        invoice.delete(soft_delete=soft_delete)
        return Response('', 200)

    def send_invoice(self, request, invoice_id=None):
        user = request.user
        company = user.company
        invoice = self.queryset.filter(company=company).get(pk=invoice_id)
        result = invoice.send_to_email(request.data.get('email'), request)
        return Response({'result': result})

    def get_invoice(self, request, *args, **kwargs):
        invoice_id = kwargs.get('invoice_id')
        user = request.user
        company = user.company
        invoice = self.queryset.filter(company=company).get(pk=invoice_id)

        return Response(InvoiceSerialize(invoice).data, status=status.HTTP_200_OK)

    def list_invoices(self, request, *args, **kwargs):
        invoices = self.get_queryset()
        return Response(InvoiceSerialize(invoices, many=True).data, status=status.HTTP_200_OK)

    def make_invoice(self, request, *args, **kwargs):

        order_id = kwargs.get('order_id')
        user = request.user
        company = user.company
        order = company.related_orders.get(pk=order_id)

        if not hasattr(order, 'related_invoice'):
            invoice = Invoice.objects.create(
                company=company,
                order=order
            )
        else:
            invoice = order.related_invoice


        return Response(InvoiceSerialize(invoice).data, status=status.HTTP_200_OK)

    def submit_invoice(self, request, *args, **kwargs):

        user = request.user

        company = user.company
        invoice_id = request.data.get('invoice_id')
        discounts = request.data.get('discounts')
        invoice = Invoice.objects.all().get(company=company, pk=invoice_id)
        ids = []
        for discount in discounts:
            ids.append(discount.get('id'))
        discount = Discount.objects.all().filter(company=company, pk__in=ids)
        invoice.discounts.set(discount)
        invoice.set_discounts(DiscountSerializer(discounts, many=True).data)
        invoice.signature_customer = request.data.get('signature_customer')
        invoice.signature_user = request.data.get('signature_user')
        invoice.invoice_notes = request.data.get('invoice_notes')
        isPdfShow = request.data.get('isPdfShow')
        # Close order
        invoice.order.close_order()
        print(isPdfShow)
        print(type(isPdfShow))
        if isPdfShow == True:
            invoice.order.show_review_link_on_pdf = True
        else:
            invoice.order.show_review_link_on_pdf = False
        invoice.order.save()
        # Type submit
        invoice_type = request.data.get('type')
        if invoice_type:
            invoice.type = invoice_type
        # Set agreement
        agreement = request.data.get('agreement')
        invoice.is_agreement = agreement
        # Itemize price
        invoice.price_itemize = request.data.get('price_itemize')
        invoice.save()
        # Make data invoice
        invoice.make_data()

        # Reset active order for user
        user.active_order = None

        user.save()
        return Response({
            'url_pdf': invoice.url_pdf()
        }, status=status.HTTP_200_OK)

    def load_invoice(self, request, *args, **kwargs):
        invoice = self.get_queryset().get(id=32)
        invoice_data = {"discounts": invoice.get_discounts()}

        return Response(invoice_data)

    def save_invoice(self, request, *args, **kwargs):
        invoice_id = request.data.get('id')
        invoice = self.get_queryset().get(id=invoice_id)
        invoice_data = invoice.p_data

        # Prepare discounts
        discounts = request.data.get('discounts')
        invoice_data["store_discounts"] = discounts

        invoice.p_data = invoice_data
        invoice.save()

        return Response({})
