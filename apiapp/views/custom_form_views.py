from django.contrib.auth.models import Group
from rest_framework import permissions
from rest_framework import viewsets
from rest_framework.response import Response


from apiapp.serializers.custom_form import CustomFormSerializer
from crm.models.custom_form import CustomForm
from django.core.exceptions import ObjectDoesNotExist


class CustomFormViewSet(viewsets.ModelViewSet):
    queryset = CustomForm.objects.all()
    serializer_class = CustomFormSerializer
    pagination_class = None

    def get_queryset(self):
            return self.queryset

    def create(self, request, job_id):
        try:
            result = self.queryset.get(job_id=request.data.get("job_id"))
            result.form_data = request.data.get("form_data");
            result.save();
        except ObjectDoesNotExist:
            self.queryset.create(**request.data);

        return Response('Successfully saved.')

    def load(self, serializer, job_id):
        try:
            result = self.queryset.get(job_id=job_id)
            return Response(CustomFormSerializer(result).data)
        except ObjectDoesNotExist:
            return Response('{}')
