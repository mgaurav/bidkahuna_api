from rest_framework import permissions
from rest_framework import serializers
from rest_framework import viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response

from apiapp.permissions.billing_access_permission import BillingAccessPermission
from apiapp.permissions.contractor_access_permission import ContractorAccessPermission
from crm.models.prospect_status import ProspectStatus


class ProspectStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProspectStatus
        fields = '__all__'


class CompanyProspectView(viewsets.ModelViewSet):
    serializer_class = ProspectStatusSerializer
    queryset = ProspectStatus.objects.all()
    permission_classes = (permissions.IsAuthenticated, BillingAccessPermission,)
    pagination_class = None

    def get_queryset(self):
        return self.queryset.filter(company=self.request.user.company)

    def update(self, request, *args, **kwargs):
        if not ContractorAccessPermission().has_permission(request=request):
            raise PermissionDenied(ContractorAccessPermission.message)

        data = request.data
        use_obj_id = []
        for obj in data:
            obj_id = obj.get('id')
            obj["company"] = self.request.user.company.id
            if obj_id:
                instance = self.get_queryset().get(id=obj_id)
            else:
                instance = ProspectStatus(company=self.request.user.company)

            object_serialize = ProspectStatusSerializer(data=obj, instance=instance)
            object_serialize.is_valid(raise_exception=True)
            object_serialize.save()
            use_obj_id.append(object_serialize.instance.id)

        items = self.get_queryset().exclude(id__in=use_obj_id)
        items.delete()

        return Response(ProspectStatusSerializer(self.get_queryset(), many=True).data)
