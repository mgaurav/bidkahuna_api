from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.serializers import ModelSerializer
from rest_framework.views import APIView

from crm.models.option import Option
from crm.models.registraion_form_storage import RegistrationFormStorage


class RegisterFormSerializer(ModelSerializer):
    class Meta:
        model = RegistrationFormStorage
        exclude = ('id',)


class RegistrationView(APIView):
    throttle_scope = 'register_form'

    def post(self, request):
        form_serializer = RegisterFormSerializer(data=request.data.get('form'))
        form_serializer.is_valid(raise_exception=True)
        form_serializer.save()
        form_serializer.instance.send_to_email()
        return Response({'result': True})


@api_view(['GET'])
def get_terms(request):
    return Response({'terms': Option.objects.all().get(name='terms').value})


@api_view(['GET'])
def get_privacy_policy(request):
    return Response({'privacy_policy': Option.objects.all().get(name='privacy').value})
