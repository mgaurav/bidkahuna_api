from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response

from crm.models import Company, User


class DashboardView(generics.RetrieveAPIView):
    """
    Get summary info for a Dashboard
    """

    def get(self, request, *args, **kwargs):
        response = {}
        user = request.user
        if user.role == 'SUPERADMIN':
            response = {
                "company": {"count": User.objects.all().filter(role='CONTRACTOR').count()}
            }
        elif user.role == 'CONTRACTOR':
            # Base Response
            response = {
                "customers": {"count": user.company.related_customers.filter(deleted_at__isnull=True).count()},
                "products": {"count": user.company.related_products.filter(deleted_at__isnull=True).count()},
                "jobs": {"count": user.company.related_orders.filter(deleted_at__isnull=True).count()},
                "billing": {
                    "is_active": user.company.is_active
                }
            }

        return Response(response, status=status.HTTP_200_OK)
