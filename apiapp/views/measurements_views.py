from rest_framework import viewsets, permissions
from rest_framework import serializers
from crm.models import MeasurementType


class MeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = MeasurementType
        fields = '__all__'


class MeasurementViewSet(viewsets.ModelViewSet):
    serializer_class = MeasurementSerializer
    permission_classes = (permissions.IsAuthenticated,)
    queryset = MeasurementType.objects.all()
    pagination_class = None
