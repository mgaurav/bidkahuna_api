import calendar
from datetime import date, datetime

from rest_framework import permissions
from rest_framework import serializers
from rest_framework import viewsets
from rest_framework.response import Response

from apiapp.permissions.billing_access_permission import BillingAccessPermission
from apiapp.serializers.customer import CustomerSerialize
from apiapp.utils.time_stamp_fields import stamp_to_time, TimestampField
from crm.models import Order
from crm.models.order_action import OrderAction


class FullActionSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderAction
        fields = ['id', 'date', 'description']


class ActionSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderAction
        fields = ['id', 'date', 'description']


class OrderActionViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated, BillingAccessPermission,)
    serializer_class = ActionSerializer
    queryset = OrderAction.active_objects.all()
    lookup_url_kwarg = 'action_id'
    pagination_class = None

    def get_queryset(self):
        order_id = self.kwargs.get('order_id')
        try:
            order = self.get_order_queryset().get(pk=order_id)
            return self.queryset.filter(order=order)
        except Order.DoesNotExist as e:
            raise serializers.ValidationError({'error': e})

    def get_order_queryset(self):
        queryset = self.request.user.company.related_orders.all()
        if not self.request.user.is_contractor():
            queryset = self.request.user.related_orders.all()
        return queryset

    def perform_create(self, serializer):
        order_id = self.kwargs.get('order_id')
        order = self.get_order_queryset().get(id=order_id)
        try:
            serializer.save(order=order)
        except Order.DoesNotExist as e:
            raise serializers.ValidationError({'order': e})

    def list_actions(self, request):
        view_param = request.GET.get('view')
        view_date = stamp_to_time(request.GET.get('date'))
        if view_param == 'month':
            date_start = date(view_date.year, view_date.month, 1)
            date_end = date(view_date.year, view_date.month, calendar.mdays[view_date.month])
        queryset = OrderAction.active_objects.all() \
            .filter(order__deleted_at__isnull=True) \
            .filter(order__company=request.user.company,
                    date__gte=date_start,
                    date__lte=date_end)
        if request.user.is_sales():
            queryset = queryset.filter(order__user=request.user)

        return Response({'data': ActionSerializer(queryset, many=True).data})

    def full_data_action(self, request, action_id=None):
        queryset_action = OrderAction.active_objects.all() \
            .filter(order__company=self.request.user.company)

        if not self.request.user.is_contractor():
            queryset_action.filter(order__user=request.user)

        queryset_action = queryset_action.get(pk=action_id)

        actions_serializer = FullActionSerializer(queryset_action, many=False)
        customer_serializer = CustomerSerialize(queryset_action.order.customer, many=False)

        invoice_uid = None
        try:
            invoice_uid = queryset_action.order.related_invoice.uid
        except:
            pass

        return Response({
            'data': {
                'action': actions_serializer.data,
                'customer': customer_serializer.data,
                'invoice': {
                    'uid': invoice_uid
                }
            }
        })
