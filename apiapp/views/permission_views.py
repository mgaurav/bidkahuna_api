from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from rest_framework import permissions
from rest_framework import serializers
from rest_framework import viewsets

from apiapp.serializers.content_type import ContentTypeSerializer


class PermissionSerializer(serializers.ModelSerializer):
    content_type = ContentTypeSerializer()

    class Meta:
        model = Permission
        fields = ['id', 'name', 'content_type']


class PermissionViewSet(viewsets.ModelViewSet):
    queryset = Permission.objects.all() \
        .filter(group__name='SALESPERSONS')
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = PermissionSerializer
    pagination_class = None
