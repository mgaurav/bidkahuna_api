from rest_framework import permissions
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import serializers
from django.db import transaction

from apiapp.views.product_serializers import BrandSimpleSerializer, CategorySimpleSerializer, \
    ImageSimpleSerializer
from crm.models import Product, Order, OrderItem, Customer
import crm.measurements


class ProductSerializer(serializers.ModelSerializer):
    brand = BrandSimpleSerializer(read_only=True)
    category = CategorySimpleSerializer(read_only=True)
    subcategory = CategorySimpleSerializer(read_only=True)
    related_images = ImageSimpleSerializer(read_only=True, many=True)

    class Meta:
        model = Product
        fields = ['id', 'name', 'brand', 'category', 'subcategory', 'is_publish', 'taxable', 'enable_discount', 'show_dimension_note', 'related_images']


class OrderItemSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = OrderItem
        fields = ['id', 'product', 'get_data', 'price', 'note', 'dimension_notes']


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):
    related_items = OrderItemSerializer(read_only=True, many=True)
    customer = CustomerSerializer(read_only=True, many=False)

    class Meta:
        model = Order
        fields = ['id', 'status', 'related_items', "customer"]


class OrderItemViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = OrderItemSerializer
    pagination_class = None

    def get_order_queryset(self):
        queryset = Order.objects.all()
        queryset = queryset.filter(company=self.request.user.company)
        if not self.request.user.is_contractor():
            queryset = queryset.filter(user=self.request.user)
        return queryset

    def get_order_item_queryset(self):
        queryset = OrderItem.objects.all()
        queryset = queryset.filter(order__company=self.request.user.company)
        if not self.request.user.is_contractor():
            queryset = queryset.filter(order__user=self.request.user)
        return queryset

    @transaction.atomic
    def save_order_item(self, request):
        user = request.user
        company = user.company
        order_id = request.data.get('order_id')
        product_id = request.data.get('product_id')
        item_description = request.data.get('item_description')
        item_id = request.data.get('item_id')
        if not item_id:
            item_id = None
        order = self.get_order_queryset().select_for_update().get(id=order_id)
        product = Product.objects.get(company=company, pk=product_id)
        order_item, created = order.related_items.get_or_create(pk=item_id, order=order, product=product)
        order_item.note = request.data.get('note')
        order_item.dimension_notes = request.data.get('dimension_notes')
        order_item.set_data(item_description)
        class_name = product.measurement.class_name
        measurement_class = getattr(crm.measurements, class_name)
        measurement = measurement_class(product, order_item)
        measurement.process()
        order_item.save()
        return Response({"item_id": order_item.pk}, status=status.HTTP_200_OK)

    def change_order_item(self, request, order_id=None, order_item_id=None, *args, **kwargs):
        product_id = request.data.get('new_product_id')
        order_item = self.get_order_item_queryset().get(id=order_item_id)
        order_item.product_id = product_id
        product = Product.objects.get(company=request.user.company, pk=product_id)
        class_name = product.measurement.class_name
        measurement_class = getattr(crm.measurements, class_name)
        measurement = measurement_class(product, order_item)
        measurement.process()
        order_item.save()
        return Response('ok')

    def brand_based_change_order_items(self, request, order_id=None, order_item_id=None, *args, **kwargs):
        order = self.get_order_queryset().get(id=order_id)
        order_items = order.related_items.filter(order=order)
        brand_id = request.data.get('brand_id')
        for order_item in order_items:
            relatedItem = Product.objects.get(pk=order_item.product_id)
            swap_products = Product.objects.all().filter(company=request.user.company, brand_id=brand_id, subcategory=relatedItem.subcategory, is_publish=True)
            for swap_product in swap_products:
                if(swap_product.brand):
                    order_item.product_id = swap_product.id
                    class_name = swap_product.measurement.class_name
                    measurement_class = getattr(crm.measurements, class_name)
                    measurement = measurement_class(swap_product, order_item)
                    measurement.process()
                    order_item.save()

        return Response('ok')

    def remove_order_item(self, request, *args, **kwargs):
        order_item_id = kwargs.get('order_item_id')
        order_item = self.get_order_item_queryset().get(pk=order_item_id)
        order_item.delete()
        return Response({}, status=status.HTTP_202_ACCEPTED)
