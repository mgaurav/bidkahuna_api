from django.contrib.auth.models import Group
from rest_framework import permissions
from rest_framework import viewsets
from rest_framework.response import Response


from apiapp.serializers.estimate_serializer import EstimateFormSerializer
from crm.models.estimate_form import EstimateForm
from django.core.exceptions import ObjectDoesNotExist


class EstimateFormViewSet(viewsets.ModelViewSet):
    queryset = EstimateForm.objects.all()
    serializer_class = EstimateFormSerializer
    pagination_class = None

    def get_queryset(self):
        if  self.request.user.role == 'SUPERADMIN' or self.request.user.role == 'SALESPERSON':
            return self.queryset.all()
        else:
            return self.queryset.filter(user=self.request.user)

    def load(self, serializer, pk=None):
        if pk:
            try:
                result = self.queryset.get(id=pk,user=self.request.user)
                return Response(EstimateFormSerializer(result).data)
            except ObjectDoesNotExist:
                return Response('{}')
        else:
            result = self.get_queryset()
            return Response(EstimateFormSerializer(result,many=True).data)

    def create(self, request, *args, **kwargs):
        data = EstimateForm.objects.create(user=request.user,form_data=request.data.get('form_data'))
        data.save()
        return Response(EstimateFormSerializer(data).data)
