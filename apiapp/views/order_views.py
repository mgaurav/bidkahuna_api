from rest_framework import permissions
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import serializers

from apiapp.paginations.page_number_pagination import PageNumberPagination
from apiapp.serializers.customer import ShortCustomerSerializer
from apiapp.utils.time_stamp_fields import TimestampField
from apiapp.views.product_serializers import BrandSimpleSerializer, CategorySimpleSerializer, \
    ImageSimpleSerializer
from crm.models import Invoice
from crm.models import Product, Order, OrderItem, ObjectDoesNotExist, Customer, Checklist
import crm.measurements
from crm.models.order_action import OrderAction


class ProductSerializer(serializers.ModelSerializer):
    brand = BrandSimpleSerializer(read_only=True)
    category = CategorySimpleSerializer(read_only=True)
    subcategory = CategorySimpleSerializer(read_only=True)
    related_images = ImageSimpleSerializer(read_only=True, many=True)

    class Meta:
        model = Product
        fields = ['id', 'name', 'brand', 'category', 'subcategory', 'is_publish', 'taxable', 'enable_discount', 'show_dimension_note', 'related_images']


class OrderItemSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = OrderItem
        fields = ['id', 'product', 'get_data', 'price', 'note', 'dimension_notes']


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = '__all__'


class RelatedActionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderAction
        fields = ['id', 'date', 'description']


class OrderSerializer(serializers.ModelSerializer):
    related_items = OrderItemSerializer(read_only=True, many=True)
    customer = CustomerSerializer(read_only=True, many=False)
    related_actions = RelatedActionsSerializer(read_only=True, many=True)

    class Meta:
        model = Order
        fields = ['id', 'status', 'related_items', 'checklist_id', 'checklist_answers', "customer", 'related_actions','show_review_link_on_pdf']


class UserDataSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        return instance.first_name + ' ' + instance.last_name


class InvoiceDataSerializer(serializers.ModelSerializer):
    created_at = TimestampField()

    class Meta:
        model = Invoice
        fields = ['id', 'uid', 'created_at', 'url_pdf']


class ShortOrderSerializer(serializers.ModelSerializer):
    user = UserDataSerializer(many=False, read_only=True)
    related_invoice = InvoiceDataSerializer(many=False, read_only=True)
    customer = ShortCustomerSerializer(many=False, read_only=True)
    created_at = TimestampField()

    class Meta:
        model = Order
        fields = ['id', 'status', 'user', 'created_at', 'related_invoice', 'customer']


class OrderViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    pagination_class = PageNumberPagination

    def get_queryset(self):
        queryset = self.queryset \
            .filter(company=self.request.user.company, deleted_at__isnull=True)
        if self.request.user.is_sales():
            queryset = queryset.filter(user=self.request.user)
        if self.request.GET.get('customer_id'):
            customer_id = self.request.GET.get("customer_id")
            customer = Customer.objects.all().get(pk=customer_id)
            queryset=Order.objects.all() \
                .filter(customer=customer)
        return queryset

    def get_serializer_class(self):
        kwargs_serializer_class = self.kwargs.get('serializer_class')
        if kwargs_serializer_class:
            return kwargs_serializer_class
        return self.serializer_class

    def save_as_draft(self, request, order_id=None):
        order = self.get_queryset().get(pk=order_id)
        order.save_as_draft(self.request.user)
        return Response('')

    def set_current_order(self, request, order_id=None):
        """
        Set this order as current for the user
        :param request:
        :param order_id:
        :return:
        """
        try:
            order = self.get_queryset().get(pk=order_id)
            order.set_as_active_for_user(request.user)
            return Response('', status=status.HTTP_202_ACCEPTED)
        except Order.DoesNotExist:
            return Response('', status=status.HTTP_404_NOT_FOUND)

    def exit_current_order(self, request):
        try:
            request.user.active_order_id = None
            request.user.save()
            return Response('', status=status.HTTP_200_OK)
        except Exception:
            return Response('', status=status.HTTP_404_NOT_FOUND)

    """
        ORDER VIEWS
    """

    def current_order(self, request):
        """
        Get current Worksheet
        :param request:
        :return:
        """
        current_order = request.user.active_order
        if current_order:
            order = self.get_queryset().get(pk=current_order.id)
            order_items = order.related_items.filter(order=order)
            all_brands = []
            for order_item in order_items:
                current_product = Product.objects.get(pk=order_item.product_id)
                products = Product.objects.all().filter(company=request.user.company,
                                                        subcategory=current_product.subcategory, is_publish=True)

                for product in products:
                    if (product.brand):
                        all_brands.append(product.brand)

            all_brands = set(all_brands)
            pricesOfSuggestionProduct = []
            for order_item in order_items:
                orderItems = {}
                relatedItems = Product.objects.all().filter(company=request.user.company,
                                                            subcategory=order_item.product.subcategory, is_publish=True)

                for brand in all_brands:
                    brand_price = order_item.price
                    for product in relatedItems:
                        if (product.brand and product.brand.id == brand.id and order_item.product.measurement == product.measurement):
                            class_name = product.measurement.class_name
                            measurement_class = getattr(crm.measurements, class_name)
                            measurement = measurement_class(product, order_item)
                            measurement.process()
                            brand_price = order_item.price

                    orderItems[str(brand.id)+"-"+brand.name] = brand_price

                pricesOfSuggestionProduct.append(orderItems)
            return Response({"relatedProductPrice":pricesOfSuggestionProduct,"data":OrderSerializer(current_order).data}, status=status.HTTP_200_OK)
        else:
            return Response({}, status=status.HTTP_204_NO_CONTENT)

    def create_order(self, request):
        """
        Create new Worksheet
        :param request:
        :return:
        """
        user = request.user
        company = user.company
        customer_id = request.data.get("customer_id")
        customer = Customer.objects.all().get(pk=customer_id, company=company)
        Order.objects.all() \
            .filter(customer=customer, user=user, company=company, status="ACTIVE") \
            .update(status="DRAFT")
        order = Order.objects.create(customer=customer, user=user, company=company, status="ACTIVE")
        order.set_as_active_for_user(user)
        result = {"id": order.id}
        return Response(result, status=status.HTTP_201_CREATED)

    def remove_order(self, request, *args, **kwargs):
        """
        Remove Worksheet by Id
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        user = request.user
        company = user.company
        order_id = kwargs.get('order_id')
        try:
            order_queryset = self.get_queryset()
            if user.is_sales():
                order_queryset = order_queryset.filter(user=user, company=company)
            order = order_queryset.get(pk=order_id)
            order.delete(soft_delete=True)
            user.active_order = None
            user.save()
            return Response({}, status=status.HTTP_202_ACCEPTED)
        except ObjectDoesNotExist:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

    def save_checklist_data(self, request, *args, **kwargs):
        current_order = request.user.active_order
        try:
            if current_order:
                order = self.get_queryset().get(pk=current_order.id)
                checklist_id = request.data.get('checklist_id')
                answer_data = request.data.get('answer_data')
                if checklist_id:
                    order.checklist_id = checklist_id
                if answer_data:
                    order.checklist_answers = answer_data
                order.save()

                result = {"id": current_order.id, "checklist_id": checklist_id, "answer_data": answer_data}
                return Response(result, status=status.HTTP_202_ACCEPTED)
        except ObjectDoesNotExist:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

    def retrieve(self, request, *args, **kwargs):
        order_id = kwargs.get('order_id')
        order = Order.objects.get(pk=order_id)
        return Response(OrderSerializer(order).data)
