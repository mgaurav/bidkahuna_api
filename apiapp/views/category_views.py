from rest_framework import permissions
from rest_framework import serializers
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from django.core.files import File

from apiapp.utils.time_stamp_fields import TimestampField
from crm.models import Category, Product


class CategoryParentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['pk', 'id', 'name']


class CategorySerializer(serializers.ModelSerializer):
    parent = CategoryParentSerializer(read_only=True, required=False, default=None)
    created_at = TimestampField(read_only=True)
    updated_at = TimestampField(read_only=True)
    deleted_at = TimestampField(read_only=True)

    class Meta:
        model = Category
        fields = ['id', 'pk', 'order', 'name', 'parent', 'image_url', 'created_at', 'updated_at', 'deleted_at']

    # @todo Users and permission
    def update(self, instance, validated_data):
        # Initial data
        instance.order = self.initial_data.get('order')
        instance.name = self.initial_data.get('name')

        # Update parent category
        parent = self.initial_data.get('parent')
        if parent is None:
            instance.parent = None
        else:
            instance.parent = Category.objects.get(pk=parent.get('pk'))
        # Update image
        new_image = self.initial_data.get('_new_image')
        if new_image:
            instance.image = File(open('media/tmp/' + new_image, "rb"), name=new_image)

        instance.save()
        return instance


class CategoryViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Category.objects.all()
    pagination_class = None

    def list(self, request, *args, **kwargs):
        """
        Get params:
            filter = product
                List Categories which used in products.
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        queryset = self.get_queryset().filter(company=request.user.company)

        filter_param = request.GET.get('filter')
        if filter_param == 'product':
            company = request.user.company
            used_products_categories = Product.objects.all().filter(company=company).values('category').distinct()
            has_uncategory_product = next((
                value for value in used_products_categories
                if value["category"] is None
            ), None)

            queryset = Category.objects.all().filter(pk__in=used_products_categories)

        pk = kwargs.get('pk', request.data.get('pk', None))
        if pk is None:
            category = queryset
            many = True
        else:
            category = queryset.get(pk=pk)
            many = False

        result = CategorySerializer(category, many=many).data

        if filter_param == 'product' and has_uncategory_product:
            result.append({'id': None, 'name': 'No Category', 'parent': None})

        return Response(result)

    def update(self, request, *args, **kwargs):
        pk = kwargs.get('pk')

        if pk is None:
            pk = request.data.get('pk')

        if pk is None:
            category = Category(company=request.user.company)
            serializer = CategorySerializer(category, data=request.data)
        else:
            category = request.user.company.related_categories.get(pk=pk)
            serializer = CategorySerializer(category, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        category = Category.objects.get(pk=pk)
        category.delete()
        return Response('', status=status.HTTP_200_OK)

    def sort_category(self, request):
        categories = self.queryset.filter(company=request.user.company)
        for category in categories:
            order = next((row for row in request.data if row.get('id') == category.id)).get('order')
            if order:
                category.order = order
                category.save()

        return Response('')
