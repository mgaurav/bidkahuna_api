from rest_framework import permissions
from rest_framework import serializers
from rest_framework import viewsets

from apiapp.serializers.billing import BillingSerializer
from crm.models import Company


class BillingCompanySerializer(serializers.ModelSerializer):
    billing = BillingSerializer(read_only=True)

    class Meta:
        model = Company
        fields = ('id', 'billing')


class AdminBillingCompany(viewsets.ModelViewSet):
    queryset = Company.objects.all()
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser,)
    serializer_class = BillingCompanySerializer
    pagination_class = None
