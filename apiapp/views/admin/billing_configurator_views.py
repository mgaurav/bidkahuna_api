from rest_framework import permissions
from rest_framework import serializers
from rest_framework import viewsets
from crm.models import BillingPlan


class AdminBillingSerializer(serializers.ModelSerializer):
    class Meta:
        model = BillingPlan
        fields = ['id', 'name', 'title', 'description', 'price', 'related_company_count']


class AdminBillingConfigurator(viewsets.ModelViewSet):
    serializer_class = AdminBillingSerializer
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser,)
    queryset = BillingPlan.objects.all()
    pagination_class = None
