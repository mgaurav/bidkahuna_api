from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from apiapp.permissions.superuser_access_permission import SuperuserAccessPermission
from crm.models.option import Option


class OptionsViewSet(APIView):
    permission_classes = (IsAuthenticated, SuperuserAccessPermission,)

    def get(self, request, *args, **kwargs):
        option_name = kwargs.get('option')
        model, created = Option.objects.all().get_or_create(name=option_name)
        return Response({
            'name': option_name,
            'value': model.value
        })

    def post(self, request, *args, **kwargs):
        option_name = kwargs.get('option')
        model, created = Option.objects.all().get_or_create(name=option_name)
        model.value = self.request.data.get('value')
        model.save()
        return Response({'result': created})
