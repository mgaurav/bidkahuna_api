from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework_jwt.settings import api_settings

from crm.models import User

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


class AdminAuthView(APIView):
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser,)

    def post(self, request):
        user = User.objects.all().get(id=request.data.get('user_id'))
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        response = {
            'token': token
        }

        return Response(response)
