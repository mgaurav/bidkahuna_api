from django.contrib.auth.models import Group
from rest_framework import permissions
from rest_framework import viewsets
from rest_framework.response import Response


from apiapp.serializers.user import ContractorSerializer
from crm.models import User


class AdminUsersViewSet(viewsets.ModelViewSet):
    """
    Last revision: 07 dec 16
    """
    queryset = User.objects.all()
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser,)
    serializer_class = ContractorSerializer
    pagination_class = None

    def get_queryset(self):
        group = self.kwargs.get('group')
        if group:
            return self.queryset.filter(role=group)
        else:
            return self.queryset

    def perform_create(self, serializer):

        serializer.save(role='CONTRACTOR', is_active=False)
        group = Group.objects.get(name='CONTRACTOR')
        group.user_set.add(serializer.instance)

        # Parse password
        new_password = serializer.initial_data.get('password')
        if new_password:
            serializer.instance.set_password(new_password)

        # Set company name
        company = serializer.initial_data.get('company')
        company_name = company.get('name')
        if company_name:
            serializer.instance.company.name = company_name
            serializer.instance.company.save()

        # Set Billing Plan
        using_plan = serializer.initial_data.get('using_plan')
        serializer.instance.company.billing.used_plan_id = using_plan
        serializer.instance.company.billing.save()
        serializer.save()

    def perform_update(self, serializer):
        new_password = serializer.initial_data.get('password')
        if new_password:
            serializer.instance.set_password(new_password)
        # Update Billing Plan
        using_plan = serializer.initial_data.get('using_plan')
        serializer.instance.company.data_billing.used_plan_id = using_plan
        serializer.instance.company.data_billing.save()
        # Update company name
        company = serializer.initial_data.get('company')
        company_name = company.get('name')
        if company_name:
            serializer.instance.company.name = company_name
            serializer.instance.company.save()
        super(AdminUsersViewSet, self).perform_update(serializer)

    def toggle_trial(self, request, user_id=None):
        instance = self.queryset.filter(pk=user_id).get();
        instance.is_active = True;
        instance.save()
        if(instance.company.is_active):
            instance.company.is_active = False;
            instance.company.save()
        else:
            instance.company.is_active = True;
            instance.company.save()
        return Response('Success')