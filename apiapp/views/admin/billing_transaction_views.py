from rest_framework import permissions
from rest_framework import viewsets
from rest_framework.response import Response

from crm.models import Company, BillingTransaction


class AdminBillingTransaction(viewsets.ViewSet):
    permission_classes = (permissions.IsAuthenticated, permissions.IsAdminUser,)

    def change_balance(self, request, company_id=None):
        company = Company.objects.get(pk=company_id)
        BillingTransaction.objects.create(
            company=company,
            value=request.data.get('value'),
            type='V',
            comment="Change from the Admin panel."
        )
        return Response('')
