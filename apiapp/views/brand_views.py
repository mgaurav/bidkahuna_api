from rest_framework import permissions
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response

from apiapp.permissions.billing_access_permission import BillingAccessPermission
from apiapp.serializers.brand import BrandSerializer
from apiapp.serializers.requests.save_brand_request import SaveBrandRequest
from apiapp.utils.time_stamp_fields import stamp_to_time
from crm.models import Brand


class BrandViewSet(viewsets.ModelViewSet):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer
    permission_classes = (permissions.IsAuthenticated, BillingAccessPermission)
    pagination_class = None

    def get_queryset(self):
        queryset = self.queryset
        if self.kwargs.get('filter_by_company'):
            queryset = queryset.filter(company=self.request.user.company)
        timestamp = self.request.GET.get('t')
        if timestamp:
            timestamp = stamp_to_time(timestamp)
            queryset = queryset.get_update_summary(timestamp=timestamp)
        return queryset

    def get_serializer_class(self):
        if self.action == 'create':
            return SaveBrandRequest
        if self.action == 'update':
            return SaveBrandRequest
        if self.action == 'retrieve':
            return BrandSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        serializer.save(company=self.request.user.company)

    def create(self, request, *args, **kwargs):
        request.user.company.related_brands.create(**request.data)
        return Response(request.data, status=status.HTTP_200_OK)

    def list(self, request, *args, **kwargs):
        pk = kwargs.get('pk', request.data.get('id', None))
        if pk is None:
            brand = self.get_queryset()
            many = True
        else:
            brand = request.user.company.related_brands.get(pk=pk)
            many = False
        return Response(BrandSerializer(brand, many=many).data)

    def destroy(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        brand = request.user.company.related_brands.get(pk=pk)
        brand.delete()
        return Response('', status=status.HTTP_200_OK)
