from rest_framework import permissions
from rest_framework import serializers
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response

from apiapp.utils.time_stamp_fields import TimestampField, stamp_to_time
from crm.models import ProductTag


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductTag
        fields = ['id', 'tag']


class TagSynchroSerializer(serializers.ModelSerializer):
    created_at = TimestampField(read_only=True)
    updated_at = TimestampField(read_only=True)
    deleted_at = TimestampField(read_only=True)

    class Meta:
        model = ProductTag
        fields = ['id', 'tag', 'created_at', 'updated_at', 'deleted_at']


class TagViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = ProductTag.objects.all()
    serializer_class = TagSerializer
    pagination_class = None

    def create_tag(self, request, *args, **kwargs):
        tag_text = request.data.get("tag")
        company = request.user.company
        tag, created = self.queryset.get_or_create(tag=tag_text, company=company)
        return Response(TagSerializer(tag, many=False).data, status=status.HTTP_200_OK)

    def search(self, request, *args, **kwargs):
        query = request.GET.get('q')
        company = request.user.company
        tags = self.queryset.filter(company=company, tag__icontains=query)
        return Response(TagSerializer(tags, many=True).data, status=status.HTTP_200_OK)

    def synchronize_tags(self, request):
        company = request.user.company
        tags = self.queryset.filter(company=company)
        filter_stamp = request.GET.get('t')
        if filter_stamp:
            timestamp = stamp_to_time(filter_stamp)
            tags = tags.get_update_summary(timestamp=timestamp)
        return Response(TagSynchroSerializer(tags, many=True).data, status=status.HTTP_200_OK)
