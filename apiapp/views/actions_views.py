import calendar
from datetime import date

from rest_framework import permissions
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response

from apiapp.permissions.billing_access_permission import BillingAccessPermission
from apiapp.serializers.customer import CustomerSerialize
from apiapp.serializers.customer_action import ActionSerializer, ActionOrderSerializer
from apiapp.utils.time_stamp_fields import stamp_to_time
from crm.models.customer_action import CustomerAction
from crm.models.order_action import OrderAction


class ActionViews(viewsets.ViewSet):
    permission_classes = (permissions.IsAuthenticated, BillingAccessPermission,)

    def list_actions(self, request):
        query_param = request.GET.get('view')
        query_date = request.GET.get('date')

        if not query_param or not query_date:
            return Response('`view` and `date` parameters are required.', status=status.HTTP_400_BAD_REQUEST)

        view_date = stamp_to_time(query_date)
        if query_param == 'month':
            date_start = date(view_date.year, view_date.month, 1)
            date_end = date(view_date.year, view_date.month, calendar.mdays[view_date.month])
        else:
            return Response('`view` value is not supported.', status=status.HTTP_400_BAD_REQUEST)

        queryset = OrderAction.active_objects.all() \
            .filter(order__deleted_at__isnull=True) \
            .filter(order__company=request.user.company,
                    deleted_at__isnull=True,
                    date__gte=date_start,
                    date__lte=date_end)
        if request.user.is_sales():
            queryset = queryset.filter(order__user=request.user)

        order_actions = ActionOrderSerializer(queryset, many=True).data

        queryset = CustomerAction.active_objects.all() \
            .filter(customer__company=request.user.company,
                    deleted_at__isnull=True,
                    customer__deleted_at__isnull=True,
                    date__gte=date_start,
                    date__lte=date_end,
                    user=request.user)

        customer_actions = ActionSerializer(queryset, many=True).data

        response = order_actions + customer_actions

        return Response({'data': response})

    def retrieve_action(self, request, action_type=None, action_id=None):
        if action_type == 'customer':
            queryset = CustomerAction.active_objects.all() \
                .filter(customer__deleted_at__isnull=True) \
                .filter(customer__company=request.user.company)
            if not self.request.user.is_contractor():
                queryset = queryset.filter(user=request.user)
            queryset = queryset.get(id=action_id)
            serializer_class = ActionSerializer
            customer_object = queryset.customer
        elif action_type == 'order':
            queryset = OrderAction.active_objects.all() \
                .filter(order__deleted_at__isnull=True) \
                .filter(order__company=request.user.company)
            if not self.request.user.is_contractor():
                queryset = queryset.filter(order__user=request.user)
            queryset = queryset.get(id=action_id)
            serializer_class = ActionOrderSerializer
            customer_object = queryset.order.customer
        else:
            return Response('This type value is not supported.', status=status.HTTP_400_BAD_REQUEST)

        action_data = serializer_class(queryset, many=False).data
        customer_data = CustomerSerialize(customer_object, many=False).data

        return Response({
            'data': {
                'action': action_data,
                'customer': customer_data
            }
        })
