from rest_framework import permissions
from rest_framework import serializers
from rest_framework import viewsets
from rest_framework.response import Response

from crm.models import Discount


class DiscountSerialize(serializers.ModelSerializer):
    class Meta:
        model = Discount
        fields = '__all__'


class DiscountViewSet(viewsets.ModelViewSet):
    queryset = Discount.objects.all()
    permission_classes = (permissions.IsAuthenticated, permissions.DjangoModelPermissions,)
    serializer_class = DiscountSerialize
    pagination_class = None

    def get_queryset(self):
        return self.queryset.filter(company=self.request.user.company)

    def perform_create(self, serializer):
        serializer.save(company=self.request.user.company)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return Response({
            'data': {
                'discounts': serializer.data,
                'open_discount': self.request.user.company.has_open_discount
            }
        })
