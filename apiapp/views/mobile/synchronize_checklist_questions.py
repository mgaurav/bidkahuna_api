from rest_framework import serializers
from rest_framework.response import Response

from apiapp.utils.time_stamp_fields import TimestampField, \
    time_to_stamp

from apiapp.views.mobile.base_view import BaseMobileView
from crm.models import ChecklistQuestions


class ChecklistQuestionsSynchroSerializer(serializers.ModelSerializer):
    created_at = TimestampField()
    updated_at = TimestampField()

    class Meta:
        model = ChecklistQuestions
        fields = ['id', 'question','checklist_id', 'is_section','show_na_button','position','created_at', 'updated_at', 'deleted_at']

    def to_representation(self, instance):
        if instance.deleted_at:
            return {
                'id': instance.id,
                'deleted_at': time_to_stamp(instance.deleted_at)}
        else:
            return super(ChecklistQuestionsSynchroSerializer, self) \
                .to_representation(instance)


class SynchronizeChecklistQuestions(BaseMobileView):
    queryset = ChecklistQuestions.objects.all()

    def get_queryset(self):
        query_set = self.queryset
        return query_set

    # noinspection PyUnusedLocal
    def get(self, request):
        self.make_response()
        query_set = self.get_queryset()

        self.response_data['data']['active'] = ChecklistQuestionsSynchroSerializer(
            query_set.filter(deleted_at__isnull=True), many=True) \
            .data

        self.response_data['data']['deleted'] = ChecklistQuestionsSynchroSerializer(
            query_set.filter(deleted_at__isnull=False), many=True) \
            .data

        return Response(self.response_data)
