import sys
from django.core.exceptions import ValidationError
from rest_framework import serializers
from rest_framework.response import Response

from apiapp.utils.time_stamp_fields import TimestampField, \
    stamp_to_time, time_to_stamp
from apiapp.views.mobile.base_view import BaseMobileView
from crm.models import Customer


class CustomerSynchroSerializer(serializers.ModelSerializer):
    created_at = TimestampField(read_only=True)
    updated_at = TimestampField(read_only=True)

    class Meta:
        model = Customer
        fields = ['id', 'created_at', 'updated_at', 'first_name', 'last_name',
                  'spouse_name', 'spouse_phone', 'email', 'phone', 'address',
                  'city', 'state', 'zip']

    def to_representation(self, instance):
        if instance.deleted_at:
            return {
                'id': instance.id,
                'deleted_at': time_to_stamp(instance.deleted_at)
            }
        else:
            return super(CustomerSynchroSerializer, self) \
                .to_representation(instance)


class SynchronizeCustomers(BaseMobileView):
    queryset = Customer.objects.all()
    def get_queryset(self):
        query_set = self.queryset.filter(company=self.request.user.company)
        filter_t = self.request.query_params.get('t')
        if filter_t:
            time = stamp_to_time(filter_t)
            query_set = query_set.get_update_summary(timestamp=time)

        if self.request.user.is_sales():
            query_set = Customer.objects.all().filter(user_id=self.request.user.id)

        return query_set

    # noinspection PyUnusedLocal
    def get(self, request):
        self.make_response()

        query_set = self.get_queryset()

        active_customers = CustomerSynchroSerializer(
            query_set.filter(deleted_at__isnull=True), many=True) \
            .data

        deleted_customers = CustomerSynchroSerializer(
            query_set.filter(deleted_at__isnull=False), many=True) \
            .data

        self.response_data['data'] = {
            'active': active_customers,
            'deleted': deleted_customers
        }

        return Response(self.response_data)

    def post(self, request):
        self.make_response()

        data_request = request.data.get('data', {})

        # Delete objects
        deleted_objects = data_request.get('deleted', [])
        for obj in deleted_objects:
            result = {'result': None}
            try:
                self.check_process_permission('crm.delete_customer')
                customer = Customer.objects.all() \
                    .filter(company=request.user.company) \
                    .get(id=obj.get('id'))
                customer.delete(soft_delete=True)
                result['result'] = True
                result['event'] = 'delete'
                result['obj'] = {
                    'id': customer.id,
                    'deleted_at': time_to_stamp(customer.deleted_at)}
            except Customer.DoesNotExist:
                self.hander_does_not_exist(result, 'Customer')
            except Exception as e:
                self.handler_exception(result, e)

            finally:
                self.response_data['data']['deleted'].append(result)

        # Create or update objects
        active_objects = data_request.get('active', [])
        for obj in active_objects:
            _id = obj.get('_id', None)
            obj_id = obj.get('id', None)
            result = {'result': None, '_id': _id, 'id': obj_id}
            try:
                if obj_id:
                    self.check_process_permission('crm.change_customer')
                    instance = Customer.active_objects.all() \
                        .filter(company=request.user.company) \
                        .get(id=obj_id)
                else:
                    self.check_process_permission('crm.add_customer')
                    instance = Customer(company=request.user.company,
                                        user=request.user)

                serializer = CustomerSynchroSerializer(data=obj,
                                                       instance=instance)
                serializer.is_valid(raise_exception=True)

                for (key, value) in serializer.validated_data.items():
                    setattr(serializer.instance, key, value)

                serializer.instance.full_clean()
                serializer.save()
                result['result'] = True
                result['event'] = 'save'
                result['obj'] = {
                    'id': instance.id,
                    'updated_at': time_to_stamp(instance.updated_at)}
            except ValidationError as e:
                self.handler_validation_error(result, e)
            except Exception as e:
                self.handler_exception(result, e)
            finally:
                self.response_data['data']['active'].append(result)

        return Response(self.response_data)
