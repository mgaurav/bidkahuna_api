import sys
from django.core.exceptions import ValidationError
from rest_framework import serializers
from rest_framework.response import Response

from apiapp.utils.time_stamp_fields import TimestampField, stamp_to_time, time_to_stamp
from apiapp.views.mobile.base_view import BaseMobileView
from crm.models import CustomerNote, User, Customer


class CustomerNoteSynchroSerializer(serializers.ModelSerializer):
    created_at = TimestampField(read_only=True)
    updated_at = TimestampField(read_only=True)

    class Meta:
        model = CustomerNote
        fields = ['id', 'text', 'customer_id', 'created_at', 'updated_at', 'author_id']

    def to_representation(self, instance):
        if instance.deleted_at:
            return dict(id=instance.id, deleted_at=time_to_stamp(instance.deleted_at))
        else:
            return super(CustomerNoteSynchroSerializer, self).to_representation(instance)


class CustomerNoteUserSynchroSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name']


class SynchronizeCustomersNotes(BaseMobileView):
    queryset = CustomerNote.objects.all()

    def get_queryset(self):
        query_set = self.queryset.filter(customer__company=self.request.user.company)

        filter_t = self.request.query_params.get('t')
        if filter_t:
            time = stamp_to_time(filter_t)
            query_set = query_set.get_update_summary(timestamp=time)

        return query_set

    # noinspection PyUnusedLocal
    def get(self, request):
        self.make_response()
        query_set = self.get_queryset()

        active_notes = CustomerNoteSynchroSerializer(
            query_set.filter(deleted_at__isnull=True), many=True) \
            .data

        deleted_notes = CustomerNoteSynchroSerializer(
            query_set.filter(deleted_at__isnull=False), many=True) \
            .data

        notes_authors_keys = []
        for note in active_notes:
            notes_authors_keys.append(note.get('author_id'))

        related_authors = []
        if len(notes_authors_keys) > 0:
            authors = User.objects.all().filter(pk__in=notes_authors_keys)
            related_authors = CustomerNoteUserSynchroSerializer(authors, many=True).data

        self.response_data['data']['active'] = active_notes
        self.response_data['data']['deleted'] = deleted_notes
        self.response_data['related_authors'] = {'included': related_authors}

        return Response(self.response_data)

    def post(self, request):
        self.make_response()

        data_request = request.data.get('data', {})

        # Delete objects
        deleted_objects = data_request.get('deleted', [])
        for obj in deleted_objects:
            result = {'result': None}
            try:
                self.check_process_permission('crm.delete_customernote')
                note = CustomerNote.objects.all().get(id=obj.get('id'), author=request.user)
                note.delete(soft_delete=True)
                result['result'] = True
                result['event'] = 'delete'
                result['obj'] = {
                    'id': note.id,
                    'deleted_at': time_to_stamp(note.deleted_at)}
            except CustomerNote.DoesNotExist:
                self.hander_does_not_exist(result, 'CustomerNote')
            except Exception as e:
                self.handler_exception(result, e)
            finally:
                self.response_data['data']['deleted'].append(result)

        # Create or update objects
        active_objects = data_request.get('active', [])
        for obj in active_objects:
            _id = obj.get('_id', None)
            obj_id = obj.get('id', None)
            result = {'result': None, '_id': _id}
            try:
                if obj_id:
                    self.check_process_permission('crm.change_customernote')
                    instance = CustomerNote.active_objects.all() \
                        .filter(author=request.user) \
                        .get(id=obj_id)
                else:
                    self.check_process_permission('crm.add_customernote')
                    customer = self.request.user.company.related_customers.get(id=obj.get('customer_id'))
                    instance = CustomerNote(customer=customer,
                                            author=request.user)

                serializer = CustomerNoteSynchroSerializer(data=obj,
                                                           instance=instance)
                serializer.is_valid(raise_exception=True)

                for (key, value) in serializer.validated_data.items():
                    setattr(serializer.instance, key, value)

                serializer.instance.full_clean()
                serializer.save()
                result['result'] = True
                result['event'] = 'save'
                result['obj'] = {
                    'id': instance.id,
                    'updated_at': time_to_stamp(instance.updated_at)}
            except Customer.DoesNotExist:
                self.hander_does_not_exist(result, 'Customer')
            except CustomerNote.DoesNotExist:
                self.hander_does_not_exist(result, 'CustomerNote')
            except ValidationError as e:
                self.handler_validation_error(result, e)
            except Exception as e:
                self.handler_exception(result, e)
            finally:
                self.response_data['data']['active'].append(result)

        return Response(self.response_data)
