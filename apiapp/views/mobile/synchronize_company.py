from rest_framework import serializers
from rest_framework.response import Response

from apiapp.utils.time_stamp_fields import TimestampField
from apiapp.views.mobile.base_view import BaseMobileView
from crm.models import Company


class CompanySerializer(serializers.ModelSerializer):
    updated_at = TimestampField(read_only=True)

    class Meta:
        model = Company
        fields = ['id', 'name', 'address', 'address_city', 'address_state',
                  'address_zip', 'logo', 'updated_at', 'terms', 'agreements',
                  'is_active', 'has_open_discount', 'website', 'phone', 'email_id', 'display_signature_boxes','monthly_payment_calc','months','interest']


class SynchronizeCompany(BaseMobileView):
    def get(self, request):
        self.make_response()
        self.response_data['data'] = CompanySerializer(request.user.company, many=False).data
        return Response(self.response_data)
