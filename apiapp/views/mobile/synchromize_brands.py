from rest_framework import serializers
from rest_framework.response import Response

from apiapp.utils.time_stamp_fields import TimestampField, \
    stamp_to_time, time_to_stamp

from apiapp.views.mobile.base_view import BaseMobileView
from crm.models import Brand


class BrandSynchroSerializer(serializers.ModelSerializer):
    created_at = TimestampField()
    updated_at = TimestampField()

    class Meta:
        model = Brand
        fields = ['id', 'name', 'created_at', 'updated_at']

    def to_representation(self, instance):
        if instance.deleted_at:
            return {
                'id': instance.id,
                'deleted_at': time_to_stamp(instance.deleted_at)}
        else:
            return super(BrandSynchroSerializer, self) \
                .to_representation(instance)


class SynchronizeBrands(BaseMobileView):
    queryset = Brand.objects.all()

    def get_queryset(self):
        query_set = self.queryset.filter(company=self.request.user.company)

        filter_t = self.request.query_params.get('t')
        if filter_t:
            time = stamp_to_time(filter_t)
            query_set = query_set.get_update_summary(timestamp=time)

        return query_set

    # noinspection PyUnusedLocal
    def get(self, request):
        self.make_response()
        query_set = self.get_queryset()

        self.response_data['data']['active'] = BrandSynchroSerializer(
            query_set.filter(deleted_at__isnull=True), many=True) \
            .data

        self.response_data['data']['deleted'] = BrandSynchroSerializer(
            query_set.filter(deleted_at__isnull=False), many=True) \
            .data

        return Response(self.response_data)
