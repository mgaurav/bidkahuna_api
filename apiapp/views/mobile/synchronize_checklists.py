from rest_framework import serializers
from rest_framework.response import Response

from apiapp.utils.time_stamp_fields import TimestampField, \
    time_to_stamp

from apiapp.views.mobile.base_view import BaseMobileView
from crm.models import Checklist


class ChecklistSynchroSerializer(serializers.ModelSerializer):
    created_at = TimestampField()
    updated_at = TimestampField()

    class Meta:
        model = Checklist
        fields = ['id', 'name', 'is_active', 'published', 'company', 'sort_order', 'created_at', 'updated_at']

    def to_representation(self, instance):
        if instance.deleted_at:
            return {
                'id': instance.id,
                'deleted_at': time_to_stamp(instance.deleted_at)}
        else:
            return super(ChecklistSynchroSerializer, self) \
                .to_representation(instance)


class SynchronizeChecklists(BaseMobileView):
    queryset = Checklist.objects.all()

    def get_queryset(self):
        query_set = self.queryset.filter(company=self.request.user.company, published=True)
        return query_set

    # noinspection PyUnusedLocal
    def get(self, request):
        self.make_response()
        query_set = self.get_queryset()

        self.response_data['data']['active'] = ChecklistSynchroSerializer(
            query_set.filter(deleted_at__isnull=True), many=True) \
            .data

        self.response_data['data']['deleted'] = ChecklistSynchroSerializer(
            query_set.filter(deleted_at__isnull=False), many=True) \
            .data

        return Response(self.response_data)
