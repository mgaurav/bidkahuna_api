from django.db.models import Q
from rest_framework import permissions
from rest_framework import viewsets
from rest_framework.response import Response

from apiapp.utils.time_stamp_fields import stamp_to_time, time_to_stamp
from crm.models import Customer, CustomerNote, Brand, Category, ProductTag, Order


class SynchronizeView(viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated,)

    def synchronize_data(self, request):
        timestamp = request.GET.get('t', 0)
        response = {}
        if timestamp:
            time = stamp_to_time(timestamp)
            response["timestamp"] = timestamp
            response["date"] = time
            # Check Jobs
            orders = Order.objects.all() \
                .filter(company=request.user.company) \
                .filter(Q(created_at__gt=time) | Q(updated_at__gt=time) | Q(deleted_at__gt=time))
            response["orders"] = {"amount": orders.count()}
            # Check Customers Data
            customers = Customer.objects.all() \
                .filter(company=request.user.company) \
                .filter(Q(created_at__gt=time) | Q(updated_at__gt=time) | Q(deleted_at__gt=time))
            response["customers"] = {"amount": customers.count()}
            # Check Notes Data
            notes = CustomerNote.objects.all() \
                .filter(customer__in=customers) \
                .filter(Q(created_at__gt=time) | Q(updated_at__gt=time) | Q(deleted_at__gt=time))
            response["notes"] = {"amount": notes.count()}
            # Check Brands Data
            brands = Brand.objects.all() \
                .filter(company=request.user.company) \
                .get_update_summary(timestamp=time)
            response["brands"] = {"amount": brands.count()}
            # Check Categories Data
            categories = Category.objects.all() \
                .filter(company=request.user.company) \
                .filter(Q(created_at__gt=time) | Q(updated_at__gt=time) | Q(deleted_at__gt=time))
            response["categories"] = {"amount": categories.count()}
            # Check company data
            company_update = time_to_stamp(request.user.company.updated_at) > int(timestamp)
            response["company_update"] = company_update
            # Tags
            tags = ProductTag.objects.all() \
                .filter(company=request.user.company) \
                .get_update_summary(timestamp=time)
            response["tags"] = {"amount": tags.count()}
        return Response(response)
