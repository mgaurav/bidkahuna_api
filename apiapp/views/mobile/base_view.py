import sys
from rest_framework import permissions
from rest_framework.views import APIView

from apiapp.permissions.billing_access_permission \
    import BillingAccessPermission

from apiapp.utils.time_stamp_fields import stamp_to_time


class BaseMobileView(APIView):
    permission_classes = (permissions.IsAuthenticated, BillingAccessPermission,)

    def __init__(self, **kwargs):
        super(BaseMobileView, self).__init__(**kwargs)
        self.response_data = {}

    @staticmethod
    def hander_does_not_exist(result, model):
        result['result'] = False
        result['event'] = 'error'
        result['message'] = 'Object ' + model + ' not found'
        return result

    @staticmethod
    def handler_rest_validation_error(result, e):
        result['result'] = False
        result['event'] = 'validation'
        result['validation_errors'] = e.detail
        result['message'] = 'Object is not valid'
        return result

    @staticmethod
    def handler_validation_error(result, e):
        result['result'] = False
        result['event'] = 'validation'
        result['validation_errors'] = e.message_dict
        result['message'] = 'Object is not valid'
        return result

    @staticmethod
    def handler_exception(result, e):
        exc_type, exc_obj, tb = sys.exc_info()
        result['result'] = False
        result['event'] = 'exception'
        result['message'] = 'Error Code ' + str(tb.tb_lineno) + ': ' + str(e)
        return result

    def check_process_permission(self, code_name):
        if not self.request.user.has_perm(code_name):
            raise Exception('You have no ' + code_name + ' permission.')

    def make_response(self):
        self.response_data = {
            'filter': {},
            'data': {
                'active': [],
                'deleted': []

            }
        }

        query = self.request.query_params
        filter_t = query.get('t')
        if filter_t:
            time = stamp_to_time(filter_t)
            self.response_data['filter']['timestamp'] = int(filter_t)
            self.response_data['filter']['data'] = time
