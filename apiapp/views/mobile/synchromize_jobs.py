from rest_framework.exceptions import ValidationError as RestValidationError

from django.db import transaction
from rest_framework import serializers
from rest_framework.response import Response
from django.core.exceptions import ValidationError

from apiapp.utils.time_stamp_fields import TimestampField, stamp_to_time, time_to_stamp
from apiapp.views.mobile.base_view import BaseMobileView
from crm.models import Order, OrderItem, Product

from crm.models import Invoice
from crm.models.order_action import OrderAction
import crm.measurements


class OrderItemSynchroSerializer(serializers.ModelSerializer):
    created_at = TimestampField(read_only=True)
    updated_at = TimestampField(read_only=True)

    def to_representation(self, instance):
        if instance.deleted_at:
            return {'id': instance.id,
                    'deleted_at': TimestampField(read_only=True).to_representation(instance.deleted_at)}
        else:
            return super(OrderItemSynchroSerializer, self).to_representation(instance)

    class Meta:
        model = OrderItem
        fields = ['id', 'product', 'item_details', 'price', 'created_at', 'updated_at', 'note', 'dimension_notes']


class OrderInvoiceSynchroSerializer(serializers.ModelSerializer):
    created_at = TimestampField(read_only=True)
    updated_at = TimestampField(read_only=True)
    related_discounts = serializers.ListField(source='get_discounts', read_only=True)

    class Meta:
        model = Invoice
        fields = ['id', 'uid', 'created_at', 'is_agreement', 'type',
                  'price_itemize', 'hash', 'updated_at', 'signature_customer',
                  'signature_user', 'invoice_notes', 'related_discounts']
        extra_kwargs = {'hash': {'read_only': True}, 'uid': {'read_only': True}}


class OrderActionSynchroSerializer(serializers.ModelSerializer):
    created_at = TimestampField(read_only=True)
    updated_at = TimestampField(read_only=True)
    date = TimestampField()

    class Meta:
        model = OrderAction
        fields = ['id', 'created_at', 'updated_at', 'date', 'description']


class OrderSynchroSerializer(serializers.ModelSerializer):
    created_at = TimestampField(read_only=True)
    updated_at = TimestampField(read_only=True)
    related_items = OrderItemSynchroSerializer(read_only=True, many=True)
    related_invoice = OrderInvoiceSynchroSerializer(read_only=True, many=False)
    related_actions = OrderActionSynchroSerializer(read_only=True, many=True)

    class Meta:
        model = Order
        fields = ['id', 'status', 'created_at', 'updated_at', 'show_review_link_on_pdf',
                  'related_items', 'related_invoice', 'related_actions', 'customer']


class OrderDeletedSynchroSerializer(serializers.ModelSerializer):
    deleted_at = TimestampField(read_only=True)

    class Meta:
        model = Order
        fields = ['id', 'deleted_at']


class SynchronizeJobs(BaseMobileView):
    queryset = Order.objects.all()
    queryset_products = Product.objects.all()

    def get_queryset(self):
        query_set = self.queryset.filter(user=self.request.user, company=self.request.user.company)

        filter_t = self.request.query_params.get('t')
        if filter_t:
            time = stamp_to_time(filter_t)
            query_set = query_set.get_update_summary(timestamp=time)

        return query_set

    # noinspection PyUnusedLocal
    def get(self, request):
        self.make_response()
        query_set = self.get_queryset()

        active_orders = OrderSynchroSerializer(
            query_set.filter(deleted_at__isnull=True), many=True) \
            .data

        deleted_orders = OrderDeletedSynchroSerializer(
            query_set.filter(deleted_at__isnull=False), many=True) \
            .data

        self.response_data['data']['active'] = active_orders
        self.response_data['data']['deleted'] = deleted_orders

        return Response(self.response_data)

    @transaction.atomic
    def post(self, request):
        products = self.queryset_products.filter(company=self.request.user.company)

        commit = True
        sid = transaction.savepoint()
        self.make_response()
        data_request = request.data.get('data', {})

        # Delete objects
        deleted_objects = data_request.get('deleted', [])
        for obj in deleted_objects:
            result = {'result': None}
            try:
                obj_id = obj.get('id')
                result['id'] = obj_id
                order = request.user.related_orders.all().get(id=obj_id)
                order.delete(soft_delete=True)
                result['result'] = True
                result['event'] = 'delete'
                result['obj'] = {
                    'id': order.id,
                    'deleted_at': time_to_stamp(order.deleted_at)}
            except Order.DoesNotExist:
                self.hander_does_not_exist(result, 'Order')
            except Exception as e:
                self.handler_exception(result, e)
            finally:
                self.response_data['data']['deleted'].append(result)

        # Create or update objects
        active_objects = data_request.get('active', [])
        for obj in active_objects:
            _id = obj.get('_id', None)
            obj_id = obj.get('id', None)
            result = {
                'result': None,
                '_id': _id,
                'id': obj_id,
                "related_items": [],
                "related_invoice": {},
                "related_actions": []
            }
            try:
                if obj_id:
                    instance = self.get_queryset().get(pk=obj_id)
                elif _id:
                    instance = Order(company=request.user.company, user=request.user)
                else:
                    raise Exception('Not found id or _id parameter')

                obj_serializer = OrderSynchroSerializer(data=obj, instance=instance)
                obj_serializer.is_valid(raise_exception=True)

                for (key, value) in obj_serializer.validated_data.items():
                    setattr(obj_serializer.instance, key, value)

                obj_serializer.instance.full_clean()
                obj_serializer.save()
                result['result'] = True
                result['event'] = 'save'
                result['obj'] = {
                    'customer_id': instance.customer_id,
                    'id': instance.id,
                    'updated_at': time_to_stamp(instance.updated_at)}

                # Parsing related_actions
                related_actions = obj.get('related_actions', [])
                using_items_id = []
                for action in related_actions:
                    action__id = action.get('_id', None)
                    action_obj_id = action.get('id', None)
                    if action_obj_id:
                        using_items_id.append(int(action_obj_id))
                    action_result = {'result': None,
                                     '_id': action__id,
                                     'id': action_obj_id}
                    try:
                        if action_obj_id:
                            action_item_instance = instance.related_actions.get(id=action_obj_id)
                        elif action__id:
                            action_item_instance = OrderAction(order=instance)
                        else:
                            raise Exception('Not found id or _id parameter')

                        order_action_serializer = OrderActionSynchroSerializer(
                            data=action, instance=action_item_instance)

                        order_action_serializer.is_valid(raise_exception=True)

                        for (key, value) in order_action_serializer.validated_data.items():
                            setattr(order_action_serializer.instance, key, value)

                        order_action_serializer.instance.full_clean()
                        order_action_serializer.save()
                        using_items_id.append(order_action_serializer.instance.id)

                        action_result['result'] = True
                        action_result['event'] = 'save'
                        action_result['obj'] = {
                            'id': order_action_serializer.instance.id,
                            'updated_at': time_to_stamp(action_item_instance.updated_at)}

                    except RestValidationError as e:
                        commit = False
                        self.handler_rest_validation_error(action_result, e)
                    except ValidationError as e:
                        commit = False
                        self.handler_validation_error(action_result, e)
                    except Exception as e:
                        commit = False
                        self.handler_exception(action_result, e)
                    finally:
                        result['related_actions'].append(action_result)
                # Check deleted items
                if len(using_items_id) > 0 or len(related_actions) == 0:
                    actions = instance.related_actions.exclude(id__in=using_items_id)
                    actions.delete()
                    pass

                # Parsing related_items
                related_items = obj.get('related_items')
                using_items_id = []
                for item in related_items:
                    item__id = item.get('_id', None)
                    item_obj_id = item.get('id', None)
                    #
                    if item_obj_id:
                        using_items_id.append(int(item_obj_id))
                    #
                    item_result = {'result': None,
                                   '_id': item__id,
                                   'id': item_obj_id}

                    try:
                        if item_obj_id:
                            order_item_instance = instance.related_items.get(id=item_obj_id)
                        elif item__id:
                            order_item_instance = OrderItem(order=instance,
                                                            product=products.get(id=item.get('product')))
                        else:
                            raise Exception('Not found id or _id parameter')

                        order_item_serializer = OrderItemSynchroSerializer(
                            data=item, instance=order_item_instance)

                        # Processing Order
                        try:
                            product = order_item_instance.product
                            order_item = order_item_instance
                            class_name = product.measurement.class_name
                            measurement_class = getattr(crm.measurements, class_name)
                            measurement = measurement_class(product, order_item)
                            order_item_serializer.initial_data['price'] = measurement.get_price()
                        except:
                            pass
                            # @todo Check it
                            # raise Exception('Error calculating order.')

                        order_item_serializer.is_valid(raise_exception=True)

                        for (key, value) in order_item_serializer.validated_data.items():
                            setattr(order_item_serializer.instance, key, value)

                        order_item_serializer.instance.set_data(item.get('item_details', {}))

                        order_item_serializer.instance.full_clean()
                        order_item_serializer.save()
                        using_items_id.append(order_item_serializer.instance.id)

                        item_result['result'] = True
                        item_result['event'] = 'save'
                        item_result['obj'] = {
                            'id': order_item_instance.id,
                            'price': order_item_instance.price,
                            # TODO: item.get('item_details') change for order_item_instance.get_data()
                            'item_details': item.get('item_details'),
                            'product_id': order_item_instance.product_id,
                            'updated_at': time_to_stamp(order_item_instance.updated_at)}
                    except RestValidationError as e:
                        commit = False
                        self.handler_rest_validation_error(item_result, e)
                    except ValidationError as e:
                        commit = False
                        self.handler_validation_error(item_result, e)
                    except Exception as e:
                        commit = False
                        self.handler_exception(item_result, e)
                    finally:
                        result['related_items'].append(item_result)
                # Check deleted items
                if len(using_items_id) > 0 or len(related_items) == 0:
                    items = instance.related_items.exclude(id__in=using_items_id)
                    items.delete()
                    pass

                # Parsing invoice
                related_invoice = obj.get('related_invoice')
                if related_invoice:
                    try:
                        invoice_result = {}
                        if hasattr(instance, 'related_invoice'):
                            invoice_instance = instance.related_invoice
                        else:
                            invoice_instance = Invoice(order=instance,
                                                       company=request.user.company)

                        invoice_serializer = OrderInvoiceSynchroSerializer(data=related_invoice,
                                                                           instance=invoice_instance)

                        invoice_serializer.is_valid(raise_exception=True)

                        for (key, value) in invoice_serializer.validated_data.items():
                            setattr(invoice_serializer.instance, key, value)

                        discounts = invoice_serializer.initial_data.get('related_discounts', [])
                        invoice_serializer.instance.set_discounts(discounts)

                        invoice_serializer.instance.full_clean()
                        invoice_serializer.save()
                        invoice_result['obj'] = {
                            "uid": invoice_serializer.instance.uid,
                            "hash": invoice_serializer.instance.hash,
                            "updated_at": time_to_stamp(invoice_serializer.instance.updated_at)
                        }
                    except RestValidationError as e:
                        commit = False
                        self.handler_rest_validation_error(invoice_result, e)
                    except ValidationError as e:
                        commit = False
                        self.handler_validation_error(invoice_result, e)
                    except Exception as e:
                        commit = False
                        self.handler_exception(invoice_result, e)
                    finally:
                        result['related_invoice'] = invoice_result

            except Order.DoesNotExist:
                commit = False
                self.hander_does_not_exist(result, 'Order')
            except ValidationError as ve:
                commit = False
                self.handler_validation_error(result, ve)
            except Exception as e:
                commit = False
                self.handler_exception(result, e)
            finally:
                result["commit"] = commit
                try:
                    if not commit:
                        transaction.savepoint_rollback(sid)
                    else:
                        transaction.savepoint_commit(sid)
                except Exception as e:
                    self.response_data['warning'] = {"check transaction"}
                self.response_data['data']['active'].append(result)

        # Update tasks
        tasks = request.data.get('tasks', [])
        self.response_data['tasks'] = []
        for task in tasks:
            task_result = {
                "_id": task.get("_id"),
                "obj": task
            }
            try:
                task_type = task.get('type')
                if task_type == 'ESTIMATE':
                    options = task.get('options')
                    order_id = options.get('order_id')
                    if not order_id:
                        new_order_id = options.get('_order_id')
                        order_id = next(row for row
                                        in self.response_data['data']['active']
                                        if new_order_id == row['_id']).get('obj', {}).get('id')
                    email = options.get('email')
                    invoice = Order.objects.all().filter(company=request.user.company).get(id=order_id) \
                        .related_invoice
                    task_result['result'] = invoice.send_to_email(email, request)
                else:
                    raise Exception('task_type is not correct')
            except Order.DoesNotExist:
                self.hander_does_not_exist(task_result, 'Order')
            except Exception as e:
                self.handler_exception(task_result, e)
            finally:
                self.response_data['tasks'].append(task_result)

        return Response(self.response_data)
