from rest_framework import serializers
from rest_framework.response import Response
from django.core.exceptions import ValidationError

from apiapp.utils.time_stamp_fields import stamp_to_time, TimestampField, time_to_stamp

from apiapp.views.mobile.base_view import BaseMobileView
from crm.models import User
from crm.models.customer_action import CustomerAction


class ActionSynchroSerializer(serializers.ModelSerializer):
    created_at = TimestampField(required=False)
    updated_at = TimestampField(required=False)
    date = TimestampField()

    class Meta:
        model = CustomerAction
        fields = ['id', 'customer_id', 'user_id', 'created_at', 'updated_at', 'date', 'description']

    def to_representation(self, instance):
        if instance.deleted_at:
            return {
                'id': instance.id,
                'deleted_at': time_to_stamp(instance.deleted_at)}
        else:
            return super(ActionSynchroSerializer, self) \
                .to_representation(instance)


class CustomerActionUserSynchroSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name']


class SynchronizeActions(BaseMobileView):
    queryset = CustomerAction.objects.all()

    def get_queryset(self):
        queryset = self.queryset.filter(customer__company=self.request.user.company)

        if not self.request.user.is_contractor():
            queryset = queryset.filter(user=self.request.user)

        filter_t = self.request.query_params.get('t')
        if filter_t:
            time = stamp_to_time(filter_t)
            queryset = queryset.get_update_summary(timestamp=time)

        return queryset

    # noinspection PyUnusedLocal
    def get(self, request):
        self.make_response()
        query_set = self.get_queryset()

        active_actions = ActionSynchroSerializer(
            query_set.filter(deleted_at__isnull=True), many=True) \
            .data

        actions_user_keys = []
        for action in active_actions:
            actions_user_keys.append(action.get('user_id'))

        related_users = []
        if len(actions_user_keys) > 0:
            users = User.objects.all().filter(pk__in=actions_user_keys)
            related_users = CustomerActionUserSynchroSerializer(users, many=True).data

        self.response_data['data']['active'] = active_actions
        self.response_data['included'] = {'related_users': related_users}
        self.response_data['data']['deleted'] = ActionSynchroSerializer(
            query_set.filter(deleted_at__isnull=False), many=True) \
            .data

        return Response(self.response_data)

    def post(self, request):
        self.make_response()
        data_request = request.data.get('data', {})

        # Delete objects
        deleted_objects = data_request.get('deleted', [])
        for obj in deleted_objects:
            result = {'result': None}
            try:
                action = self.get_queryset().get(id=obj.get('id'))
                action.delete(soft_delete=True)
                result['result'] = True
                result['event'] = 'delete'
                result['obj'] = {
                    'id': action.id,
                    'deleted_at': time_to_stamp(action.deleted_at)}
            except CustomerAction.DoesNotExist:
                self.hander_does_not_exist(result, 'CustomerAction')
            except Exception as e:
                self.handler_exception(result, e)
            finally:
                self.response_data['data']['deleted'].append(result)

        # Create or update objects
        active_objects = data_request.get('active', [])
        for obj in active_objects:
            _id = obj.get('_id', None)
            obj_id = obj.get('id', None)
            result = {'result': None, '_id': _id}
            try:
                if obj_id:
                    instance = self.get_queryset().get(id=obj_id)
                else:
                    customer = self.request.user.company.related_customers.get(id=obj.get('customer_id'))
                    instance = CustomerAction(customer=customer,
                                              user=request.user)

                serializer = ActionSynchroSerializer(data=obj,
                                                     instance=instance)
                serializer.is_valid(raise_exception=True)

                for (key, value) in serializer.validated_data.items():
                    setattr(serializer.instance, key, value)

                serializer.instance.full_clean()
                serializer.save()
                result['result'] = True
                result['event'] = 'save'
                result['obj'] = {
                    'id': instance.id,
                    'updated_at': time_to_stamp(instance.updated_at)}

            except CustomerAction.DoesNotExist:
                self.hander_does_not_exist(result, 'CustomerAction')
            except ValidationError as e:
                self.handler_validation_error(result, e)
            except Exception as e:
                self.handler_exception(result, e)
            finally:
                self.response_data['data']['active'].append(result)

        return Response(self.response_data)
