from rest_framework import status, permissions
from rest_framework import viewsets
from apiapp.paginations.page_number_pagination import PageNumberPagination
from rest_framework.response import Response
from apiapp.serializers.checklist import ChecklistSerialize
from apiapp.serializers.requests.save_checklist_request import SaveChecklistRequest
from crm.models import Checklist, ChecklistQuestions, ObjectDoesNotExist, ValidationError

class ChecklistViewSet(viewsets.ModelViewSet):
    queryset = Checklist.objects.all()
    serializer_class = ChecklistSerialize
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = PageNumberPagination

    def get_queryset(self):
        queryset = self.queryset.filter(company=self.request.user.company, deleted_at__isnull=True).order_by('sort_order')
        published = self.request.GET.get('published')
        if published:
            queryset = queryset.filter(published=True)
        return queryset

    def get_serializer_class(self):
        if self.action == 'create':
            return SaveChecklistRequest
        if self.action == 'update':
            return SaveChecklistRequest
        return self.serializer_class

    def perform_create(self, serializer):
        try:
            serializer.save(company=self.request.user.company)
        except ValidationError as e:
            raise ValidationError(e)

    def perform_update(self, serializer):
        try:
            serializer.save()
        except ValidationError as e:
            raise ValidationError(e)

    def retrieve(self, request, *args, **kwargs):
        instance_object = self.get_object()
        serializer_object = self.get_serializer(instance_object)
        result = {
            'data': serializer_object.data
        }
        return Response(result)

    def destroy(self, request, *args, **kwargs):
        """
        Remove Worksheet by Id
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        checklist_id = kwargs.get('checklist_id')
        try:
            checklist_queryset = self.get_queryset()
            checklist = checklist_queryset.get(pk=checklist_id)
            checklist.delete()
            checklist_questions = ChecklistQuestions.objects.all().filter(checklist_id=checklist_id)
            checklist_questions.delete()
            return Response({}, status=status.HTTP_202_ACCEPTED)
        except ObjectDoesNotExist:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

    def sort_checklist(self, request, *args, **kwargs):
        checklists = self.queryset.filter(company=self.request.user.company, deleted_at__isnull=True)
        for checklist in checklists:
            for row in request.data:
                if row.get('id') == checklist.id:
                    checklist.sort_order = row.get('order')
                    checklist.save()

        return Response('')
