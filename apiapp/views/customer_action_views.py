from rest_framework import permissions, status, viewsets
from rest_framework.response import Response

from apiapp.paginations.page_number_pagination import PageNumberPagination
from apiapp.permissions.billing_access_permission import BillingAccessPermission
from apiapp.serializers.customer_action import ActionSerializer
from apiapp.serializers.requests.save_customer_action_request import SaveCustomerActionRequest
from crm.models.customer_action import CustomerAction


class ActionViewSet(viewsets.ModelViewSet):
    queryset = CustomerAction.active_objects.active()
    permission_classes = (permissions.IsAuthenticated, BillingAccessPermission,)
    serializer_class = ActionSerializer
    pagination_class = PageNumberPagination

    def get_queryset(self):
        queryset = self.queryset.filter(customer__company=self.request.user.company)
        if not self.request.user.is_contractor():
            queryset = queryset.filter(user=self.request.user)
        return queryset

    def delete_note(self, request, customer_id=None, action_id=None, *args, **kwargs):
        customer = request.user.company.related_customers.get(pk=customer_id)
        action = self.get_queryset().filter(customer=customer).get(id=action_id)
        action.delete(soft_delete=True)
        return Response('', status=status.HTTP_202_ACCEPTED)

    def save(self, request, customer_id=None, action_id=None, *args, **kwargs):
        customer = request.user.company.related_customers.get(pk=customer_id)
        if action_id:
            instance = self.get_queryset().get(id=action_id)
        else:
            instance = CustomerAction(user=self.request.user)
        action_serialize = SaveCustomerActionRequest(instance=instance, data=self.request.data)
        action_serialize.is_valid(raise_exception=True)
        action_serialize.save(
            customer=customer)
        return Response('')
