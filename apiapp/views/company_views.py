from django.core.exceptions import ValidationError
from django.core.files import File
from rest_framework import permissions
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import serializers

from apiapp.serializers.company import CompanySerializer
from apiapp.serializers.requests.save_company_request import SaveCompanyRequest
from crm.models import Company


class CompanyViewSet(viewsets.ModelViewSet):
    """
    Last revision: 07 dec 16
    """
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    permission_classes = (permissions.IsAuthenticated, permissions.DjangoModelPermissions,)
    pagination_class = None

    def get_object(self):
        company = self.request.user.company
        return company

    def get_serializer_class(self):
        if self.action == 'update':
            return SaveCompanyRequest
        return self.serializer_class

    def perform_update(self, serializer):
        # Update logo
        new_logo = serializer.initial_data.get('new_logo')
        if new_logo:
            serializer.instance._logo = File(open('media/tmp/' + new_logo, "rb"), name=new_logo)
            serializer.is_valid(raise_exception=True)
            try:
                serializer.instance.full_clean()
                serializer.save()
            except ValidationError as e:
                raise serializers.ValidationError(e.message_dict)
        super(CompanyViewSet, self).perform_update(serializer)

    def get_terms(self, request):
        company = self.get_object()
        response = {"data": company.terms}
        return Response(response, status=status.HTTP_200_OK)

    def update_terms(self, request):
        company = self.get_object()
        company.terms = request.data.get('data')
        company.save()
        return Response('', status=status.HTTP_200_OK)

    def get_agreements(self, request):
        company = self.get_object()
        response = {"data": company.agreements}
        return Response(response, status=status.HTTP_200_OK)

    def update_agreements(self, request):
        company = self.get_object()
        company.agreements = request.data.get('data')
        company.save()
        return Response('', status=status.HTTP_200_OK)

    def get_settings(self, request):
        return Response({'display_signature_boxes': request.user.company.display_signature_boxes,'monthly_payment_calc':request.user.company.monthly_payment_calc,'months':request.user.company.months,'interest':request.user.company.interest,'enable_invoice_numbering':request.user.company.enable_invoice_numbering,'last_invoice_number':request.user.company.last_invoice_number,'enable_tax':request.user.company.enable_tax,'tax_rate':request.user.company.tax_rate},
                        status=status.HTTP_200_OK)

    def update_settings(self, request):
        company = self.get_object()
        company.display_signature_boxes = request.data.get('display_signature_boxes')
        company.monthly_payment_calc = request.data.get('monthly_payment_calc')
        company.months = request.data.get('months')
        company.interest = request.data.get('interest')
        company.enable_invoice_numbering = request.data.get('enable_invoice_numbering')
        company.last_invoice_number = request.data.get('last_invoice_number')
        company.enable_tax = request.data.get('enable_tax')
        company.tax_rate = request.data.get('tax_rate')
        company.save()
        return Response('', status=status.HTTP_200_OK)
