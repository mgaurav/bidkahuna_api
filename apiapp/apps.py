from django.apps import AppConfig


class ApiappConfig(AppConfig):
    name = 'apiapp'

    def ready(self):
        print('Init API app')
