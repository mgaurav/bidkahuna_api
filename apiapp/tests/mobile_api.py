from rest_framework.test import APITestCase
from rest_framework import status


class MobileApiTest(APITestCase):
    def test_api_permissions(self):
        response = self.client.get('/api/mobile/synchronize/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
