from rest_framework.test import APITestCase
from rest_framework import status


class AdminApiTest(APITestCase):
    def test_api_list_contractor(self):
        response = self.client.get('/api/admin/users/contractors/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_put_contractor(self):
        response = self.client.put('/api/admin/users/contractor/create/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_get_contractor(self):
        response = self.client.get('/api/admin/users/contractor/0/get/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_update_contractor(self):
        response = self.client.post('/api/admin/users/contractor/0/update/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_destroy_contractor(self):
        response = self.client.delete('/api/admin/users/contractor/0/destroy/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_get_billing_data(self):
        response = self.client.get('/api/admin/billing/0/get/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_put_billing_data(self):
        response = self.client.put('/api/admin/billing/0/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
