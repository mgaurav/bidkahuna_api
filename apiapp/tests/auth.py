from rest_framework.test import APITestCase
from rest_framework import status


class AuthTest(APITestCase):
    def test_login_with_empty_form(self):
        """
        Send auth form with empty data.
        :return:
        """
        response = self.client.post('/api-token-auth/')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data,
            {'password': ['This field is required.'], 'username': ['This field is required.']}
        )


class LoginAsSuperuser(APITestCase):
    def test_auth_as_superuser(self):
        data = {
            "username": "superadmin",
            "password": "superadmin"
        }
        response = self.client.post('/api-token-auth/', data, format='json')

