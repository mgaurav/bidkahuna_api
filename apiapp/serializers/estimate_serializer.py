from rest_framework import serializers

from crm.models.estimate_form import EstimateForm


class EstimateFormSerializer(serializers.ModelSerializer):
    class Meta:
        model = EstimateForm
        fields = ['id', 'form_data','created_at','updated_at']
