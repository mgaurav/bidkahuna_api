from django.contrib.auth.models import Permission
from rest_framework import serializers
from apiapp.serializers.content_type import ContentTypeSerializer


class PermissionSerializer(serializers.ModelSerializer):
    content_type = ContentTypeSerializer()

    class Meta:
        model = Permission
        fields = ['id', 'name', 'codename', 'content_type']
