from django.core.exceptions import ValidationError
from rest_framework import fields
from rest_framework import serializers

from crm.models import Product


class SaveProductRequest(serializers.ModelSerializer):
    measurement_id = fields.IntegerField(required=False)

    class Meta:
        model = Product
        fields = ['id', 'brand', 'name', 'is_publish', 'taxable', 'enable_discount', 'show_dimension_note',
                  'measurement_id', 'category', 'subcategory',
                  'details', 'warranty']

    def validate(self, attrs):
        instance = Product(**attrs)
        try:
            instance.clean()
        except ValidationError as e:
            raise serializers.ValidationError(e.message_dict)
        return attrs
