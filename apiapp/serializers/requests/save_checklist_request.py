from rest_framework import serializers

from apiapp.utils.time_stamp_fields import TimestampField
from crm.models import Checklist, ValidationError


class SaveChecklistRequest(serializers.ModelSerializer):
    created_at = TimestampField(read_only=True)
    updated_at = TimestampField(read_only=True)
    deleted_at = TimestampField(read_only=True)

    class Meta:
        model = Checklist
        fields = [
            'id',
            'name',
            'is_active',
            'published',
            'sort_order',
            'created_at',
            'updated_at',
            'deleted_at'
        ]

    def validate(self, attrs):
        instance = Checklist(**attrs)
        try:
            instance.clean()
        except ValidationError as e:
            raise serializers.ValidationError(e.message_dict)
        return attrs
