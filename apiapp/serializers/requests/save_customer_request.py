from rest_framework import serializers

from apiapp.utils.time_stamp_fields import TimestampField
from apiapp.views.customer_lead_source_view import LeadSourceSerializer
from crm.models import Customer


class SaveCustomerRequest(serializers.ModelSerializer):
    created_at = TimestampField(read_only=True)
    updated_at = TimestampField(read_only=True)
    deleted_at = TimestampField(read_only=True)
    lead_source = LeadSourceSerializer(read_only=True)

    class Meta:
        model = Customer
        fields = [
            'id',
            'first_name',
            'last_name',
            'spouse_name',
            'spouse_phone',
            'email',
            'phone',
            'address',
            'city',
            'state',
            'zip',
            'created_at',
            'updated_at',
            'deleted_at',
            'prospect',
            'lead_source',
            'lead_owner'
        ]
        extra_kwargs = {'lead_owner': {'read_only': True}, 'uid': {'lead_source': True},
                        'prospect': {'read_only': True}}
