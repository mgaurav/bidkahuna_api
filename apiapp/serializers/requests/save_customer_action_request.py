from rest_framework import serializers

from crm.models.customer_action import CustomerAction


class SaveCustomerActionRequest(serializers.ModelSerializer):
    class Meta:
        model = CustomerAction
        fields = ['description', 'date']
        extra_kwargs = {'id': {'read_only': True}}
