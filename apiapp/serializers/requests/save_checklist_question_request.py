from rest_framework import serializers

from apiapp.utils.time_stamp_fields import TimestampField
from crm.models import ChecklistQuestions
from crm.models import ChecklistQuestionProduct

class SaveChecklistQuestionProductRequest(serializers.ModelSerializer):
    created_at = TimestampField(read_only=True)
    updated_at = TimestampField(read_only=True)
    deleted_at = TimestampField(read_only=True)
    class Meta:
            model = ChecklistQuestionProduct
            fields = [
                'question',
                'product_id',
                'question_type',
                'created_at',
                'updated_at',
                'deleted_at'
            ]

class SaveChecklistQuestionRequest(serializers.ModelSerializer):
    created_at = TimestampField(read_only=True)
    updated_at = TimestampField(read_only=True)
    deleted_at = TimestampField(read_only=True)
    fail_products = SaveChecklistQuestionProductRequest(many=True)

    class Meta:
        model = ChecklistQuestions
        fields = [
            'id',
            'question',
            'checklist_id',
            'is_section',
            'show_na_button',
            'position',
            'fail_products',
            'created_at',
            'updated_at',
            'deleted_at'
        ]

    def create(self, validated_data):
        products_data = validated_data.pop('fail_products')
        checklistquestion = ChecklistQuestions.objects.create(**validated_data)
        for product_data in products_data:
            ChecklistQuestionProduct.objects.create(question=checklistquestion, **product_data)
        return checklistquestion

    def update(self, instance, validated_data):
        products_data = validated_data.pop('fail_products')
        ChecklistQuestionProduct.objects.filter(question=instance).delete()
        instance.is_section = self.initial_data.get('is_section')
        instance.show_na_button = self.initial_data.get('show_na_button')
        instance.save()
        for product_data in products_data:
            ChecklistQuestionProduct.objects.create(question=instance, **product_data)
        return instance;