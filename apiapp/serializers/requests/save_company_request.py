from django.core.exceptions import ValidationError
from rest_framework import fields
from rest_framework import serializers

from crm.models import Company


class SaveCompanyRequest(serializers.ModelSerializer):
    new_logo = fields.CharField(required=False, read_only=True)
    logo = fields.CharField(read_only=True)
    id = fields.IntegerField(read_only=True)

    class Meta:
        model = Company
        fields = ['id', 'name', 'new_logo',
                  'address', 'address_city', 'address_state', 'address_zip',
                  'logo', 'phone', 'website', 'email_id']

    def validate(self, attrs):
        instance = Company(**attrs)
        try:
            instance.full_clean()
        except ValidationError as e:
            raise serializers.ValidationError(e.message_dict)
        return attrs
