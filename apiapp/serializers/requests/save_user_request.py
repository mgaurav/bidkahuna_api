from rest_framework import serializers

from crm.models import User


class SaveUserRequest(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id',
            'first_name',
            'last_name',
            'username',
            'email'
        ]
