from rest_framework import serializers

from crm.models import Brand


class SaveBrandRequest(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ['id', 'name']
        extra_kwargs = {'id': {'read_only': True}}
        validators = [
            serializers.UniqueTogetherValidator(
                queryset=model.objects.all(),
                fields=('name', 'company'),
            )
        ]
