from rest_framework import serializers

from apiapp.utils.time_stamp_fields import TimestampField
from crm.models import Brand


class BrandSerializer(serializers.ModelSerializer):
    created_at = TimestampField()
    updated_at = TimestampField()
    deleted_at = TimestampField()

    class Meta:
        model = Brand
        fields = ['id', 'name', 'created_at', 'updated_at', 'deleted_at']
