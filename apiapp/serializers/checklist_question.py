from rest_framework import serializers

from apiapp.utils.time_stamp_fields import TimestampField
from crm.models import ChecklistQuestions
from crm.models import ChecklistQuestionProduct

class ChecklistQuestionProductSerialize(serializers.ModelSerializer):
    class Meta:
        model = ChecklistQuestionProduct
        fields = ['product_id', 'question_type']

class ChecklistQuestionSerialize(serializers.ModelSerializer):
    fail_products = ChecklistQuestionProductSerialize(many=True, read_only=True)
    created_at = TimestampField(read_only=True)
    updated_at = TimestampField(read_only=True)
    deleted_at = TimestampField(read_only=True)

    class Meta:
        model = ChecklistQuestions
        fields = ['id', 'question', 'checklist_id', 'is_section', 'show_na_button', 'position', 'created_at', 'updated_at', 'deleted_at', 'fail_products']
