from rest_framework import serializers

from apiapp.serializers.billing import BillingSerializer
from crm.models import Company, BillingTransaction


class CompanyRelatedBillingSerializer(serializers.ModelSerializer):
    class Meta:
        model = BillingTransaction
        fields = ['created_at', 'comment']


class CompanyBillingSerializer(serializers.ModelSerializer):
    billing = BillingSerializer(read_only=True)
    related_billing_transactions = CompanyRelatedBillingSerializer(read_only=True, many=True)

    class Meta:
        model = Company
        fields = ['id', 'name', 'is_active', 'billing', 'related_billing_transactions']


class CompanyNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ['name']


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ['id', 'name', 'address', 'address_city', 'address_state', 'address_zip', 'logo', 'phone', 'website', 'email_id']
