from rest_framework import serializers

from apiapp.utils.time_stamp_fields import TimestampField
from crm.models import Checklist

class ChecklistSerialize(serializers.ModelSerializer):
    created_at = TimestampField(read_only=True)
    updated_at = TimestampField(read_only=True)
    deleted_at = TimestampField(read_only=True)

    class Meta:
        model = Checklist
        fields = ['id', 'name','is_active','published','company','sort_order','created_at',
                  'updated_at', 'deleted_at']
        extra_kwargs = {'is_active': {'read_only': True}}
