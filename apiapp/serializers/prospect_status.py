from rest_framework import serializers

from crm.models import ProspectStatus


class ProspectStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProspectStatus
        exclude = ('company',)
