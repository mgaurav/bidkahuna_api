from rest_framework import serializers

from crm.models import BillingCompany, BillingPlan


class BillingPlanSerializer(serializers.ModelSerializer):
    class Meta:
        model = BillingPlan
        fields = '__all__'


class BillingSerializer(serializers.ModelSerializer):
    used_plan = BillingPlanSerializer()

    class Meta:
        model = BillingCompany
        fields = ['created_at', 'last_transaction', 'next_check', 'used_plan']
