from rest_framework import serializers

from apiapp.utils.time_stamp_fields import TimestampField
from apiapp.views.customer_lead_source_view import LeadSourceSerializer
from crm.models import Customer


class ShortCustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ['full_name']


class CustomerSerialize(serializers.ModelSerializer):
    created_at = TimestampField(read_only=True)
    updated_at = TimestampField(read_only=True)
    deleted_at = TimestampField(read_only=True)
    lead_source = LeadSourceSerializer()
    zip = serializers.CharField(label='Zip', required=False)

    class Meta:
        model = Customer
        fields = ['id', 'first_name', 'last_name', 'email', 'phone',
                  'address', 'city', 'state', 'zip', 'created_at',
                  'updated_at', 'deleted_at', 'company_id', 'spouse_name',
                  'spouse_phone', 'prospect',
                  'lead_source', 'lead_owner']
