from rest_framework import serializers

from apiapp.serializers.company import CompanyNameSerializer, CompanyBillingSerializer
from apiapp.serializers.permission import PermissionSerializer
from crm.models import User


class ContractorSerializer(serializers.ModelSerializer):
    company = CompanyBillingSerializer(read_only=True)

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'username', 'email', 'show_checklist', 'show_custom_form', 'company', 'is_active','review_link']
        extra_kwargs = {'is_active': {'read_only': True}}


class UserNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name')


class ListUsersSerializer(serializers.ModelSerializer):
    """
    Short data format.
    Used for a list of Users.
    """
    company = CompanyNameSerializer(read_only=True)

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'username', 'email', 'company']


class UserDataSerializer(serializers.ModelSerializer):
    """
    Full data format for a User.
    """
    user_permissions = PermissionSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'username', 'email', 'show_checklist', 'show_custom_form', 'is_staff', 'group', 'role', 'user_permissions']
