from rest_framework import serializers

from crm.models.custom_form import CustomForm


class CustomFormSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomForm
        fields = ['id', 'job_id', 'form_data']
