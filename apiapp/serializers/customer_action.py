from rest_framework import serializers

from crm.models.customer_action import CustomerAction
from crm.models.order_action import OrderAction


class ActionOrderSerializer(serializers.ModelSerializer):
    # order = serializers.CharField(source='user.get_full_name')

    class Meta:
        model = OrderAction
        fields = ['id', 'description', 'date', 'order_id']

    def to_representation(self, instance):
        result = super(ActionOrderSerializer, self).to_representation(instance)
        result.setdefault('type', 'order')

        if hasattr(instance.order, 'related_invoice'):
            invoice = instance.order.related_invoice
            result.setdefault('invoice', {
                'id': invoice.id
            })

        return result


class ActionSerializer(serializers.ModelSerializer):
    # user = serializers.CharField(source='user.get_full_name')

    class Meta:
        model = CustomerAction
        fields = ['id', 'description', 'date']

    def to_representation(self, instance):
        result = super(ActionSerializer, self).to_representation(instance)
        result.setdefault('type', 'customer')
        return result
