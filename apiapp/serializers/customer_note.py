from rest_framework import serializers

from apiapp.serializers.user import UserNameSerializer
from apiapp.utils.time_stamp_fields import TimestampField
from crm.models import CustomerNote


class NoteSerializer(serializers.ModelSerializer):
    created_at = TimestampField()
    updated_at = TimestampField()
    author = UserNameSerializer(many=False, )

    class Meta:
        model = CustomerNote
        fields = ['id', 'text', 'created_at', 'author', 'created_at', 'updated_at']
