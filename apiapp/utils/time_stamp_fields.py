import time
from datetime import datetime

from rest_framework import serializers


def time_to_stamp(db_time):
    return int(time.mktime(db_time.timetuple())) * 1000


def stamp_to_time(timestamp):
    return datetime.fromtimestamp(int(timestamp) / 1e3)


class TimestampField(serializers.Field):
    def to_representation(self, value):
        return int(time.mktime(value.timetuple())) * 1000

    def to_internal_value(self, value):
        return stamp_to_time(value)
