from rest_framework import pagination
from rest_framework.response import Response


class PageNumberPagination(pagination.PageNumberPagination):
    def get_paginated_response(self, data):
        next_page = None
        if self.page.has_next():
            next_page = self.page.number + 1

        prev_page = None
        if self.page.has_previous():
            prev_page = self.page.number - 1

        return Response({
            'paginator': {
                'next': next_page,
                'previous': prev_page,
                'count': self.page.paginator.count,
                'page': self.page.number,
                'pages': self.page.paginator.num_pages,
            },
            'results': data
        })
