import json
from .utils import *
from .models import *
from django import template
from crm.models import User
from django.template.loader import render_to_string
from django.db.models import Sum
from django.http import JsonResponse
from django.contrib.auth.forms import AuthenticationForm
from django.shortcuts import render,redirect,get_object_or_404
from django.contrib.auth import logout as Logout, login as Login

register = template.Library()
sheet_size = 'Size'

@register.inclusion_tag('shoping-cart.html')
def globle(request):

    if request.session.get('gmail'):

        obj = UserLead.objects.filter(email=request.session('email'))
        return {'foos': obj}
    else:
        return {'foos': 0}


def registration(request):

    if request.method == 'POST':
        data = {}
        data['first_name'] = request.POST.get('first_name')
        data['last_name'] = request.POST.get('last_name')
        data['username'] = request.POST.get('email')
        data['email'] = request.POST.get('email')
        password = request.POST.get('password')

        city = request.POST.get('city')
        zip_code = request.POST.get('zip_code')

        user_obj = User.objects.create(**data)
        user_obj.set_password(password)
        user_obj.save()

        details = UserDetail.objects.create(user=user_obj,zip_code=zip_code,city=city)
        details.save()

        return redirect('login')

    return render(request, 'Shopping/registraions.html',)


def login(request):

    if request.user.is_authenticated():
        return redirect('home')

    else:
        if request.method == 'POST':

            form = AuthenticationForm(data=request.POST)

            if form.is_valid():

                user = form.get_user()
                Login(request, user)
                return redirect('home')
            else:
                return render(request, 'Shopping/login.html', {'form': form})
        else:

            form = AuthenticationForm()

        return render(request, 'Shopping/login.html', {'form':form})


def reqlogin(request):

    if request.method == 'POST':

            form = AuthenticationForm(data=request.POST)

            if form.is_valid():

                user = form.get_user()
                Login(request, user)
                return redirect('dimensions')
            else:
                return render(request, 'Shopping/login.html', {'form': form})

    form = AuthenticationForm()

    return render(request, 'Shopping/login.html', {'form':form})


def logout(request):

    Logout(request)

    return redirect('login')


def home(request):

    room_obj = Room.objects.all()

    data = {'rooms': room_obj}
    return render(request, 'Shopping/choose-room.html', data)


def frame(request):

    frame_obj = FrameColor.objects.all()
    data = {'frames': frame_obj}

    return render(request, 'Shopping/choose-frame.html', data)


def style(request):

    style_obj = WindowStyle.objects.all()

    data = {'styles': style_obj}

    return render(request, 'Shopping/choose-window.html', data)


def dimensions(request, item_id=None):

    sheet_name = 'Size'
    EXCEL_FILE = excel_file()
    if not EXCEL_FILE is None:
        sheetnames = getsheetnames(EXCEL_FILE)

        df = getsheet(sheet_name)

        data = {}
        data['heights'] = list(df.columns)[1:]
        data['widths'] = list(df['Height'])
        grid_names = 'Grids'
        glass_name = 'Glass'
        exterior = 'Exterior Color'
        # quality_name = 'Quality'

        data['grid_names'] = [i.split('Grids-', )[1] for i in sheetnames if grid_names in i]
        data['glass_name'] = [i for i in sheetnames if glass_name in i]
        data['exterior_name'] = [i.split('-',)[1] for i in sheetnames if exterior in i]

        # data['quality_name'] = [i for i in sheetnames if quality_name in i]

        if request.method == 'POST':
            if not request.POST.get('edit'):

                email = request.POST.get('email')
                name = request.POST.get('name')
                pincode = request.POST.get('pincode')
                number = request.POST.get('number')
                try:

                    if UserLead.objects.filter(email=email).exists():
                        user = UserLead.objects.get(email=email)

                    else:
                        user = UserLead.objects.create(email=email, user=name, pin_code=pincode, mobile_number=number)
                        user.save()
                    if request.session.get('email'):
                        del request.session['email']
                        request.session['email'] = email
                    else:
                        request.session['email'] = email

                    cart_items = json.loads(request.POST.get('cart_items'))

                    save_cart_itmes(cart_items, user, sheet_size)

                    items = ShoppingCart.objects.filter(leaduser__email=email,buy_status=False).count()
                    if request.session.get('items'):
                        del request.session['items']
                        request.session['items'] = items
                    else:
                        request.session['items'] = items

                    return JsonResponse({'success':True})
                except ValueError:

                    return JsonResponse({'success':False})
            else:
                detials = {}
                detials['id'] = request.POST.get('id')
                detials['real_height'] = request.POST.get('height')
                detials['real_width'] = request.POST.get('width')
                detials['glass'] = request.POST.get('glass')
                detials['grid'] = request.POST.get('grid')
                detials['exterior'] = request.POST.get('exterior')

                detials['tempered'] = request.POST.get('tempered')

                if ShoppingCart.objects.filter(id=detials['id'], buy_status=False).exists():

                    obj = ShoppingCart.objects.get(id=detials['id'], buy_status=False)
                    check = check_hight_width(request.POST.get('height'), request.POST.get('width'), sheet_size, obj)

                    if check.get('height_error') == False:
                        detials['height'] = check.get('height')
                        detials['width'] = check.get('width')
                        success = edit_item_save(sheet_size, detials, obj)
                        return JsonResponse(success)
                    else:
                        error = check
                        del check
                        return JsonResponse(error)

        if item_id and ShoppingCart.objects.filter(id=item_id,buy_status=False):
            obj = ShoppingCart.objects.get(id=item_id,buy_status=False)
            data['item'] = obj
        return render(request, 'Shopping/dimension-option.html', data)
    return render(request, 'Shopping/dimension-option.html', {'sheeterror': 'Price not available please try again later'})


def edit_item(request, item_id):

    # if request.method == "POST":
    #     print('here')
    d = ShoppingCart.objects.filter(id=item_id)
    data = get_object_or_404(ShoppingCart, pk=item_id)

    return render(request, 'Shopping/dimension-option.html',data)


# @login_required(login_url='login')
def cart(request):

    email = request.session.get('email')
    data = {}
    data['shoppings'] = ShoppingCart.objects.filter(leaduser__email=email,buy_status=False)
    data_payments = ShoppingCart.objects.filter(leaduser__email=email,buy_status=False).aggregate(Sum('total_price'))['total_price__sum']
    data['data_payments'] = data_payments
    data['dealer'] = Dealer.objects.first()
    # data['shoppings'] = ShoppingCart.objects.all()

    if request.method == 'POST':

        id = request.POST.get('deletid')
        ShoppingCart.objects.filter(id=id).delete()
        items = ShoppingCart.objects.filter(leaduser__email=email,buy_status=False).count()
        if request.session.get('items'):
            del request.session['items']
            request.session['items'] = items
        else:
            request.session['items'] = items

        # id = request.POST.get('cartid')
        # value = request.POST.get('value')
        #
        # obj = ShoppingCart.objects.get(id=id)
        # total = int(obj.single_price)*int(value)
        # obj.total_price= total
        # obj.quantity = value
        # obj.save()

    return render(request, 'Shopping/shoping-cart.html', data)


def checkdimension(request):

    styleID = request.GET.get('styleid')
    sheet_name = 'Size'

    # sheetnames = getsheetnames(EXCEL_FILE)
    # quality_sheet = getsheet(sheet_name)

    style = WindowStyle.objects.get(id=int(styleID))
    quality_sheet = getwindowstylesheet(sheet_name, str(style.price_sheet))

    height_value = request.GET.get('height')
    width_value = request.GET.get('width')
    height_list = list(quality_sheet['Height'])
    width_list = list(quality_sheet.columns)[1:]

    max_height = max(height_list)
    max_width = max(width_list)

    if int(height_value)>int(max_height):
        return JsonResponse({'height_error': True, 'error': 'Window height out of range'})
    if int(width_value)>int(max_width):
        return JsonResponse({'height_error': True, 'error': 'Window width out of range'})

    # height = min(height_list, key=lambda x: abs(int(x) - int(height_value)))
    # width = min(width_list, key=lambda x: abs(int(x) - int(width_value)))
    height = next(x[1] for x in enumerate(height_list) if x[1] >= int(height_value))
    width = next(x[1] for x in enumerate(width_list) if x[1] >= int(width_value))
    value = round(quality_sheet.loc[quality_sheet['Height'] >= int(height), int(width)].values[0].item(), 2)
    val = str(value).upper()

    if val == "NAN":

        return JsonResponse({'height_error': True, 'error': 'Window size is not available'})
    return JsonResponse({'height_error': False, 'height': height, 'width': width})


def emaillogout(request):

    if request.session['email']:

        del request.session['email']
    return redirect('home')


def emailregister(request):

    email = request.POST.get('email')
    name = request.POST.get('name')
    pincode = request.POST.get('pincode')
    number = request.POST.get('number')
    if not UserLead.objects.filter(email=email).exists():
        user = UserLead.objects.create(email=email, user=name, pin_code=pincode, mobile_number=number)
        user.save()
        return JsonResponse({'success': True})
    else:
        return JsonResponse({'success': False})


def emaillogin(request):
    email = request.POST.get('email')
    items = ShoppingCart.objects.filter(leaduser__email=email,buy_status=False).count()
    if request.session.get('items'):
        del request.session['items']
        request.session['items'] = items
    else:
        request.session['items'] = items

    if UserLead.objects.filter(email=email).exists():

        if request.session.get('email'):
            del request.session['email']
            request.session['email'] = email
        else:
            request.session['email'] = email
        return JsonResponse({'success': True})
    else:
        return JsonResponse({'success': False})


def dealer_details(request):
    if request.method == 'POST':
        address = request.POST.get('address')
        city = request.POST.get('city')
        phone = str(request.POST.get('phone'))
        time = request.POST.get('time')
        email = request.session.get('email')

        if UserLead.objects.filter(email=email).exists():
            user = UserLead.objects.get(email=email)
            user.address = address
            user.city = city
            user.mobile_number = phone
            user.best_time_to_cal = time
            user.save()

        userdetails = UserLead.objects.get(email=email)
        itemsdata = ShoppingCart.objects.filter(leaduser__email=email, buy_status=False)
        data_payments = ShoppingCart.objects.filter(leaduser__email=email, buy_status=False).aggregate(Sum('total_price'))['total_price__sum']
        message = render_to_string('Shopping/mailer.html', {'userdetails': userdetails, 'itmes': itemsdata, 'totalpayment': data_payments}, request)
        send_mailer(email, message)
        ShoppingCart.objects.filter(leaduser__email=email, buy_status=False).update(buy_status=True)
        items = ShoppingCart.objects.filter(leaduser__email=email, buy_status=False).count()
        if request.session.get('items'):
            del request.session['items']
            request.session['items'] = items
        else:
            request.session['items'] = items
    return JsonResponse({'success': True})


def details(request):

    if request.method == 'POST':
        name = request.POST.get('name')
        email = request.POST.get('email')
        address = request.POST.get('address')
        city = request.POST.get('city')
        phone = str(request.POST.get('phone'))
        time = request.POST.get('time')

        if UserLead.objects.filter(email=email).exists():
            user = UserLead.objects.get(email=email)
            user.user = name
            user.address = address
            user.city = city
            user.mobile_number = phone
            user.best_time_to_cal = time
            user.save()
        else:
            user = UserLead.objects.create(email=email, user=name,city=city,best_time_to_cal=time,
                                           address=address, mobile_number=phone)
            user.save()
        items = ShoppingCart.objects.filter(leaduser__email=email, buy_status=False).count()
        if request.session.get('items'):
            del request.session['items']
            request.session['items'] = items
        else:
            request.session['items'] = items

        if UserLead.objects.filter(email=email).exists():

            if request.session.get('email'):
                del request.session['email']
                request.session['email'] = email
            else:
                request.session['email'] = email
        userdetails = UserLead.objects.get(email=email)
        message = render_to_string('Shopping/appointment_mailer.html',
                                   {'userdetails': userdetails},request)
        send_appointment_mailer(email, message)

    return render(request, 'Shopping/landing.html')


def saveitem(request):
    try:
        email = request.session.get('email')
        user = UserLead.objects.get(email=email)
        cart_items = json.loads(request.POST.get('cart_items'))
        save_cart_itmes(cart_items, user, sheet_size)
        items = ShoppingCart.objects.filter(leaduser__email=email, buy_status=False).count()
        if request.session.get('items'):
            del request.session['items']
            request.session['items'] = items
        else:
            request.session['items'] = items
        return JsonResponse({'success': True})
    except ValueError:

        return JsonResponse({'success': False})


def privacy(request):

    return render(request, 'Shopping/privacy.html', )