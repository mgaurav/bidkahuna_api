from django.db import models
from crm.models import User
from crm.models.product_images import ProductImage

# Create your models here.


class UserDetail(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    zip_code = models.IntegerField()
    city = models.CharField(max_length=200)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)


class Dealer(models.Model):

    contractor = models.ForeignKey(User, on_delete=models.CASCADE)
    sheet_file = models.FileField(upload_to='excelsheet')
    description = models.TextField()
    address = models.TextField()
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return self.contractor.username


class UserLead(models.Model):
    user = models.CharField(max_length=50)
    pin_code = models.IntegerField(blank=True, null=True)
    email = models.EmailField(unique=True)
    mobile_number = models.CharField(max_length=15, blank=True,null=True)
    city = models.CharField(max_length=30, blank=True, null=True)
    best_time_to_cal = models.CharField(blank=True, null=True, max_length=20)
    address = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return self.user


class Room(models.Model):

    room_name = models.CharField(max_length=100)
    room_image = models.ImageField(upload_to='room')
    # hover_image = models.ImageField(upload_to='room')
    description = models.TextField(blank=True, null=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return self.room_name


class FrameColor(models.Model):

    frame_name = models.CharField(max_length=100)
    frame_image = models.ImageField(upload_to='frame')
    color_code = models.ImageField(upload_to='frame',blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    # frame_color = models.CharField(max_length=100)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return self.frame_name


class WindowStyle(models.Model):

    product_details = models.ForeignKey(ProductImage, on_delete=models.SET_NULL, blank=True, null=True)
    window_name = models.CharField(max_length=100)
    window_image = models.ImageField(upload_to='style')
    # hover_image = models.ImageField(upload_to='style')
    description = models.TextField(blank=True, null=True)
    price_sheet = models.FileField(upload_to='excelsheet/windowsheet/', blank=True, null=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return self.window_name


class WindowDimension(models.Model):

    picture_option = (('T', 'Tempered'), ('O', 'Obscured'), ('G','Grids'))

    window = models.ForeignKey(WindowStyle, on_delete=models.CASCADE)
    real_width = models.CharField(max_length=100)
    real_height = models.CharField(max_length=100)
    width = models.CharField(max_length=100)
    height = models.CharField(max_length=100)
    picture_window = models.CharField(choices=picture_option, max_length=2)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now_add=True)


dimensions = ((1,18),(2,24),(3,36),(4,42),(5,48), (6,60),(7,72),(8,84))

# products = ((1,'Master'),(2,'Size'),
#             (3,'Frame Color White'),(4,'Frame Color Tan'),(5,'Exterior Color-No'), #Color
#             (6,'Exterior Color-Bronze'),(7,'Exterior Color-Desert Clay'), #Color
#             (8,'Exterior Color-English Red'),(9,'Exterior Color-Forest Green'),(10,'Room-Living Room'),
#             (11,'Room-Dining Room'),(12,'Room-Kitchen'),(13,'Room-Family Room'),
#             (14,'Room-Laundry Room'),(15,'Room-Garage'),(16,'Room-Master Bedroom'),
#             (17,'Room-Master Bath'),(18,'Room-Bedroom 1'),(19,'Room-Bedroom 2'),(20,'Room-Bedroom 3'),
#             (21,'Room-Basement'),(22,'Room-Basement Bath'),(23,'Grids-No'),(24,'Grids-Flat'),
#             (25,'Grids-Sculpted'),(26,'Privacy Glass-No'),(27,'Privacy Glass-Standard'),
#             (28,'Privacy Glass-Rain'),(29,'Privacy Glass-Glue Chip'),(30,'Tempered'),
#             )
# types = ((1,'Master'), (2,'Room'),(3,'Picture Window'),(4,'Color'), (5,'Privacy Glass'),(6,'Grids'))
#
#
# class RateList(models.Model):
#
#     width = models.IntegerField(choices=dimensions)
#     height = models.IntegerField(choices=dimensions)
#     type = models.IntegerField(choices=types, default=1)
#     product_type = models.IntegerField(choices=products)
#     price = models.IntegerField()
#
#     def __str__(self):
#
#         return f'{self.get_product_type_display()} : {self.get_width_display()} x {self.get_height_display()} :{self.get_type_display()} : {self.price}'
#
#
#     def save(self, *args,**kwargs):
#
#         width = self.width
#         height = self.height
#         type = self.product_type
#         price = self.price
#
#
#         if RateList.objects.filter(width=width,height=height,product_type=type).exists():
#             RateList.objects.filter(width=width, height=height, product_type=type).update(price=price)
#         else:
#             super(RateList, self).save(*args, **kwargs)
#


class ShoppingCart(models.Model):

    quality_choice = (('gd', 'Good'), ('bt', 'Batter'), ('bs', 'Best'))

    leaduser = models.ForeignKey(UserLead, on_delete=models.SET_NULL, blank=True, null=True)
    room = models.ForeignKey(Room, on_delete=models.SET_NULL, blank=True, null=True)
    frame = models.ForeignKey(FrameColor, on_delete=models.SET_NULL, blank=True, null=True)
    style = models.ForeignKey(WindowStyle, on_delete=models.SET_NULL, blank=True, null=True)
    private_glass = models.BooleanField(default=False)
    grid = models.CharField(max_length=50, blank=True, null=True)
    exterior = models.CharField(max_length=50, blank=True, null=True)
    tempered = models.BooleanField(default=False)
    height = models.IntegerField()
    width = models.IntegerField()
    real_width = models.IntegerField()
    real_height = models.IntegerField()
    room_prince = models.DecimalField(max_digits=9, decimal_places=2,blank=True, null=True)
    frame_prince = models.DecimalField(max_digits=9, decimal_places=2,blank=True, null=True)
    style_prince = models.DecimalField(max_digits=9, decimal_places=2,blank=True, null=True)
    total_price = models.DecimalField(max_digits=9, decimal_places=2,blank=True, null=True)
    single_price = models.DecimalField(max_digits=9, decimal_places=2,blank=True, null=True)
    window_price = models.DecimalField(max_digits=9, decimal_places=2,blank=True, null=True)
    exterior_price = models.DecimalField(max_digits=9, decimal_places=2,blank=True, null=True)
    grid_price = models.DecimalField(max_digits=9, decimal_places=2, blank=True, null=True)
    glass_price = models.DecimalField(max_digits=9, decimal_places=2, blank=True, null=True)
    tempered_price = models.DecimalField(max_digits=9, decimal_places=2, blank=True, null=True)
    quantity = models.IntegerField(default=1)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)
    quality = models.CharField(choices=quality_choice, max_length=2, default='gd')
    buy_status = models.BooleanField(default=False)





