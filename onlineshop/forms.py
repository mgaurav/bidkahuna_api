from django import forms
from .models import UserDetail



class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model = UserDetail
        fields = '__all__'
