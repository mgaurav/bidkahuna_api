from django.contrib import admin
from django import forms
from .models import  *

class DealerForm(forms.ModelForm):  #OVERRIDING Scenario Admin Form Fields
    def __init__(self, *args, **kwargs):
            # try:
            #     instance = kwargs.pop('instance')
            #
            #     super(DealerForm, self).__init__(*args, **kwargs)
            #     self.fields['contractor'] = forms.ModelChoiceField(queryset=User.objects.filter(role='CONTRACTOR'),
            #                                                     initial=Dealer.objects.get(id=instance.id))
            #
            # except ValueError:
           super(DealerForm, self).__init__(*args, **kwargs)
           self.fields['contractor'].queryset = User.objects.filter(role='CONTRACTOR')
    class Meta:
        model = Dealer
        fields = '__all__'

class DealerOptions(admin.ModelAdmin):
    #overriding Scenario Admin Template
    form =  DealerForm
    # change_form_template = 'admin/dealer.html'
    def get_queryset(self, request):
        return super(DealerOptions, self).get_queryset(request).all()

    # return super(ScenarioOptions, self).get_queryset(request).filter(contractor__groups='CONTRACTOR')

class StyleForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):

        super(StyleForm, self).__init__(*args,**kwargs)

        self.fields['product_details'].queryset = ProductImage.objects.filter(product__category__name='Windows')
    class Meta:
        model = WindowStyle
        fields = '__all__'



class StyleOptions(admin.ModelAdmin):

    form = StyleForm

    def get_queryset(self, request):
        print(super(StyleOptions, self).get_queryset(request).all())
        return super(StyleOptions, self).get_queryset(request).all()




# Register your models here.
class CustomRateList(admin.ModelAdmin):

    change_form_template = 'admin/rateslistchange.html'



admin.site.register(UserDetail),
admin.site.register(Room),
admin.site.register(FrameColor),
admin.site.register(WindowStyle,StyleOptions),
# admin.site.register(WindowDimension),
admin.site.register(ShoppingCart),
admin.site.register(UserLead),
admin.site.register(Dealer,DealerOptions),
# admin.site.register(RateList,CustomRateList),



