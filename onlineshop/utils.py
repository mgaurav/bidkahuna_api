import pandas as pd
from django.conf import settings
from django.core.mail import EmailMessage
from .models import Room, WindowStyle, FrameColor, ShoppingCart, Dealer

dir = settings.MEDIA_ROOT

def excel_file():
    try:
        obj = Dealer.objects.first()

        return pd.ExcelFile(dir + '/' + obj.sheet_file.name)

    except:

        return None


def getsheetnames(file):

    EXCEL_FILE = excel_file()

    return EXCEL_FILE.sheet_names


def getsheet(sheetname):
    EXCEL_FILE = excel_file()
    try:
        return pd.read_excel(EXCEL_FILE, sheet_name=sheetname, index=False, index_col=None)
    except:
        return None


def getwindowstylesheet(sheetname, sheet_url):

    # WINDOW_EXCEL_FILE = pd.ExcelFile(dir + '/' + sheet_url, )
    # print(WINDOW_EXCEL_FILE)
    # sheet = pd.read_excel(WINDOW_EXCEL_FILE, sheet_name=sheetname, index=False, index_col=None)
    # print(sheet)
    # return sheet
    try:
        WINDOW_EXCEL_FILE = pd.ExcelFile(dir + '/' + sheet_url, )
        if WINDOW_EXCEL_FILE:
            try:
                return pd.read_excel(WINDOW_EXCEL_FILE, sheet_name=sheetname, index=False, index_col=None)
            except:
                return None
        else:
            return None
    except:
        return None


def get_value(file, height, width):
    try:
        value = round(file.loc[file['Height'] >= int(height), int(width)].values[0].item(), 2)
        return value
    except:

        value = round(file.iloc[0, 1].item(), 2)
        return value


def save_cart_itmes(cart_items, user, sheet_size):
    for cart in cart_items:

        data = {}
        for k, v in cart.items():
            data[k] = v

        room = Room.objects.get(id=data['roomid'])
        style = WindowStyle.objects.get(id=data['styleid'])
        exterior_pricesheet = str(style.price_sheet)
        frame = FrameColor.objects.get(id=data['framecolorid'])

        total_price = 0
        object = ShoppingCart.objects.create(quality='gd', leaduser=user, height=data['height'], width=data['width'],
                                             real_width=data['real_width'], real_height=data['real_height'])

        # quality_good = getsheet(sheet_size)
        # if not quality_good is None:
        #     quality_price = get_value(quality_good, data['width'], data['height'])
        #     total_price += quality_price
        #     object.window_price = quality_price
        room_file = getwindowstylesheet('Room-' + room.room_name, str(style.price_sheet))
        if not room_file is None:
            room_price = get_value(room_file, data['height'], data['width'])
            total_price += room_price
            object.room = room
            object.room_prince = room_price
        else:
            object.room = room
            object.room_prince = 0
        frame_file = getwindowstylesheet('Frame Color-' + frame.frame_name, str(style.price_sheet))

        if not frame_file is None:
            frame_price = get_value(frame_file, data['height'], data['width'])
            total_price += frame_price
            object.frame = frame
            object.frame_prince = frame_price
        else:
            object.frame = frame
            object.frame_prince = 0

        style_file = getwindowstylesheet(sheet_size, str(style.price_sheet))

        if not style_file is None:
            style_price = get_value(style_file, data['height'],data['width'])
            total_price += style_price
            object.style = style
            object.style_prince = style_price
        else:
            object.style = style
            object.style_prince = 0

        if data['tempered'] == True:

            tempered_file = getwindowstylesheet('Tempered', str(style.price_sheet))

            if not tempered_file is None:
                tempered_price = get_value(tempered_file, data['height'], data['width'])

                total_price += tempered_price
                object.tempered = True
                object.tempered_price = tempered_price
            else:
                object.tempered = False
                object.tempered_price = 0.00

        grid_file = getwindowstylesheet('Grids-'+data['grid'], str(style.price_sheet))
        if not grid_file is None:
            grid_price = get_value(grid_file, data['height'], data['width'])
            total_price += grid_price
            object.grid = data['grid']
            object.grid_price = grid_price

        else:
            object.grid = data['grid']
            object.grid_price = 0.00

        if data['glass'] == True:
            glass_file = getwindowstylesheet('Privacy Glass', str(style.price_sheet))
            if not glass_file is None:
                glass_price = get_value(glass_file, data['height'], data['width'])
                object.private_glass = True
                object.glass_price = glass_price
                total_price += glass_price
            else:
                object.private_glass = False
                object.glass_price = 0.00
        exterior_file = getwindowstylesheet('Exterior Color Upgrade-'+data['exterior'], exterior_pricesheet)

        if not exterior_file is None:
            exterior_price = get_value(exterior_file, data['height'], data['width'])
            object.exterior = data['exterior']
            object.exterior_price = exterior_price
            total_price += exterior_price
        else:
            object.exterior = data['exterior']
            object.exterior_price = 0.00
        object.total_price = total_price

        object.save()


def edit_item_save(sheet_size, detials, obj):
    try:

        total_price = 0
        quality_good = getwindowstylesheet(sheet_size, str(obj.style.price_sheet))
        if not quality_good is None:
            quality_price = get_value(quality_good, detials['height'], detials['width'])
            total_price += quality_price
            obj.style_prince = quality_price
            obj.height = detials['height']
            obj.width = detials['width']
            obj.real_height = detials['real_height']
            obj.real_width = detials['real_width']

        if detials['tempered'] == 'true':

            tempered_file = getwindowstylesheet('Tempered', str(obj.style.price_sheet))
            if not tempered_file is None:
                tempered_price = get_value(tempered_file, detials['height'], detials['width'])
                total_price += tempered_price
                obj.tempered = True
                obj.tempered_price = tempered_price
            else:
                obj.tempered = False
                obj.tempered_price = 0
        else:
            obj.tempered = False
            obj.tempered_price = 0

        grid_file = getwindowstylesheet('Grids-'+detials['grid'], str(obj.style.price_sheet))
        if not grid_file is None:
            grid_price = get_value(grid_file, detials['height'], detials['width'])
            obj.grid = detials['grid']
            obj.grid_price = grid_price
            total_price += grid_price
        else:
            obj.grid = detials['grid']
            obj.grid_price = 0

        if detials['glass'] == 'true':
            glass_file = getwindowstylesheet('Privacy Glass', str(obj.style.price_sheet))
            if not glass_file is None:
                glass_price = get_value(glass_file, detials['height'], detials['width'])
                obj.private_glass = True
                obj.glass_price = glass_price
                total_price += glass_price
            else:
                obj.private_glass = False
                obj.glass_price = 0
        else:
            obj.private_glass = False
            obj.glass_price = 0
            # total_price -= glass_price

        exterior_file = getwindowstylesheet('Exterior Color Upgrade-'+detials['exterior'], str(obj.style.price_sheet))

        if not exterior_file is None:
            exterior_price = get_value(exterior_file, detials['height'], detials['width'])

            obj.exterior = detials['exterior']
            obj.exterior_price = exterior_price
            total_price += exterior_price
        else:
            obj.exterior = detials['exterior']
            obj.exterior_price = 0
        if obj.room_prince:
            total_price += obj.room_prince
        if obj.frame_prince:
            total_price += obj.frame_prince
        obj.total_price = total_price

        obj.save()

        return {'success': True}
    except ValueError:
        return {'success': False}


def check_hight_width(height_value, width_value, sheet_size, obj):

    # sheetnames = getsheetnames(EXCEL_FILE)

    quality_sheet = getwindowstylesheet(sheet_size, str(obj.style.price_sheet))

    height_value = height_value
    width_value = width_value

    height_list = list(quality_sheet['Height'])

    width_list = list(quality_sheet.columns)[1:]


    max_height = max(height_list)
    max_width = max(width_list)

    if int(height_value)>int(max_height):
        return {'height_error': True, 'error': 'Window height out of range'}
    if int(width_value)>int(max_width):
        return {'height_error': True, 'error': 'Window width out of range'}

    # height = min(height_list, key=lambda x: abs(int(x) - int(height_value)))
    # width = min(width_list, key=lambda x: abs(int(x) - int(width_value)))
    height = next(x[1] for x in enumerate(height_list) if x[1] >= int(height_value))
    width = next(x[1] for x in enumerate(width_list) if x[1] >= int(width_value))
    value = round(quality_sheet.loc[quality_sheet['Height'] >= int(height), int(width)].values[0].item(), 2)
    val = str(value).upper()
    if val == "NAN":

        return {'height_error': True, 'error': 'Window size is not available'}

    return {'height_error': False, 'height': height, 'width': width}


def send_mailer(email, message):

    msg = EmailMessage('Bid Kahuna', message, 'Bid Kahuna' + '<' + settings.EMAIL_HOST_USER + '>', ['steve0605@gmail.com'])
    msg.content_subtype = "html"
    return msg.send()


def send_appointment_mailer(email, message):

    msg = EmailMessage('Bid Kahuna', message, 'Bid Kahuna' + '<' + settings.EMAIL_HOST_USER + '>', ['steve0605@gmail.com'])
    msg.content_subtype = "html"
    return msg.send()