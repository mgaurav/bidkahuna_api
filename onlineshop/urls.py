from django.conf.urls import url
from . import views


urlpatterns = [
        url(r'^$', views.details, name='details'),
        url(r'^room/', views.home, name='home'),
        url(r'^email-logout/', views.emaillogout, name='emaillogout'),
        url(r'^login/', views.login, name='login'),
        url(r'^requried-login/', views.reqlogin, name='reqlogin'),
        url(r'^logout/', views.logout, name='logout'),
        url(r'^registration/', views.registration, name='registration'),
        url(r'^frame-color/', views.frame, name='frame'),
        url(r'^window-style/', views.style, name='style'),
        url(r'^window-dimensions/', views.dimensions, name='dimensions'),
        url(r'^edit-item/(?P<item_id>[0-9]+)/$', views.dimensions, name='dimensions'),
        url(r'^cart/', views.cart, name='cart'),
        url(r'^emailregister/', views.emailregister, name='emailregister'),
        url(r'^emaillogin/', views.emaillogin, name='emaillogin'),
        url(r'^dealer-details/', views.dealer_details, name='dealer_details'),
        url(r'^check-dimension/', views.checkdimension, name='checkdimension'),
        url(r'^saveitem/', views.saveitem, name='saveitem'),
        url(r'^privacy/', views.privacy, name='privacy'),


]